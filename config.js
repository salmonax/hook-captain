module.exports = {
  DEVELOPMENT: {
    port: 3001,
    url: 'http://localhost:3001',
    flux_url: 'https://flux.io',
    flux_client_id: '9349c484-1c07-4f80-ac80-bcfe3b5c6a70',
    flux_client_secret: 'dce05e4e-9c85-4445-b047-81d29a6a7026',
    session: {
      name: 'session',
      secret: '123456'
    },
    db: {
      database: 'flux-kit',
      user: 'flux-kit',
      password: '123456',
      options: {
        dialect: 'postgres',
        port: 5432,
        logging: false,
        pool: {
          max: 5,
          min: 0,
          idle: 10000
        }
      }
    },
  },
  PRODUCTION: {
    ssl: {
      port: 443,
      key: 'cert/privkey.pem',
      cert: 'cert/cert.pem',
      ca: 'cert/chain.pem'
    },
    prod: true,
    port: 80,
    url: 'http://localhost:80',
    flux_url: 'https://pwc.fluxast.com',
    flux_client_id: '',
    flux_client_secret: '',
    session: {
      name: 'session',
      secret: '123456'
    },
    db: {
      database: 'flux-kit',
      user: 'flux-kit',
      password: '123456',
      options: {
        dialect: 'postgres',
        port: 5432,
        logging: false,
        pool: {
          max: 5,
          min: 0,
          idle: 10000
        }
      }
    },
  }
};
