# Flux Starter Kit

## Getting Started


Install dependencies

````
git clone https://github.com/fluxio/flux-kit flux-app
cd flux-app
npm run config
````

You should then register your application with [Flux](https://flux.io/developer/apps/) and fill in config.js with your credentials. 

## NPM Commands

### Start development server with hot reloading

````
npm run dev
````

### Generators

There are several generators to help with development

````
npm run generate
````

### Testing

Run test once

````
npm run test
````

Watch tests

````
npm run test:watch
````

### Linting

Run Airbnb's Eslint

````
npm run lint
````

### Production

Build for production

````
npm run build
````

Start production webpack server

````
npm run start
````

### Server

Start express server

````
npm run server
````

## Directory structure

```
app/
  README.md
  package.json
  .gitignore
  .babelrc
  .eslintrc           (airbnb eslint)
  config.js           (app secrets)
  node_modules/
  server/
    lib/              (miscellaneous libraries)
    models/           (database models)
    routes/           (express routes)
    index.js          (server)
  client/
    config/           (webpack config)
    dist/             (compiled code)
    src/
      components/     (react components)
      lib/            (miscellaneous libraries)
        constants.js  (client constants)
        request.js    (request handler)
      stores/         (mobx stores)
      index.js        (client)
    static/           (static assets)
    test/             (test suites)
  scripts/
    generators/       (code generators)
```

## Documentation

- [Flux SDK](https://flux.gitbooks.io/flux-javascript-sdk/content/)
- [React](https://facebook.github.io/react/docs/hello-world.html) (views)
- [React Router](https://github.com/ReactTraining/react-router/tree/v4) (client routing)
- [Mobx](https://mobx.js.org/refguide/api.html) (state)
- [Plop](https://github.com/amwmedia/plop) (generators)
- [Express](http://expressjs.com/en/4x/api.html) (server)
- [Sequelize](http://docs.sequelizejs.com/en/v3/) (database orm)
- [Ava](https://github.com/avajs/ava#usage) (testing)
- [Enzyme](https://github.com/airbnb/enzyme/blob/master/docs/guides/tape-ava.md) (react testing)
- [SASS](http://sass-lang.com/guide) (styling)
- [JS Styleguide](https://github.com/airbnb/javascript) (airbnb)
