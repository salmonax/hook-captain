const FLUX_URL = 'https://flux.io/';
const fetch = require('node-fetch');
const stringifyQuery = require('query-string').stringify;
const base64Decode = (str) => (new Buffer(str, 'base64')).toString('utf8');
const base64Encode = (str) => Buffer(str).toString('base64');
function joinUrl(...args) {
  return args.join('/')
    .replace(/([^:])\/([\/|#|\?])/g, (match, p1, p2) => `${p1}${p2}`)
    .replace(/\/*(#|\?)\/*/g, (match, p1) => p1);
}
const EMPTY_BODY = JSON.stringify(null);
let fluxUrl = FLUX_URL;

function decodeHeader(header) {
  return JSON.parse(base64Decode(header));
}

function createAuthHeaders({ tokenType, accessToken, fluxToken }) {
  return {
    Authorization: `${tokenType} ${accessToken}`,
    Cookie: `auth=${accessToken}; flux_token=${fluxToken}`,
    'Flux-Request-Token': fluxToken,
    'Flux-Request-Marker': 1,
  };
}

function handleJson(response) {
  return response.json()
    .catch(() => EMPTY_BODY);
}

function handleAuxiliaryResponse(response) {
  const auxiliaryHeader = response.headers.get('flux-auxiliary-return');
  const auxiliary = decodeHeader(auxiliaryHeader);
  return auxiliary ? handleJson(response)
    .then(value => Object.assign({}, { value }, auxiliary)) : handleJson(response);
}

function handleSuccess(response) {
  const headers = response.headers;
  return headers.has('flux-auxiliary-return') ?
    handleAuxiliaryResponse(response) : handleJson(response);
}

function handleError(response) {
  return response.text()
    .then(text => {
      const status = response.status;
      const statusText = response.statusText || 'Zut alors!';

      const error = new Error(`${status} ${statusText}: ${text}`);
      error.response = response;
      error.status = status;

      throw error;
    });
}

function handleResponse(response) {
  const status = response.status;
  const isError = status < 200 || status >= 300;
  return isError ? handleError(response) : handleSuccess(response);
}

function request(path, options = {}) {
  const { query, body, headers } = options;
  const others = Object.assign({}, options);
  // console.log('!!!!', others);
  delete others.query; delete others.body; delete others.headers;
  const payload = body ? { body: JSON.stringify(body) } : null;
  const contentType = payload ? { 'Content-Type': 'application/json' } : null;
  const search = query ? stringifyQuery(query) : '';

  return fetch(joinUrl(others.fluxUrl || fluxUrl, path, search), 
    Object.assign({
      credentials: 'include',
      headers: Object.assign({}, headers, contentType)
    }, payload, others))
    .then(handleResponse);
}

function createFluxOptionsHeader({ clientInfo }, fluxOptions) {
  return {
    'Flux-Options': base64Encode(JSON.stringify(
      Object.assign({ ClientInfo: clientInfo }, fluxOptions)
    )),
  };
}

function authenticatedRequest(credentials, path, options = {}) {
  const { headers, fluxOptions } = options;
  const others = Object.assign({}, options);
  delete others.headers; delete others.fluxOptions;
  return request(path, Object.assign({ 
    headers: Object.assign({}, 
      createFluxOptionsHeader(credentials, fluxOptions), 
      createAuthHeaders(credentials), 
      headers)
    }, others)
  );
}

function setFluxUrl(url) {
  fluxUrl = url || FLUX_URL;
}

module.exports = { authenticatedRequest };