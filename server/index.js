const fs = require('fs');
const https = require('https');
const http = require('http');
const crypto = require('crypto');
const url = require('url');
const FluxSdk = require('flux-sdk-node');
const express = require('express');
const ENV = process.env.ENV || 'DEVELOPMENT';
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const logger = require('morgan');
const config = require('../config')[ENV];
const favicon = require('serve-favicon')
const sessionStore = new session.MemoryStore();
const cookies = cookieParser(config.session_secret);
const routes = require('./routes');
const compression = require('compression')

const level = require('level')

const db = level('./flux-data');

const app = express();
// app.db = require('./lib/db');
const server = http.createServer(app);

console.log(ENV);
const { authenticatedRequest } = require('./lib/sdk/request');

global._credentials = null

app.use(compression());

let httpsOptions;
let httpsServer;
if (config.prod) {
  console.log('In production mode. Loading certs.');
  httpsOptions = {
    key: fs.readFileSync(config.ssl.key),
    cert: fs.readFileSync(config.ssl.cert),
    ca: fs.readFileSync(config.ssl.ca),
  };
  httpsServer = https.createServer(httpsOptions, app);
}

const sdk = new FluxSdk(config.flux_client_id, {
  clientId: config.flux_client_id,
  clientSecret: config.flux_client_secret,
  fluxUrl: config.flux_url,
});

app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.use(bodyParser.json({ limit: '50mb' }));
// app.use(favicon(__dirname + '/../client/dist/img/favicon.ico'))
app.use(logger('dev'));

// hack to gzip flux-viewport and the rest
app.get('*.js', function (req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  next();
});

app.use('/assets', express.static('client/static'));
app.use(cookies);

app.use(session({
  secret: config.session.secret,
  name: config.session.name,
  resave: true,
  saveUninitialized: true,
  store: sessionStore,
}));

app.use((req, res, next) => {
  req.credentials = req.session.fluxCredentials;
  next();
});

function generateRandomValue() {
  return crypto.randomBytes(24).toString('hex');
}

function ensureSecure(req, res, next) {
  if (req.secure || !config.prod) return next();
  return res.redirect(`https://${req.get('host')}${req.url}`);
}

app.use(ensureSecure);

app.get('/login', (req, res, next) => {
  const redirectUri = url.resolve(config.url, 'login');
  console.warn('asdaosid', redirectUri, req.query.code)
  if (!req.query.code) {
    req.session.state = generateRandomValue();
    req.session.nonce = generateRandomValue();
    // console.warn('should be fucking no code working')
    // console.warn('!!',config.flux_client_id)
    const authorizeUrl = sdk.getAuthorizeUrl(req.session.state, req.session.nonce, { redirectUri });
    // console.warn(authorizeUrl)      
    // console.log('after getAuthorizeUrl...');
    // console.warn(authorizeUrl)
    return res.redirect(authorizeUrl);
  }
  return sdk.exchangeCredentials(req.session.state, req.session.nonce, req.query, { redirectUri })
    .then((credentials) => {
      req.session.state = null;
      req.session.nonce = null;
      const t = credentials.idToken.payload;
      const user = {
        id: t.email,
        credentials: JSON.stringify(credentials),
      };
      req.session.user = user;
      req.session.credentials = credentials;
      global._credentials = credentials
      res.cookie('doc', 'brown');
      res.redirect('http://localhost:3000/#authenticated');
      // res.redirect('/logout')
      
      // db.user.upsert(user).then((updated) => {
      //   return res.redirect('/');
      // }).catch(next);
    }).catch(next);
});

app.get('/logout', (req, res) => {
  console.warn('here is the fucking logout business')
  console.warn(req.session)
  console.log('????', req.header)
  authenticatedRequest(global._credentials,'api/p/a52JBkgBbgYkXVGNn/api/datatable/v1/cells/').then(n => {
    console.log(n)
  }).catch(err => console.log('error', err))

  res.send({ msg: 'we suck sorry' })
  // req.session.destroy(() => res.redirect('/'));
});

// app.use('/api', (req, res, next) => {
//   if (req.session.credentials) {
//     req.user = sdk.getUser(req.session.credentials);
//   }
//   next();
// });


app.get('/api/*', (req, res) => {
  // console.log(req.options, req.headers)
  // console.log('----',req.originalUrl, req.originalUrl.substr(5));

  const endpoint = req.originalUrl.substr(5);

  let fluxOptions = null
  if (req.headers['flux-options']) {
    fluxOptions = JSON.parse((new Buffer(req.headers['flux-options'], 'base64')).toString('utf8'));
  }

  db.get(endpoint)
    .then(data => {
      console.log('Using cached data for', endpoint);
      res.json(JSON.parse(data));
    })
    .catch(err => {
      // console.log('!!!', err)
      console.log('No cached data for endpoint', endpoint, '... fetching.');
      authenticatedRequest(_credentials, endpoint).then(data => {   
        console.log('Loaded API data from Flux');
        db.put(endpoint, JSON.stringify(data)).then(() => {
          console.log('Successfully saved data for', endpoint);
          res.json(data);
        });
      }).catch(err => {
        console.log("Failed to load Flux data for", endpoint);
      });
    })
})

routes(app);

// Production routing
app.use('/', express.static('client/dist'));
app.get('*', (req, res) => {
  res.sendFile('index.html', { root: 'client/dist' });
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({ message: err.message, error: {} });
});

server.listen(config.port);
console.log(`Listening on ${config.port}`);
if (config.prod) {
  console.log(`Production mode. SSL on ${config.ssl.port}`)
  httpsServer.listen(config.ssl.port);
}
