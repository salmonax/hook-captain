import request from '../lib/request';
import Common from './common';

export const stores = (state = {}) => ({
  common: new Common(request, state.common),
});

export default stores(window.STATE || {});
