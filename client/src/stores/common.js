// alert('not sure why this comes later....');
import { observable, computed, action, toJS } from 'mobx';
import { fluxHelpers } from '../services/';

import { jobs, cranes, stock } from 'fixtures/data';

import { getModelInputs, runAlgorithm } from 'algorithm/src/algorithm';
import { Crane } from 'algorithm/src/class';

import { getElementStocks, mergeParsedElements, getElementTable } from "algorithm/src/data/parse_elements"
import { getJobListing, getJobTable } from "algorithm/src/data/parse_jobs"
import { getCraneTypes, getCraneTable } from "algorithm/src/data/parse_cranes"
import getPickupLocations from "algorithm/src/data/parse_pickups"

import * as cranesLib from 'viewports/cranes/lib';

import * as algoProxy from 'lib/workerLoader';
//window.algoProxy = algoProxy;

// Note: Three.js is used a as global dependency due to viewport

const p = console.error.bind(console)

const today = (new Date(Date.now())).toISOString().split('T')[0];

const DEBUG = {
  cranesOnly: false, // sets isDataLoaded to true, preloads crane fixtures, preloads projectStart and projectEnd
  noAlgo: false, // stubs out noitfyAlgorithm()
  algoWorker: true, // runs the algorithm in a webworker instead of the main thread (RECOMMEND!)
};

const DEBUG_DATA = {
  start: new Date(1502722800000),
  end: new Date(1535155200000)
}

const gbDate = (dateString) => {
  const [ mm, dd, yy ] = new Date(dateString).toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: '2-digit'}).split('/');
  return [ dd, mm, yy ].join('/');
};

export default class Common {

  @observable title = 'Flux Kit'
  @observable statusCode = 200
  @observable hostname = 'localhost'
  @observable random = Date.now();

  // Source Data Page Stuff
  @observable projects = [];
  @observable selectedProject = {};
  @observable cells = [];
  @observable jobs = [];
  @observable cranes = [];
  @observable stock = [];
  @observable projectDates = {
    start: DEBUG.cranesOnly ? DEBUG_DATA.start : null,
    end: DEBUG.cranesOnly ? DEBUG_DATA.end : null,
  };
  @observable isDataLoaded = DEBUG.cranesOnly ? true : false;


  @observable algoMap = new Map();
  @observable algoScenario = []; // this is the same as currentScenario, but has post-algo activity figures
  @observable algoMapByScenario = {}; // hate this, but it'll do the job
  @observable algoScenarios = {}; // Oh brother... But renewScenario() will take it in stride.
  
  // @observable craneScheduleMap = new Map();

  

  @observable isPlanLoaded = false;
  plan = null;

  @observable isMassingLoaded = false;
  massing = null;


  @observable elementSpinner = false;
  @observable showElementProgress = false;
  @observable selections = {
    elements: [],
    jobs: '',
    cranes: '',
    plan: '',
    pickups: '',
  }

  // Used by the editor route component
  // Not sure why I opted to put it here rather than local to the component
  @observable showCranesSchedule = true;

  @observable cranesForm = {
    start: DEBUG.cranesOnly ? DEBUG_DATA.start.toISOString().split('T')[0] : today,
    end: DEBUG.cranesOnly ? DEBUG_DATA.end.toISOString().split('T')[0] : today
  }
  @observable cranesWindow = {
    start: null,
    end: null,
  }
  @observable focusedCrane = null;

  // This is an ugly hack, but moving a crane that happens to be the focused crane
  // from the CraneBar will cause this to update, allowing the TimeBar
  // to deal with some of the side effects it needs to perform
  // Since MobX-react is so good at managing transactions, I opted to do this rather
  // than firing off an event
  @observable movingFocusedCrane = null; 

  @computed get editorMode() {
    this.focusedCrane ? 'editing' : 'adding';
  }

  @action.bound setCranesFormFromDate(start, end) {
    this.cranesForm = {
      start: start.toISOString().split('T')[0],
      end: end.toISOString().split('T')[0],
    };
  }

  @action.bound updateCranesForm(which, e) {
    const minDate = new Date(this.cranesForm.start);
    const maxDate = new Date(this.cranesForm.end);

    const newDateValue = new Date(e.target.value).getTime();
    if (which === 'start') {
      if (new Date(this.cranesForm.end).getTime() <= newDateValue) {
        alert('Please select an start date earlier than the end date.');
        return;
      }
      if (this.projectDates) {
        if (this.projectDates.end.getTime() <= newDateValue) {
          alert('Please select a start date earlier than the project end date.');
          return;
        }

        if (this.projectDates.start.getTime() > new Date(newDateValue)) {
          alert('Please select a start date no earlier than the project start date.');
          return;
        }
      }
    } else if (which === 'end') {
      console.log('which is end');
      if (new Date(this.cranesForm.start).getTime() >= newDateValue) {
        console.warn(new Date(this.cranesForm.start).getTime(), newDateValue);
        alert('Please select an end date later than the start date.');
        return;
      }
      if (this.projectDates) {
        if (this.projectDates.start.getTime() >= newDateValue) {
          alert('Please select an end date later than the project start date.');
          return;
        }

        if (this.projectDates.end.getTime() < newDateValue) {
          alert('Please select an end date no later than the project end date.');
          return; 
        }
      }
    }
    this.cranesForm[which] = e.target.value;
  }

  @observable stocksViewport = null;

  // Cranes Stuff
  @observable scenarios = {}; 
  @observable currentScenario = [];
  @observable editorViewport = null;
  @observable isEditing = true;
  @observable craneInfo = { id: '' };

  @observable selectedElementId = null;
  @observable elementProgress = 0;
  @observable elementLoading = '';

  @observable timeBarBounds = {
    left: '0%', 
    right: '0%',
  }

  @observable animateTimeBar = true;
  @observable hideEditorUI = false;

  @action.bound clearScenarioData() {
    this.scenarios = {};
    this.currentScenario = [];
    this.algoScenario = [];
    this.algoMap = new Map();
  }

  @action.bound selectElementCell(type, id) {
    // Not using type; just keeping convention for now in case
    // This function just sets selectedElementId so that another button can 
    //   load and call the progress bar
    this.selectedElementId = id;
  }

  @action.bound selectCell(type, index, id) {
    if (id === 'default') return;
    const { selectedProject } = this;
    if (!selectedProject) return;

    if (this.selections[type] === undefined) {
      console.warn('ERROR ----> ', type, 'selected but not set. Check component');
    }
    if (type === 'elements') {
      this.selections[type][index] = id;
    } else {
      this.selections[type] = id;
    };
  }

  get _isMassingRendered() {
    // using current stock's magic number, but gets the job done
    if (!this.stocksViewport) return false;
    return this.stocksViewport._renderer._scene.children[1].children.length > 15000;
  }

  _totalFetchSize = 0
  _totalFetchElapsed = 0
  updateBpms(fetchSize, elapsedMilliseconds) {
    console.warn('updateBpms:', fetchSize, elapsedMilliseconds);
    this._totalFetchSize += fetchSize;
    this._totalFetchElapsed += elapsedMilliseconds;  
  }
  get bpms() {
    if (!this._totalFetchElapsed) return null;
    return this._totalFetchSize/this._totalFetchElapsed;
  }

  @action.bound loadSourceData() {
    // Note: this is one of the things that could use some refactoring
    // 1. Extract the progressBar logic into its class, so its responsibilities are clear
    // 2. Extract each step of the promise chain into their own function.

    if (!this.selectedProject) return;
    if (this.noProjectFail) {
      alert('ERROR: You must have a valid preview project in your Flux account in order to use this application. You will not be able to load data.');
      return;
    }
    // First grab the info
    const { pickups, jobs, cranes, plan } = this.selections;
    const selectedIds = [pickups, jobs, cranes, plan];
    if(selectedIds.some(n => !n)) {
      alert('Please populate all fields before loading elements.');
      return;
    }
    this.isDataLoaded = false; // prevent massing freeze if no new project selected and load pressed twice
    // console.log('this.selections in loadSourceData:', toJS(this.selections));
    const dataTable = fluxHelpers.getUser().getDataTable(this.selectedProject.id);

    const cells = selectedIds.map(id => this.cells.filter(cell => cell.id === id)[0]);

    // Use this to collect progress bar estimation
    const fetchSize = cells.reduce((total, cell) => total += cell.size, 0);
    console.log('loadSourceData fetchSize:', fetchSize);

    const timeBeforeFetch = Date.now();
    const cellPromises = cells.map(cell => dataTable.getCell(cell.id).fetch());

    const starterPromise = new Promise((resolve, reject) => {
      const preElementPromise = Promise.all(cellPromises).then(elements => {
        const elapsedMilliseconds = (Date.now()-timeBeforeFetch);
        this.updateBpms(fetchSize, elapsedMilliseconds);
        this.elementProgress = 100; // set progress to 100, to resolve promise early
        return elements;
      }).catch((err) => reject(err));

      this.showElementProgress = true;
      this.elementProgress = 0;
      this.elementLoading = 'pre-element data';
      const _startTime = Date.now();
      const _bpms = this.bpms; // cache this so it doesn't act funky during bar progression
      const _duration = _bpms ? fetchSize/_bpms : null;
      const _preProgressInterval = this._preProgressInterval = setInterval(() => {
        if (this.elementProgress === 100) {
          clearInterval(_preProgressInterval);
          this.elementLoading = '';
          preElementPromise.then(([pickups, jobs, cranes, plan]) => {
            // 1. Get our time estimation and set the bps, for progress bar
            // this.elementProgress = 100; // force to 100 if it happens to end earlier than estimated
            console.log(fetchSize, 'downloaded in', Date.now()-timeBeforeFetch, 'milliseconds')
            const elapsedMilliseconds = (Date.now()-timeBeforeFetch);
            this.updateBpms(fetchSize, elapsedMilliseconds);
            // this.bpms = fetchSize/elapsedMilliseconds;

            console.log('BPS: ', this.bpms);
            // Extracting Job data
            const jobListing = this.jobListing = getJobListing(jobs.value);
            const jobTable = getJobTable(jobListing);
            this.jobs = this.tablify('jobs', jobTable);

            // Extracting Crane data
            const craneTypes = this.craneTypes = getCraneTypes(cranes.value);
            const craneTable = getCraneTable(craneTypes);
            this.cranes = this.tablify('cranes', craneTable);
            
            // console.log('crane table: ', craneTable);
            // Make sure the damn 100% renders, at any cost!!!
            setTimeout(() => {
              this.elementProgress = 0;
              resolve(getPickupLocations(pickups.value));
            }, 0);
          })
          .catch((err) => reject(err));
          return;
        }
        const elapsed = Date.now()-_startTime;
        if (!_duration) console.warn('No bpms on preElement load, using estimate!');
        const percentage = _duration ? 
          Math.round(Math.min(elapsed/_duration*100, 100)) :
          // If there's nothing pre-loaded by the time 'load' is hit, the connection must be
          // VERY slow, so let the estimate take its time
          Math.round(Math.min(this.elementProgress + Math.random()*0.5+0.5, 100));
        this.elementProgress = percentage;
      }, 100);
    });

    starterPromise.then(parsedPickups => {
      this.pickupLoc = parsedPickups;
      return this.selections.elements.reduce((chain, id) => 
        chain.then(parsedElements => {
          // this.elementProgress = 0; // Only setting zero here; was in the setIntervals before
          const element = this.cells.filter(cell => cell.id === id)[0];
          const _startTime = Date.now();
          // Note: dividing duration by 2 makes the estimation closer for reasons completely unknown
          const _duration = element.size/this.bpms;
          console.log('need to download', element.size, 'at a speed of', this.bpms, 'bpms')
          // this.showElementProgress = true;
          this.elementLoading = element.label;
          return new Promise((resolve, reject) => {
            const timeBeforeFetch = Date.now();
            const elementPromise = dataTable.getCell(id).fetch().then(element => {
              const fetchSize = element.size;
              const elapsedMilliseconds = (Date.now()-timeBeforeFetch);
              this.updateBpms(fetchSize, elapsedMilliseconds);
              const currentBPMS = fetchSize/elapsedMilliseconds;
              // this.totalFetchSize += fetchSize;
              // this.totalFetchElapsed += elapsedMilliseconds;
              console.warn('AFTER FETCH (cumulative, current):', this.bpms, currentBPMS);
              this.elementProgress = 100; // set progress to 100, which will resolve the promise at the next setInterval
              return element;
            }).catch((err) => reject(err));

            const _progressInterval = this._progressInterval = setInterval(() => {
              if (this.elementProgress === 100) {
                clearInterval(_progressInterval); 
                this.elementLoading = '';
                elementPromise.then(element => {
                  console.warn('AFTER SETINTERVAL')
                  // Make sure the 100% gets drawn on the other end of the event queue
                  // this.elementProgress = 100;
                  if (!element.value) {
                    console.warn('WARNING: null element selected in Source Data');
                    throw new Error('Failed to fetch (mine)');
                    return;
                  }
                  const parsedElement = getElementStocks(element.value, parsedPickups);
                  console.log('parsedElement inside reduce: ', parsedElement);
                  // Do more stuff
                  // Make sure the damn 100% renders, with voodoo magicks!
                  setTimeout(() => {
                    this.elementProgress = 0;
                    resolve(parsedElements.concat(parsedElement));
                  },0);
                  // console.log('!!!', parsedElement, parsedPickups);
                })
                .catch((err) => reject(err));
                return;
              }
              const elapsed = Date.now()-_startTime;
              const percentage = Math.round(Math.min(elapsed/_duration*100, 100));
              // console.log('progress bar: ', this.bps, elapsed, _duration, percentage);
              this.elementProgress = percentage;
            }, 100);
          });
        }),
        Promise.resolve([])
      )
    })
    .then(parsedElements => {
      this.elementSpinner = true;
      // The rest is pushed to the event loop so that the spinner loads first.
      // JavaScript will block for a while when the massing loads, but the
      // spinner will animate fine.
      if (!parsedElements.length) {
        this.resetProgress();
        alert('Warning: no element stocks selected! BIM data will not be calculated.')
        return;
      }
      const elements = this.elementStock = mergeParsedElements(parsedElements);
      const elementsTable = getElementTable(elements);
      this.stock = this.tablify('stock', elementsTable.table);

      const { pickupLoc, jobListing, craneTypes, elementStock } = this;
      // const { projectStart, projectEnd } = getModelInputs(pickupLoc, jobListing, craneTypes, elementStock);

      const initProjectDates = ({ projectStart, projectEnd}) => {
        this.projectDates = {
          start: projectStart,
          end: projectEnd,
        };
        this.cranesForm = {
          start: projectStart.toISOString().split('T')[0],
          end: projectEnd.toISOString().split('T')[0],
        };
        this.clearScenarioData();
        if (this._isMassingRendered) this.resetProgress(); // better than checking isDataLoaded
        this.isDataLoaded = true;
      };

      // Optionally disable Web Worker
      if (DEBUG.algoWorker) {
        return algoProxy.getModelInputs(pickupLoc, jobListing, craneTypes, elementStock)
          .then(initProjectDates);
      } else {
        const projectDates = getModelInputs(pickupLoc, jobListing, craneTypes, elementStock);
        initProjectDates(projectDates);
      }
    }).catch(err => {
      clearInterval(this._progressInterval);
      clearInterval(this._preProgressInterval);
      if (/Failed to fetch/.test(err)) {
        alert('Connection reset during download. Please make sure you have a stable connection and try again.');
      } else {
        alert('Error during source data loading. There may be a problem with the selected data.');
      }
      console.warn('Error during source data load:', err);
      this.resetProgress();
    });
  }

  @action.bound resetProgress() {
    this.showElementProgress = false;
    this.elementSpinner = false;
    this.elementProgress = 0;
  }


  @observable elementCount = 1;
  @action.bound addElement() {
    if (this.elementCount === 9) return;
    this.elementCount++;
  }
  @action.bound removeElement() {
    if (this.elementCount === 1) return;
    this.elementCount--;
  }

  bps = 0;
  // bpms = 0;
  totalFetchElapsed = 0;
  totalFetchSize = 0;

  constructor(request, state = {}) {
    this.request = request;
    if (state.title != null) this.title = state.title;
  }

  @computed get isMassingReady() {
    const { isMassingLoaded, massing, isDataLoaded } = this;
    return isMassingLoaded && massing && isDataLoaded;
  }

  @computed get baseProject() {
    const candidateProjects = this.projects.filter(project => /PWC Project/.test(project.name));
    return candidateProjects.sort((a, b) => a.name > b.name)[0];
  }

  @action.bound selectProject(id) {
    const selectedProject = this.projects.filter(project => project.id === id)[0];
    this.selectedProject = selectedProject;
    debugger;
    const dataTable = fluxHelpers.getUser().getDataTable(selectedProject.id);
    dataTable.listCells().then(data => {
      this.cells = data.entities.sort((a, b) => {
        a = a.label.toUpperCase();
        b = b.label.toUpperCase();
        return (a < b) ? -1 : (a > b) ? 1 : 0;
      });
      this.selections = {
        elements: [],
        jobs: '',
        cranes: '',
        plan: '',
        pickups: '',
      };
      this.isDataLoaded = false;
    });
  }

  @action.bound renewScenario(scenario) {
    // This gets called mainly on route changes. It's an old band-aid fix for code operating on "this.scenarios"
    // WARNING: as long as this is here, it's better to work on "this.scenarios" and call this function.
    // Otherwise, if another handler calls it, changes to this.currentScenario will be wiped out
    this.currentScenario = this.scenarios[scenario] || []; // these are the pre-algo cranes
    this.algoScenario = this.algoScenarios[scenario] || []; // these are the post-algo cranes
    this.algoMap = this.algoMapByScenario[scenario] || new Map(); // this is the raw algo output
  }

  // Filter cranes within the current window, reading this.cranesWindow
  // It's used mainly for filtering cranes when switching scenarios.
  // It's also used when in 'focus' mode, taking an optional crane argument to use as the window
  @action.bound cranesByWindow(cranes) {
    if (!this.cranesWindow.start || !this.cranesWindow.end || !cranes) return cranes;
    const windowStart = this.cranesWindow.start;
    const windowEnd = this.cranesWindow.end;
    const { max, min } = Math;
    return cranes.filter(crane => {
      const craneStart = new Date(crane.start).getTime();
      const craneEnd = new Date(crane.end).getTime();
      return max(craneStart, windowStart) <= min(craneEnd, windowEnd);
    });
  }

  // Filter cranes by window arguments and re-render the scene. 
  // This is used by the _updateCranes method in the TimeBar component
  // It should probably be refactored out, or at least made truly end-to-end
  // by both setting this.cranesWindow from args and calling cranesByWindow()
  @action.bound filterScenario(windowStart, windowEnd, forceRender) {
    if (!this.currentScenario || !this.editorViewport) return;
    windowStart = windowStart.getTime();
    windowEnd = windowEnd.getTime();
    const { max, min } = Math;
    const cranesToShow = this.currentScenario.filter(crane => {
      const craneStart = new Date(crane.start).getTime();
      const craneEnd = new Date(crane.end).getTime();
      return (
        max(craneStart, windowStart) < min(craneEnd, windowEnd)
      );
    });
    const { _scene } = this.editorViewport._renderer;

    // WARNING: there are two things wrong with this: 
    // 1: it redraws all the cranes
    // 2: there is no hash of cranes in the three.js scene, so it
    //    relies on traversal despite the immensity of the site plan
    if (this._areDifferent(cranesToShow, this._lastCranes) || forceRender) {
      cranesLib.renewCranes(cranesToShow, _scene, this.focusedCrane ? this.focusedCrane.data : null);
      this.editorViewport.render();
    }
    this._lastCranes = cranesToShow;
  }

  _areDifferent(a, b) {
    if (!a || !b || a.length !== b.length) return true;
    a = a.map(crane => crane.uuid).sort();
    b = b.map(crane => crane.uuid).sort();
    return a.some((n, i) => n !== b[i]);
  }
  
  // Used by CraneBar (and TimeBar in focus mode) to update a single crane's schedule
  // The crane input can be dead (ie. one of the crappy cruft cranes polluting the app state)
  // and it will take care of everything
  @action.bound updateCraneWindow(crane, scenarioName, leftDate, rightDate, forceRender, skipAlgo) {
    const { _scene } = this.editorViewport._renderer;
    const craneToUpdate = this.scenarios[scenarioName].filter(({uuid}) => uuid === crane.uuid)[0];
    craneToUpdate.start = leftDate.toISOString().split('T')[0];
    craneToUpdate.end = rightDate.toISOString().split('T')[0];
    const cranesToShow = this.cranesByWindow(this.currentScenario);

    if (this._areDifferent(cranesToShow, this._lastCranes) || forceRender) {
      cranesLib.renewCranes(cranesToShow, _scene, this.focusedCrane ? this.focusedCrane.data : null);
      this.editorViewport.render();  
    }
    this._lastCranes = cranesToShow;
    // WARNING: renewCranes is constantly traversing the entire scene, which is inefficient in most cases
    this.renewScenario(scenarioName);
    if (skipAlgo) return;
    this.notifyAlgorithm(scenarioName);
  }

  @action.bound toggleEditorMode() {
    if (!this.isDataLoaded) return alert("Please load source data before editing cranes.")
    this.isEditing = !this.isEditing;
  }


  @action.bound setDefaultSelections() {
    const pwc = this.baseProject;
    p('setDefaultSelections')
    if (!pwc) {
      if (!this.noProjectFail) {
        alert('ERROR: You must have a valid preview project in your Flux account in order to use this application. You will not be able to load data.');
        this.noProjectFail = true;
      }
      return;
    }
    this.selectedProject = pwc;
    const cells = `Job Listing, Crane Types, Site Plan, PickUp Location`.split(', ');
    const dataTable = fluxHelpers.getUser().getDataTable(pwc.id);
    dataTable.listCells().then(data => {
      const { entities } = data;
      this.cells = entities.sort((a, b) => {
        a = a.label.toUpperCase();
        b = b.label.toUpperCase();
        return (a < b) ? -1 : 
                (a > b) ? 1 : 0;
      });
      const [jobs, cranes, plan, pickups] = cells.map(label => entities.filter(entity => entity.label === label)[0].id);
      this.selections = Object.assign(this.selections, { jobs, cranes, plan, pickups });

      // console.log(this.selections);
    });
  }

  @action.bound setDefaults() {
    const pwc = this.baseProject;
    p('setDefaults')
    if (!pwc) {
      if (!this.noProjectFail) {
        alert('ERROR: You must have a valid preview project in your Flux account in order to use this application. You will not be able to load data.');
        this.noProjectFail = true;
      }
      return;
    }
    const dataTable = fluxHelpers.getUser().getDataTable(pwc.id);
    this.selectedProject = pwc;
    dataTable.listCells()
      .then(data => {
        const { entities } = data;
        // console.log(JSON.stringify(data, null, 2));

        const cells = `Job Listing, Crane Types, Site Plan, PickUp Location`.split(', ')
          .map(label => entities.filter(entity => entity.label === label)[0]);
        
        const timeBeforeFetch = Date.now();
        const fetchSize = cells.reduce((total, cell) => total += cell.size, 0);
        console.log('setDefaults fetchSize: ', fetchSize);
        const cellPromises  = cells.map(cell => dataTable.getCell(cell.id).fetch());

        return Promise.all(cellPromises).then(([jobs, cranes, plan, pickups]) => {
          const elapsedMilliseconds = Date.now()-timeBeforeFetch;
          const elapsedSeconds = elapsedMilliseconds/1000;
          this.updateBpms(fetchSize, elapsedMilliseconds);
          const bps = this.bps = fetchSize/elapsedSeconds;
          console.log('setDefaults() fetched', fetchSize, 'in', elapsedSeconds,'bps:', bps);
          this.plan = plan;
          this.isPlanLoaded = true;
          // this.jobs = this.tablify('jobs', jobs);
          if (DEBUG.cranesOnly) this.cranes = this.tablify('cranes', cranes);

          // getModelInputs(pickups.value, jobs.value, cranes.value);
          // runAlgorithm();
          // do whatever with plans here
          return data;
        });
      })
      .then(data => {
        const { entities } = data;
        // this isn't always working... hmmm
        const massing = entities.filter(({label}) => label === 'Building Massing')[0];
        console.log('massing after filter: ', massing, massing.id);
        
        const fetchWhileNull = () => dataTable.getCell(massing.id).fetch().then(massing => {
          if (massing.size < 100) {
            console.warn('The massing came back null; requesting again...');
            return fetchWhileNull();
          }
          return massing;
        });

        return fetchWhileNull();
      })
      .then(massing => {
        this.massing = massing;
        this.isMassingLoaded = true;
      });
  }

  tablify(type, data) {
    // Note: leaving pre-algo stuff around for a short time
    // It required an extra shift on the jobs table
    console.log('tablify', type, data);
    let legacy = false;
    if (data && data.value) { 
      data = data.value;
      legacy = true;
    }
    const items = data.slice();
    if (type === 'jobs' && legacy) items.shift();
    const keys = items.shift();
    return items.map(item => this.objectify(item, keys, type));
  }
  
  objectify(item, keys, type) {
    const obj = {};
    keys.forEach((key, i) => { 
      // console.warn(keys[i], item[i]);
      let value = item[i]
      if (type === 'jobs' && /Date/.test(keys[i])) {
        value = gbDate(value);
      }
      return obj[keys[i]] = value;
    });
    return obj;
  }

  @action.bound notifyAlgorithm(scenarioName) {
    if (DEBUG.noAlgo) return;
    try {
      if (this.__algoPending || 
          this.__lastAlgoCall && 
          // Throttle to 50ms with worker, 250 otherwise
          Date.now() - this.__lastAlgoCall < (DEBUG.algoWorker ? 100 : 250) &&
          // Avoid throttling if the number of cranes has changed
          this.currentScenario.length === this.__lastScenarioLength) {
            // Prevent a rebuff of the last algo run from skipping the last interaction state
            if (!this.__algoDebounce) {
              this.__algoDebounce = setTimeout(() => {
                this.notifyAlgorithm(scenarioName);
              }, 350);
            }
            return;
      }
      clearTimeout(this.__algoDebounce);
      this.__algoDebounce = null;
      console.log('algorithm notified');
      this.__algoPending = true;
      const algoCranes = this.currentScenario.map(crane => {
        let inputStartDate = new Date(crane.start)
        let inputEndDate = new Date(crane.end)
        const startDate = new Date(inputStartDate.getTime() + 8*60*60*1000);
        const endDate = new Date(inputEndDate.getTime() + 17*60*60*1000);
        // CRITICAL: endate should be set at 1700 hours, not 8... This is a mistake!
        [startDate, endDate].forEach(d => d.setHours(8));
        return new Crane(
             crane.uuid, 
             this.craneTypes[crane.id], 
             startDate,
             endDate,
             [crane.coords.x, crane.coords.y, 0]
           );
      });

      const beforeAlgoTime = Date.now();
      const doAlgoBusiness = (algoOutput) => {
        // do debounce fix here if algoOutput is null
        window.algoOutput = algoOutput;
        this.algoMap.merge(algoOutput);
        this.currentScenario.forEach(crane => {
          const outputCrane = algoOutput.cranes[crane.uuid];
          if (outputCrane) {
            crane.activity = +(outputCrane.activity/100).toFixed(2);
          }
        });
        this.algoScenario = this.currentScenario;
        this.algoMapByScenario[scenarioName] = this.algoMap;
        this.algoScenarios[scenarioName] = this.algoScenario;

        this.__lastAlgoCall = Date.now();
        this.__algoPending = false;
        this.__lastScenarioLength = this.currentScenario.length;
      };

      // Optionally disable Web Worker
      if (DEBUG.algoWorker) {
        algoProxy.runAlgorithm(algoCranes)
        .then(doAlgoBusiness)
        .catch(err => {
          console.warn('Error in algoWorker:', err);
          this.__lastAlgoCall = Date.now();
          this._algoPending = false;
          // throw new Error(err);
        });
      } else {
        const algoOutput = runAlgorithm(algoCranes);
        doAlgoBusiness(algoOutput);
      }

    } catch(err) {
      this.__lastAlgoCall = Date.now();
      this.__algoPending = false;
      throw new Error(err);
    }
  }

  @action.bound clearCranes(scenarioName) {
    if (!this.editorViewport) return;
    this.scenarios[scenarioName] = [];
    this.algoScenarios[scenarioName] = []; // these are the post-algo cranes
    delete this.algoMapByScenario[scenarioName] //= new Map(); // this is the raw algo output
    this.renewScenario(scenarioName);
    const { _scene } = this.editorViewport._renderer;
    cranesLib.renewCranes([], _scene);
    this.focusedCrane = null;
    this.editorViewport.render();
  }

  @action.bound updateFocusedCrane(scenarioName, e) {
    if (!this.cranes.length) return;
    this.selectCrane(e); // sets this.craneInfo
    const { radius, id, color } = this.craneInfo;

    const { uuid } = this.focusedCrane.data;
    const sceneCrane = this.scenarios[scenarioName].filter(crane => crane.uuid === uuid)[0];
    Object.assign(sceneCrane, this.craneInfo);
    
    this.renewScenario(scenarioName);
    const { _scene } = this.editorViewport._renderer;
    cranesLib.renewCranes(this.cranesByWindow(this.scenarios[scenarioName]), _scene, this.focusedCrane ? this.focusedCrane.data : null); // wasteful, but fine for now
    this.editorViewport.render();
    this.notifyAlgorithm(scenarioName);
  }

  @action.bound refreshAllCranes(scenarioName) {
    this.renewScenario(scenarioName);
    const { _scene } = this.editorViewport._renderer;
    // console.warn('!!', this.scenarios);
    cranesLib.renewCranes(this.cranesByWindow(this.scenarios[scenarioName] || []), _scene, this.focusedCrane ? this.focusedCrane.data : null); // wasteful, but fine for now
  }

  @action.bound selectCrane(e) {
    if (!this.cranes.length)  return;
    let crane, id;
    if (e) {
      id = e.target.value || e.target.attributes.value.value;
      crane = this.cranes.filter(crane => (crane['Crane ID'] || crane['ID']) === id)[0];
    } else {
      crane = this.cranes[0];
      id = crane['Crane ID'] || crane['ID'];
    }
    const radius = parseInt(crane['Maximum Radius'] || crane['Op. Radius']);
    // this.craneInfo is the "stamp" from which a new crane is created
    // It's old and probably should be got rid of
    this.craneInfo.radius = radius;
    this.craneInfo.id = id;
    this.craneInfo.color = this.createColor(id);
  } 

  theHuntForRedBindu(text) {
    // sums the ascii values of each character in the stat to use as seed
    const seededRandom = (seed) => {
      const x = Math.sin(seed) * 10000;
      return x - Math.floor(x);
    };
    let seed = text.split('').reduce( function(sum,item,i) { return sum + item.charCodeAt()*i+2 },0);
    const color = {
      r: parseInt(seededRandom(seed)*160+90),
      g: parseInt(seededRandom(++seed)*20+10),
      b: parseInt(seededRandom(++seed)*65+20)
   };
   return `rgb(${color.r}, ${color.g}, ${color.b})`
  };

  createColor(id) {
    // const goToSleep = {
    //   MB001: '#ff9900',
    //   CR001: '#00aa00',
    //   TC001: '#ff0099',
    //   TC002: '#ff4400',
    //   TC003: '#0099ff',
    // };
    const willSleepWhenImDead = {
      MB001: '#e8c620',
      CR001: '#fd7e7e',
      TC001: '#ff8200',
      TC003: '#ac0000',
      TC002: '#c86704',
    };
    return willSleepWhenImDead[id] || this.theHuntForRedBindu(id);
  }

  setTitle(newTitle) {
    this.title = newTitle;
  }
  
}
