const { hostname, port } = window.location;
const config = {
  url: `http://${hostname}:${port}`,
  flux_url: 'http://localhost:3000/api', // flux url
  flux_client_id: '6fe335b6-7eba-4606-ad2c-9adee2363464', // your app's client id
}
export default config;