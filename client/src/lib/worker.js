import * as algorithm from 'algorithm/src/algorithm';

console.log = console.warn = function() {};

const WEEKLIES_DELETIONS = [
  'jobsDone', 
  'totalEltLifted', 
  'totalEltPlanned', 
  'number', 
  'jobsDoneCount', 
  'jobsInProgress', 
  'jobsInProgressCount',
  'avgCoverage',
  'avgDelay',
  'avgTime',
  'cost', // thisis costBreakdown now
];

const transforms = {
  // NOTE: this is mutating in place
  runAlgorithm: (output) => {
    const { cranes, weeklyDP } = output;
    for (let uuid in cranes) delete cranes[uuid].schedule;
    for (let week in weeklyDP) 
      WEEKLIES_DELETIONS.forEach(key => delete weeklyDP[week][key]);
    return output;
  }
};

onmessage = function(e) {
  const { action, args } = e.data;
  if (Object.keys(algorithm).includes(action)) {
    try {
      const output = algorithm[action](...args);
      if (transforms[action]) transforms[action](output);
      postMessage({ action, output });
    } catch(error) {
      console.warn('ERROR in algorithm worker: ', error);
      postMessage({ action, error, output: null });
    }
  }
};
