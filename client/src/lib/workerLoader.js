const MyWorker = require('worker-loader!lib/worker');
const worker = new MyWorker();

function _addResolveListener(target, resolve, reject) {
  worker.addEventListener('message', resolver);
  function resolver(e) {
    const { error, action, output } = e.data;
    if (action === target) {
      worker.removeEventListener('message', resolver);
      return !error ? 
        resolve(output) :
        reject(error); 
    }
  }
}

export function getModelInputs(pickupLoc, jobListing, craneTypes, elementStock) {
  const action = 'getModelInputs';
  return new Promise((resolve, reject) => {
    _addResolveListener(action, resolve, reject);
    worker.postMessage({
      action,
      args: [].slice.call(arguments),
    });
  });
}

export function runAlgorithm(algoCranes) {
  const action = 'runAlgorithm';
  return new Promise((resolve, reject) => {
    _addResolveListener(action, resolve, reject);
    worker.postMessage({
      action,
      args: [].slice.call(arguments),
    });
  });
}
