export const summary = [
  { 
    '#': 1,
    type: 'Type A',
    'start date': '8/7/2017',
    'end date': '8/25/2017',
    utilization: 0.23,
  },
  { 
    '#': 2,
    type: 'Type B',
    'start date': '9/11/2017',
    'end date': '9/27/2017',
    utilization: 0.92,
  },
  { 
    '#': 3,
    type: 'Type C',
    'start date': '8/10/2017',
    'end date': '8/20/2017',
    utilization: 0.12,
  },
  { 
    '#': 4,
    type: 'Type C',
    'start date': '8/11/2017',
    'end date': '8/27/2017',
    utilization: 0.34,
  },
];

const randomJobListing = () => {
  const { floor, random } = Math;
  const verbs = `ship arrange organize concatenate distribute move adjust`.split(' ').map(verb => verb[0].toUpperCase() + verb.slice(1));
  const nouns = `widgets nails screws walls doors dirt tiles pillows`.split(' ');
  const activity = `${verbs[floor(random()*verbs.length)]} ${nouns[floor(random()*nouns.length)]}`;  
  const num = floor(random()*8+1);
  const char = String.fromCharCode(65+ floor(random()*7));
  return `${char}00${num}, ${activity}, 8/7/2017, 8/25/2017, 7 days, True, ${char}00${num-1}, ${char}00${num+1}, True`;
};
  
const jobKeys = `id, description, start date, end date, duration, critical path, predecessor id, successor id, cost flag`.split(', ');

const objectify = (job, keys) => {
  const jobArray = job.split(', ');
  const obj = {};
  keys.forEach((item, i) => obj[keys[i]] = jobArray[i]);
  return obj;
};

const craneKeys = `id type, product, op.radius (m), life capacity, inst. cost ($), op. cost ($/week), mobility`.split(', ');
const craneStrings = `
  A, Tower Crane - #00as12 XXL, 10.000, 15.000, 12.000, 6.000, Fixed
  B, Tower Crane - #000as13 XL, 8.000, 15.000, 12.000, 7.000, Fixed
  C, Mobile Crawler Crane - #QWE456, 7.000, 8.000, 20.000, 8.000, Mobile
  D, All Terrain Mobile Crane - #145asd, 4.000, 10.000, 24.000, 9.000, Semi
  E, All Terrain Mobile Crane - #145asd, 4.000, 10.000, 24.000, 9.000, Semi
`.trim().split('\n').map(line => line.trim());

const stockKeys = `category, type #, object #, avg weikght (lbs), to lift`.split(', ');
const stockStrings = `
  Floors, 12, 12, 25.000, True
  Windows, 13, 13, 800, True
  Str. Beams, 24, 24, 6.300, True
  Str. Connections, 32, 32, 1.200, True
  Str. Columns, 10, 10, 8.500, True
  Str. Frames, 7, 7, 4.500, True
`.trim().split('\n').map(line => line.trim());

export const jobs = Array(10).fill().map(() => objectify(randomJobListing(), jobKeys));
export const cranes = craneStrings.map(crane => objectify(crane, craneKeys));
export const stock = stockStrings.map(item => objectify(item, stockKeys));