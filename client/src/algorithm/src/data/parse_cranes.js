import {Worker, WorkerDescription} from "./class.js"

//EXPORTED Function - Parsing Crane types JSON table coming from Flux
function getCraneTypes(craneListingJson) {
    let craneList = parseCraneTypes(craneListingJson)
    return craneList;
}

function parseCraneTypes(craneListingJson) {
	let craneHeaders = craneListingJson[0]
	let craneListed = craneListingJson.splice(1, craneListingJson.length)
	let craneList = {}
	for(var i=0; i<craneListed.length; i++){
		var obj = craneListed[i]

		// Unit conversion
	    let unitConvert = {
	      feet: 1,
	      inch: 0.083333336,
	      meters: 3.28084,
	      centimeters: 0.0328084,
	      millimeters: 0.00328084
	    }

		// Retrieving data from the JSON table
		let id = obj[craneHeaders.indexOf("Crane ID")]
		
		// Creating product description
		let type = obj[craneHeaders.indexOf("Type")]
		let product = obj[craneHeaders.indexOf("Product Description")]
		let make = obj[craneHeaders.indexOf("Make")]
		let model = obj[craneHeaders.indexOf("Model")]
		let description = new WorkerDescription(product, make, model)

		// Getting crane properties
		let opHeight = parseFloat(obj[craneHeaders.indexOf("Operating height")])
		let radiusMin = unitConvert["meters"]*parseFloat(obj[craneHeaders.indexOf("Minimum Radius")])	// m --> ft
		let radiusMax = unitConvert["meters"]*parseFloat(obj[craneHeaders.indexOf("Maximum Radius")])	// m --> ft
		let maxWindSpeed = parseFloat(obj[craneHeaders.indexOf("Maximum in-service wind speeds")])		// m/s
		let contingency = parseFloat(obj[craneHeaders.indexOf("Contingency")])							// hrs/month
		let serviceability = parseFloat(obj[craneHeaders.indexOf("Serviceability")])					// hrs/month

		// Parsing Crane Costs
		let instCostString = obj[craneHeaders.indexOf("Installation cost")]
		let opCostString = obj[craneHeaders.indexOf("Operating cost")]	
		let instCost = 1000*parseFloat(instCostString.slice(1, instCostString.length))		// £
		let opCost = 1000*parseFloat(opCostString.slice(1, opCostString.length))			// £/day 

		// Estimating Lift Capacity
		let liftCapaMinVal = obj[craneHeaders.indexOf("Lift Capacity @ Min. Radius")]		// m/min
		let liftCapaMaxVal = obj[craneHeaders.indexOf("Lift Capacity @ Max. Radius")]		// m/min
		var liftCapaMinK = parseFloat(liftCapaMinVal)
		var liftCapaMaxK = parseFloat(liftCapaMaxVal)
		var liftCapaMinH = liftCapaMinVal.substring(liftCapaMinVal.indexOf(',') + 1, liftCapaMinVal.indexOf('k'))
		var liftCapaMaxH = liftCapaMaxVal.substring(liftCapaMaxVal.indexOf(',') + 1, liftCapaMaxVal.indexOf('k'))
		let liftCapaMin = 1000*liftCapaMinK + parseFloat(liftCapaMinH)
		let liftCapaMax = 1000*liftCapaMaxK + parseFloat(liftCapaMaxH)

		// Computing Operating Speed of the crane (average of both for simplicity)
		let speedPlan = parseFloat(obj[craneHeaders.indexOf("Operating Speed (plan)")])
		let speedVert = parseFloat(obj[craneHeaders.indexOf("Operating Speed (vertical)")])
		let opSpeed = unitConvert["meters"]*0.5*(speedPlan + speedVert)				// m/min --> ft/millisecond

		// Creating Job objects
		let worker = new Worker(id, description, type, opHeight, radiusMin, radiusMax, liftCapaMax, liftCapaMin, opSpeed, maxWindSpeed, instCost, opCost, contingency, serviceability)
		craneList[id] = worker
	}
    return craneList;
}

//EXPORTED Function - Generate a JSON Table based on the array of cranes
function getCraneTable(craneTypes){
	let craneIDs = Object.keys(craneTypes)
	let craneNum = craneIDs.length
	let table = []
	// Table Headers
	let headers = ["ID", "Product Description", "Type", "Op. Radius", "Op. Speed", "Lift Capacity", "Max Wind Speed", "Op. Cost", "Inst. Cost", "Serviceability", "Contingency"]
	let headerTag = ["id", "description", "type", "radiusMax", "opSpeed", "liftCapaMax", "maxWindSpeed", "opCost", "instCost", "serviceability", "contingency"]
	table.push(headers)
	for (var i=0; i<craneNum; i++){
		let crane = craneTypes[craneIDs[i]]
		let craneJSON = []
		for (var j=0; j<headers.length; j++){
			let obj = crane[headerTag[j]]
			if (headerTag[j] == "description"){
				let curr = obj.make + " - " + obj.model
				craneJSON.push(curr)
			} else if (headerTag[j] == "radiusMax"){
				let curr = Math.round(obj/3.28084) + " m"
				craneJSON.push(curr)
			} else if (headerTag[j] == "opSpeed"){
				let curr = Math.round(obj/3.28084) + " m/min"
				craneJSON.push(curr)
			} else if (headerTag[j] == "liftCapaMax"){
				let curr = obj.toLocaleString() + " lbs"
				craneJSON.push(curr)
			} else if (headerTag[j] == "maxWindSpeed"){
				let curr = obj + " m/s"
				craneJSON.push(curr)
			} else if (headerTag[j] == "opCost"){
				let curr = obj.toLocaleString() + " £/day"
				craneJSON.push(curr)
			} else if (headerTag[j] == "instCost"){
				let curr = obj.toLocaleString() + " £"
				craneJSON.push(curr)
			} else if (headerTag[j] == "serviceability" || headerTag[j] == "contingency"){
				let curr = obj + " hrs"
				craneJSON.push(curr)
			} else {
				craneJSON.push(obj)
			}
		}
		table.push(craneJSON)
	}
	return table
}

export {getCraneTypes, getCraneTable}