import {Location} from "./class.js"

//EXPORTED Function - Parsing Pickup Locations JSON Data coming from Flux
export default function getPickupLocations(pickupListingJson) {
    let pickupList = parsePickupLocations(pickupListingJson)
    return pickupList;
}

function parsePickupLocations(jsonObjects){
  let eltList = [];

  for (var i=0; i< jsonObjects.length; i++){
    let object = jsonObjects[i]

    // Extracting object data
    let id = object.fluxId
    let XYZ = object.geometryParameters.curve.origin
    let unit = object.geometryParameters.curve.units.origin
    
    // Unit conversion
    let unitConvert = {
      feet: 1,
      inch: 0.083333336,
      meters: 3.28084,
      centimeters: 0.0328084,
      millimeters: 0.00328084
    }
    let c = unitConvert[unit]
    let location = XYZ.map(function(x) { return x * c; });
    
    // Creating Element 
    let newElt = new Location(id, location)
    eltList.push(newElt)
  }
  return eltList;
}
