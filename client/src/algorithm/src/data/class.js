// Defining all object classes needed for the algorithm

// Lift jobs to perform
class Job {
  constructor(id, description, type, zone, level, start, end, duration, float, predecessor, successor = "", critical_flag = 0) {
    this.id = id
    this.description = description
    this.type = type
    this.zone = zone
    this.level = level
    this.start = start
    this.end = end
    this.duration = duration
    this.float = float
    this.predecessor = predecessor
    this.successor = successor
    // Flags
    this.critical_flag = critical_flag
    // Attributes to fill during algorithm
    this.status = 0 // 0: not done, 1: processing, 2: done
    this.flag = false
    this.elements = []
    this.craneTime = []
    this.expStart = new Date()
    this.expEnd = new Date()
    this.expDelay = 0
    this.expTime = 0
    this.expCost = 0
    this.expRisk = 0
    this.liftTime = 0
    this.minLiftTime = 0
    this.baseline_time = 0
    this.baseline_cost = 0
    this.baseline_risk = 0
    this.KPI_time = -1
    this.KPI_cost = -1
    this.KPI_risk = -1
    this.coverage = 0
  }
}

// Crane used onsite
class Worker {
  constructor(id, description, type, opHeight, radiusMin, radiusMax, liftCapaMin, liftCapaMax, opSpeed, maxWindSpeed, instCost, opCost, contingency, serviceability) {
    this.id = id
    this.description = description
    this.type = type
    this.opHeight = opHeight
    this.radiusMin = radiusMin
    this.radiusMax = radiusMax
    this.liftCapaMin = liftCapaMin
    this.liftCapaMax = liftCapaMax
    this.opSpeed = opSpeed
    this.maxWindSpeed = maxWindSpeed
    this.instCost = instCost
    this.opCost = opCost
    this.contingency = contingency
    this.serviceability = serviceability
  }
}

class WorkerDescription {
    constructor(product, make, model){
        this.product = product
        this.make = make
        this.model = model
    }
}

// Element to be lifted
class Element {
  constructor(id, zone, level, family, dropoffXYZ, cost, weight, toLift = 1, pickupXYZ = [0,0,0], travelDist = 0, critical = 0){
    this.id = id
    this.zone = zone
    this.level = level
    this.family = family
    this.dropoffXYZ = dropoffXYZ
    this.pickupXYZ = pickupXYZ
    this.travelDist = travelDist
    this.cost = cost
    this.weight = weight
    this.critical = critical
    this.toLift = toLift
  }
}

// Element categorisation family
class Family {
  constructor(category, family, type){
    this.category = category
    this.family = family
    this.type = type
  }
}

// Location of an object onsite
class Location {
  constructor(id, location = [0,0,0]){
    this.id = id
    this.location = location
  }
}

// Weekly data point
class Weather {
  constructor(id) {
    this.id = id
    this.avgWindSp = 0
    this.stdWindSp = 0
    this.hisWindSp = []
    this.cumWindSp = 0
    this.rayleighSigma = 0
    this.rayleighMean = 0
    this.rayleighStd = 0
    this.bandwidth = 0
  }
}

// Compute the week number in a given year according to ISO-8601
Date.prototype.getWeekISONumber = function(){
    var d = new Date(+this);
    d.setHours(0,0,0,0);
    d.setDate(d.getDate()+4-(d.getDay()||7));
    return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
}

export {Job, Worker, WorkerDescription, Element, Family, Location, Weather}