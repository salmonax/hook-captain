import {Weather} from './class';
//var londonData = require('./_londonWeatherData.json');

function getWeatherData(database){
	let Weather = parseDatabase(database)
	return Weather
}

// Data set contains duplicate data points
function parseDatabase(database){
	let uniqueDB = []
	let weeklyWindSp = {}
	let lengthDB = Object.keys(database).length
	//console.log(database[12])
	// Create an id based on the object time
	for (var i=0; i<lengthDB; i++){
		let date = new Date(database[i].dt*1000)
		let id = "dt-ID_" + date.getTime()/1000
		let weekNum = date.getWeekISONumber()
		if(!uniqueDB.includes(id)){
			uniqueDB.push(id)
			if(!weeklyWindSp[weekNum]){
				weeklyWindSp[weekNum] = new Weather(weekNum)
			}
			weeklyWindSp[weekNum].hisWindSp.push(database[i].wind.speed)
		}
	}
	let numISOWeeks = Object.keys(weeklyWindSp).length
	for (var i=1; i<=numISOWeeks; i++){
		// Running statistics on
		let hisWindSp = weeklyWindSp[i].hisWindSp
		let numWeekDP = hisWindSp.length
		let stats = computeStats(hisWindSp)
		weeklyWindSp[i].cumWindSp = stats.sum
		weeklyWindSp[i].avgWindSp = Math.round(stats.mean*1000)/1000
		weeklyWindSp[i].stdWindSp = Math.round(stats.std*1000)/1000
		weeklyWindSp[i].bandwidth = [Math.round(stats.band[0]*1000)/1000,Math.round(stats.band[1]*1000)/1000]
		// Estimating the underlying probability distribution
		let sigma = rayleighEstimator(hisWindSp)
		weeklyWindSp[i].rayleighSigma = Math.round(sigma*1000)/1000
		weeklyWindSp[i].rayleighMean = Math.round(1.253*sigma*1000)/1000
		weeklyWindSp[i].rayleighStd = Math.round(Math.sqrt(0.429*sigma*sigma)*1000)/1000
	}
	//console.log(weeklyWindSp)
	return weeklyWindSp
}

// Compute the statistical mean and variance of a dataset
function computeStats(data){
	let len = data.length
	let sum = 0
	let sum_sq = 0
	for (var i=0; i<len; i++){
		sum += data[i]
		sum_sq += data[i]*data[i]
	}
	let mean = sum/len
	let variance = sum_sq/len - mean*mean
	let std = Math.sqrt(variance)
	let band = [mean-0.5*std, mean+0.5*std]
	return{
		mean: sum/len,
		var: variance,
		std: std,
		sum: sum,
		band: band
	}
}

// Parameter estimation for a Rayleigh probability density function using MLE method
function rayleighEstimator(data){
	let sq_sum = 0
	let N = data.length
	for (var i=0; i<N; i++){
		sq_sum += data[i]*data[i]
	}
	return Math.sqrt((1/(2*(N-1)))*sq_sum)
}

// Probability density function for the Rayleigh distribution
function rayleighPDF(x, sigma){
	let prob = (x/(sigma*sigma))*Math.exp(-(x*x)/(2*sigma*sigma))
	return prob
}

export {getWeatherData, rayleighPDF, rayleighEstimator}