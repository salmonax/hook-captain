import {Job} from "./class.js"
var moment = require('moment')
var Papa = require('papaparse')

//EXPORTED Function - Parsing Job Listing JSON table coming from Flux
function getJobListing(jobListingJson) {
    let jobList = parseJobListing(jobListingJson)
    return jobList;
}

function parseJobListing(jobListingJson) {
	var jobHeaders = jobListingJson[0]
	var jobListed = jobListingJson.splice(2, jobListingJson.length)
	var firstJob = jobListed[0][jobHeaders.indexOf("task_code")]
	var jobList = {}

	// Get project timeline
	var projectTimeline = getProjectTimeline(jobListed, jobHeaders)	
	var projectDates = {
		projectStart: projectTimeline.start,
		projectEnd: projectTimeline.end,
		concreteStart: null,
		steelStart: null,
		expectedEnd: null
	}
	var projectConcreteStartDate = null
	var projectSteelworkStartDate = null
	
	for(var i=0; i<jobListed.length; i++){
		let obj = jobListed[i]

		// Retrieving data from the JSON table
		let id = obj[jobHeaders.indexOf("task_code")]
		let wbsCode = obj[jobHeaders.indexOf("wbs_id")]
		let description = obj[jobHeaders.indexOf("task_name")]
		let float = obj[jobHeaders.indexOf("total_float_hr_cnt")]
		let critical_flag = 1
		let obj_critFlag = obj[jobHeaders.indexOf("critical_flag")]
		
		// Understanding task description
		let taskDesc = parseTaskDescription(wbsCode, description)
		let type = taskDesc.type
		let zone = taskDesc.zone
		let level = taskDesc.level	

		// Parse through the list of succ / predecessor tasks
		let pred = parseLinkedTasks(obj[jobHeaders.indexOf("pred_list")])
		let succ = parseLinkedTasks(obj[jobHeaders.indexOf("succ_list")])

		// Parsing Task Start and End dates
		let start = parseDate(obj[jobHeaders.indexOf("target_start_date")])
		let end = parseDate(obj[jobHeaders.indexOf("target_end_date")])
		if (!start){
			start = projectDates.projectEnd
		}
		if (!end){
			end = projectDates.projectEnd
		}

		// Estimating the task's duration
		let duration = 0
		if (end != null && start != null){
			duration = (end - start)/8.64e+7			
		}

		// Checking input validity
		if(!id){
			id = i + "#" + randomString(4) // Generate random id if not inputted
		}
		if (!float){
			float = 0 // Assume no tolerance if not inputted
		}
		if (!pred || pred == "-" ){
			pred = "" // No predecessor task needed if not inputted
		}		
		if (!succ || succ == "-" ){
			succ = "" // No successor task needed if not inputted
		}
		if (!obj_critFlag || obj_critFlag == "-" || obj_critFlag == "N"){
			critical_flag = 0
		}

		// Checking for steel & concrete work start date
		if (type.work == "concrete"){
			if (projectConcreteStartDate == null || projectConcreteStartDate > start){
				projectConcreteStartDate = start
			}
		} else if (type.work == "steel"){
			if (projectSteelworkStartDate == null || projectSteelworkStartDate > start){
				projectSteelworkStartDate = start
			}
		}
	
		// Creating Job objects
		let job = new Job(id, description, type, zone, level, start, end, duration, float, pred, succ, critical_flag)
		jobList[id] = job
	}
	// Updating project timeline
	projectDates.concreteStart = projectConcreteStartDate
	projectDates.steelStart = projectSteelworkStartDate
    return {
    	projectDates: projectDates,
    	firstJob: firstJob,
    	jobList: jobList
    }
}

// Generate a random string for the tasks ID
function randomString(length) {
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

// Parse the successor and predecessor list of each jobs
function parseLinkedTasks(taskList) {
	let parsedList = []
	if (taskList != null){
		var list = Papa.parse(taskList).data[0]
		for(var i=0; i<list.length; i++){
			list[i] = list[i].replace(/ /g,'')
		}
		parsedList = list
	}
	return parsedList
}

// Parse the task description into type, zone and level based on the Flux JSON
function parseTaskDescription(wbsCode, description) {
	let type = {}
	let zone = ""
	let level = []
	if(wbsCode.includes("Concrete Cores")){
		type["work"] = "concrete"
		type["element"] = "cores"
	} else if (wbsCode.includes("Structure.Block")){
		type["work"] = "steel"
		zone = wbsCode.substr(wbsCode.length - 1)
		if (description.includes("Beams") || description.includes("Frames")){
			type["element"] = "Structural Framing"
			level = (description.replace(/L/g,'Level ')).match(/Level [0-9]+/g)
		} else if (description.includes("Columns")){
			type["element"] = "Structural Columns"
			level = (description.replace(/L/g,'Level ')).match(/Level [0-9]+/g)
		} else {
			type ["element"] = ""
		}
	}
	return {
		type: type,
		zone: zone,
		level: level
	}
}

// Parse date from the Flux JSON
function parseDate(input) {
	var date = null;
	if (isFinite(input) && input != null){
		let dateInit = 25568 //Absolute 0 for javascript
		let millisecondsInDay = 8.64e+7
		date = new Date((input - dateInit)*millisecondsInDay)
	}
	else if (input != null) {
		let euro_date = input.substring(0, 10)
		let hours = parseInt(input.substring(11,13))
		euro_date = euro_date.split('/');
		let us_date = euro_date.reverse().join('/');
		let day_date = new Date(us_date)
		date = new Date(day_date.setHours(hours))
	}
	return date
}

// Computes the projects timeline based on the list of jobs
function getProjectTimeline(jobList, jobHeaders) {
	let projectStartDate = null
	let projectEndDate = null
	for(var i=0; i<jobList.length; i++){
		let obj = jobList[i]

		// Parsing through the data string inputted
		let start = parseDate(obj[jobHeaders.indexOf("target_start_date")])
		let end = parseDate(obj[jobHeaders.indexOf("target_end_date")])

		// Updating current expected project start / end date
		if (projectStartDate == null || start < projectStartDate){
			projectStartDate = start
		}
		if (projectEndDate == null || end > projectEndDate){
			projectEndDate = end
		}
	}
	return{
		start: projectStartDate,
		end: projectEndDate
	}
}

//EXPORTED Function - Generate a JSON Table based on the array of Jobs
function getJobTable(jobListing){
	let jobList = jobListing.jobList
	let jobIDs = Object.keys(jobList)
	let jobNum = jobIDs.length
	let table = []
	// Table Headers
	let headers = ["ID", "Description", "Zone", "Start Date", "End Date", "Duration", "Predecessor ID", "Successor ID", "Critical"]
	let headerTag = ["id", "description", "zone", "start", "end", "duration", "predecessor", "successor", "critical_flag"]
	table.push(headers)
	for (var i=0; i<jobNum; i++){
		let job = jobList[jobIDs[i]]
		let jobJSON = []
		for (var j=0; j<headers.length; j++){
			let obj = job[headerTag[j]]
			if (typeof obj.getMonth === 'function'){
				let curr = moment(obj).format('L')
				jobJSON.push(curr)
			} else if (Array.isArray(obj)){
				let curr = obj.join(" - ")
				jobJSON.push(curr)
			} else if (headerTag[j] == "critical_flag"){
				let curr = (obj == 1)? "True":"False"
				jobJSON.push(curr)
			} else if (headerTag[j] == "duration"){
				jobJSON.push(Math.ceil(obj) + " days")
			} else {
				jobJSON.push(obj)
			}
		}
		table.push(jobJSON)
	}
	return table
}

export {getJobListing, getJobTable}