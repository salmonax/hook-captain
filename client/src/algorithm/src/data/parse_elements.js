import {Family, Element} from "./class.js"
import getPickupLocations from "./parse_pickups.js"

function getElementStocks(revitObjectsJson, pickups) {
    let eltList = loadRevitData(revitObjectsJson, pickups);
    return eltList;
}

// Parsing Revit Elements JSON data coming from Flux
function loadRevitData(jsonObjects, pickupLocs){
  let eltList = {}
  let zone = ""
  let level = ""
  for (var i=0; i< jsonObjects.length; i++){
    let object = jsonObjects[i]

    // Identifying object
    let id = object.fluxId
    if (object.instanceParameters.Zone) {
      zone = object.instanceParameters.Zone
      level = object.geometryParameters.level
    }
    let family_category = object.familyInfo.category
    let family_family = object.familyInfo.family
    let family_type = object.familyInfo.type

    // Locating object in the model
    let level = object.geometryParameters.level
    if (!level){
      level = object.geometryParameters.topLevel
    }
    let modelLoc = object.geometryParameters.location
    let locType = "location"
    if (modelLoc == undefined){
      modelLoc = object.geometryParameters.profile
      locType = "profile"
    }

    // Unit conversion
    let unitConvert = {
      feet: 1,
      inches: 0.083333336,
      meters: 3.28084,
      centimeters: 0.0328084,
      millimeters: 0.00328084
    }

    // Find dropoff & pickup locations for each object
    let XYZ = estimateObjectLocation(modelLoc, locType)
    let dropoffXYZ = (XYZ.value).map(function(x) { return x * unitConvert[XYZ.unit]; });
    let pickup = findPickupLocation(pickupLocs, dropoffXYZ)
    let pickupXYZ = pickup.XYZ
    let travelDist = pickup.dist
    
    // Checking is object is steel work or concrete
    let structMaterial = object.instanceParameters["Structural Material"]
    if (!structMaterial){
      structMaterial = object.typeParameters["Structural Material"]
    }
    let toLift = 1
    if (structMaterial && structMaterial.includes("Concrete")){
      toLift = 0
    }

    // Estimating object cost for steel work (concrete elements won't be lifted)
    let costData = getCostData(family_category, structMaterial)
    let cost = 0
    if(costData.measure == "linear") {
      if (family_category == "Structural Columns"){
        let length = getColumnLength(object, unitConvert)
        cost = length*costData.value
      } else {
        let c = unitConvert[object.units["instanceParameters/Cut Length"]]
        cost = c*costData.value*object.instanceParameters["Cut Length"]
      }
    } else if (costData.measure == "area") {
      let unit = object.units["instanceParameters/Area"]
      var c = unitConvert[unit.substring(0, unit.indexOf('*'))]
      cost = c*c*costData.value*object.instanceParameters["Area"]
    } else if (costData.measure == "volume") {
      let unit = object.units["instanceParameters/Volume"]
      var c = unitConvert[unit.substring(0, unit.indexOf('*'))]
      cost = c*c*c*costData.value*object.instanceParameters["Volume"]
    } else {
      cost = costData.value
    }
    
    // Estimating object weight (applied to all elements)
    var volumeUnit = object.units["instanceParameters/Volume"]
    var c = unitConvert[volumeUnit.substring(0, volumeUnit.indexOf('*'))]
    let volumeValue = object.instanceParameters["Volume"]
    let weightValue = getMatProp(structMaterial)
    let weight = c*c*c*weightValue*object.instanceParameters["Volume"]
    
    // Creating Element 
    let family = new Family(family_category,family_family,family_type)
    let newElt = new Element(id, zone, level, family, dropoffXYZ, cost, weight, toLift, pickupXYZ, travelDist)
    
    // Ordering Elements per zone & levels
    if(!Object.keys(eltList).includes(zone)){
      eltList[zone] = {}
    }
    if(!Object.keys(eltList[zone]).includes(level)){
      eltList[zone][level] = []
    }
    eltList[zone][level].push(newElt)
  }
  return eltList
}

// Cost database per object types
function getCostData(category, structMaterial){
  // Costs Values are given in £
  // Costs Units can be linear, area, volume or unit
  let costDataBase = {
    "Structural Framing": {
      cost:{
        "Steel ASTM A36": 15, // £/ft
        "Steel ASTM A572": 17, // £/ft
        "Steel ASTM A992": 20,  // £/ft
        "Steel ASTM A500, Grade B, Rectangular and Square": 12, // £/ft
        "Steel ASTM A500, Grade B, Round": 13, // £/ft
        "Concrete, Cast-in-Place gray": 50,  // £/cbf
        "Concrete, Lightweight": 50 // £/cbf
      },
      measure: {
        "Steel ASTM A36": "linear",
        "Steel ASTM A572": "linear", // £/ft
        "Steel ASTM A992": "linear",  // £/ft
        "Steel ASTM A500, Grade B, Rectangular and Square": "linear", // £/ft
        "Steel ASTM A500, Grade B, Round": "linear",
        "Concrete, Cast-in-Place gray": "volume",  // £/cbf
        "Concrete, Lightweight": "volume" // £/cbf
      }
    },
    "Structural Columns": {
      cost:{
        "Steel ASTM A36": 15, // £/ft
        "Steel ASTM A572": 17, // £/ft
        "Steel ASTM A992": 20,  // £/ft
        "Steel ASTM A500, Grade B, Rectangular and Square": 12, // £/ft
        "Steel ASTM A500, Grade B, Round": 13, // £/ft
        "Concrete, Cast-in-Place gray": 50,  // £/cbf
        "Concrete, Lightweight": 50 // £/cbf
      },
      measure: {
        "Steel ASTM A36": "linear", // £/ft
        "Steel ASTM A572": "linear", // £/ft
        "Steel ASTM A992": "linear",  // £/ft
        "Steel ASTM A500, Grade B, Rectangular and Square": "linear", // £/ft
        "Steel ASTM A500, Grade B, Round": "linear", // £/ft
        "Concrete, Cast-in-Place gray": "volume",  // £/cbf
        "Concrete, Lightweight": "volume" // £/cbf
      }
    }
  }
  // Dealing with elements which are not registered in the database
  let cost = 500; // £
  let measure = "unitary" // Price per item
  let costCat = costDataBase[category]
  if (costCat){
    let costValue = costCat.cost[structMaterial]
    let unitValue = costCat.measure[structMaterial]
    if (costValue && unitValue){
      cost = costValue
      measure = unitValue
    }
  }
  return {
    value: cost,
    measure: measure
  }
}

// Compute the length of Structural Columns object from Revit
function getColumnLength(object, unitConvert){
  let volume = object.instanceParameters["Volume"]
  let volumeUnit = object.units["instanceParameters/Volume"]
  let area = object.typeParameters["Section Area"]
  let areaUnit = object.units["typeParameters/Section Area"]
  if (!area){
    area = object.typeParameters["A"]
    areaUnit = object.units["typeParameters/A"]
    ac = unitConvert[areaUnit.substring(0, areaUnit.indexOf('*'))]
  }
  (areaUnit == "inches*inches")? areaUnit = "feet*feet":null
  let ac = unitConvert[areaUnit.substring(0, areaUnit.indexOf('*'))]
  let vc = unitConvert[volumeUnit.substring(0, volumeUnit.indexOf('*'))]
  return volume*vc*vc*vc/(area*ac*ac)
}

// Material properties database per structural material types
function getMatProp(structMaterial){
  // Material Weight is given in pds/cbf
  let matWeightDataBase = {
    "Steel ASTM A572": 490,
    "Steel ASTM A992": 490,
    "Concrete, Cast-in-Place gray": 150,
    "Concrete, Lightweight": 200
  }
  // Dealing with elements which are not registered in the database
  var defaultWeight = 250;
  var value = matWeightDataBase[structMaterial]
  var weight = (value != undefined) ? value : defaultWeight
  return weight
}

// Estimating object dropoff location onsite based on the model location
function estimateObjectLocation(modelLoc, locType){
  let dropoffXYZ = []
  let unit = ""
  if (locType == "location"){
    if (modelLoc.primitive == "line"){
      var startXYZ = modelLoc.start
      var endXYZ = modelLoc.end
      var sumXYZ = startXYZ.map((a, i) => a + endXYZ[i])
      dropoffXYZ = sumXYZ.map((x) => x / 2)
      unit = modelLoc.units.start
    } else if (modelLoc.primitive == "point"){
      dropoffXYZ = modelLoc.point
      unit = modelLoc.units.point
    } else {
      throw "Element location type not supported"
    }
  } else {
    if (modelLoc[0].primitive == "line"){
      let sumXYZ = modelLoc[0].start
      for (var i = 1; i < modelLoc.length; i++){
        var arrCur = modelLoc[i].start
        var arrTemp = sumXYZ.map((a, i) => a + arrCur[i])
        sumXYZ = arrTemp
      }
      dropoffXYZ = sumXYZ.map((x) => x / modelLoc.length)
      unit = modelLoc[0].units.start
    } else {
      throw "Element location type not supported"
    }
  }
  return {
    value: dropoffXYZ,
    unit: unit
  }
}

function findPickupLocation(pickupLoc, dropoffXYZ) {
  let pickupXYZ = []
  let distanceToPickup = null
  for(var i=0; i<pickupLoc.length; i++){
    let dist = euclidianDist(pickupLoc[i].location,dropoffXYZ)
    if (dist < distanceToPickup || !distanceToPickup){
      pickupXYZ = pickupLoc[i].location
      distanceToPickup = dist
    }
  }
  return {
    XYZ: pickupXYZ,
    dist: distanceToPickup
  }
}

function euclidianDist(A,B){
  if (A.length != B.length){
    return undefined
  }
  let dist = 0
  for (var i=0; i<A.length; i++){
    dist += (A[i]-B[i])*(A[i]-B[i])
  }
  return Math.sqrt(dist)
}

// Merge parsed elements into a single object
function mergeParsedElements(parsedElts){
  let eltList = parsedElts[0]
  for(var i=1; i<parsedElts.length; i++){
    let currElts = parsedElts[i]
    let zones = Object.keys(currElts)
    for (var z=0; z<zones.length; z++){
      let zone = zones[z]
      let levels = Object.keys(currElts[zone])
      for (var l=0; l<levels.length; l++){
        let level = levels[l]
        if(!Object.keys(eltList).includes(zone)){
          eltList[zone] = {}
        }
        if(!Object.keys(eltList[zone]).includes(level)){
          eltList[zone][level] = []
        }
        eltList[zone][level] = eltList[zone][level].concat(currElts[zone][level])
      }
    }
  }
  return eltList
}

// Generate a JSON Table based on the array of elements
function getElementTable(eltStocks){
  let zones = Object.keys(eltStocks), levels = []
  let cat = [], fam = [], type = [], unique = []
  let counts = [], weight = []
  let tornadoChartData = {}
  let pieChartData = {}
  let total = 0
  // Table Headers
  let headers = ["Category", "Family", "Type", "Count", "Avg Weight (lbs)"]
  let table = [headers]
  for(var z=0; z<zones.length; z++){
    let zone = zones[z]
    pieChartData[zone] = 0
    let zoneLevel = Object.keys(eltStocks[zones[z]])
    for(var l=0; l<zoneLevel.length; l++){
      let level = zoneLevel[l]
      let currElts = eltStocks[zone][level]
      let eltNum = currElts.length
      if(!levels.includes(level)){
        levels.push(level)
        tornadoChartData[level] = {}
      }
      if(!tornadoChartData[level][zone]){
        tornadoChartData[level][zone] = 0
      }
      for(var i=0; i<eltNum; i++){
        let currFam = currElts[i].family
        let currUni = currFam.category + currFam.family + currFam.type
        pieChartData[zone] += 1
        tornadoChartData[level][zone] += 1
        total += 1
        if(!unique.includes(currUni)){
          unique.push(currUni)
          cat.push(currFam.category)
          fam.push(currFam.family)
          type.push(currFam.type)
          counts[currUni] = 0
          weight[currUni] = 0
        }
        let cumWeight = counts[currUni]*weight[currUni]
        counts[currUni] += 1
        weight[currUni] = Math.round((cumWeight + currElts[i].weight)/counts[currUni])
      }
    }
  }
  let values = [cat, fam, type, Object.values(counts), Object.values(weight)]
  let tableValues = values[0].map(function(col, i) { 
    return values.map(function(row) { 
      return row[i] 
    })
  });
  table = table.concat(tableValues)
  
  return {
    total: total,
    tornado: tornadoChartData,
    pie: pieChartData,
    table: table
  }
}

export {getElementStocks, mergeParsedElements, getElementTable}