// Defining all object classes needed for the algorithm

// Weekly data point
class Weekly {
  constructor(number, start, end) {
    this.number = number
    this.start = start
    this.end = end
    this.jobsDone = []
    this.jobsDoneCount = 0
    this.jobsInProgress = []
    this.jobsInProgressCount = 0
    this.totalEltLifted = 0
    this.totalEltPlanned = 0
    this.avgCoverage = 0
    this.totDelay = 0
    this.avgDelay = 0
    this.cumDelay = 0
    this.totTime = 0
    this.avgTime = 0
    this.cumTime = 0
    this.cost = new costDP(0,0,0,0,0)
    this.costBreakdown = new costDP(0,0,0,0,0)
    this.totCost = 0
    this.cumCost = 0
    this.totRisk = 1
    this.cumRisk = 0
    this.cumKPI_time = 0
    this.cumKPI_cost = 0
    this.cumKPI_risk = 0
    this.KPI_time = 0
    this.KPI_cost = 0
    this.KPI_risk = 0
  }
}

// Monthyl data point
class Monthly {
  constructor(number, month, year, start, end) {
    this.number = number
    this.month = month
    this.year = year
    this.start = start
    this.end = end
    this.cost = new costDP(0,0,0,0,0)
  }
}

// Cost data point
class costDP {
  constructor(instCost, opCost, delayCost, addCost, maintCost){
    this.instCost = instCost
    this.opCost = opCost
    this.delayCost = delayCost
    this.addCost = addCost
    this.maintCost = maintCost
  }
}

class weatherDP {
  constructor(avgWindSp, bandwidth){
    this.avgWindSp = avgWindSp
    this.bandwidth = bandwidth
    this.numOfCrane = 0
    this.probOfImpact = 0
    this.probNoCranes = 1
  }
}

// Crane object
class Crane {
  constructor(id, type, start, end, location) {
    this.id = id
    this.type = type
    this.location = location
    this.schedule = []
    this.availability = start
    this.start = start
    this.expStart = null
    this.expEnd = null
    this.lastMaintenance = start
    this.end = end
    this.activeTime = 0
    this.activity = 0
    this.utilization = 0
    this.timeToCheck = 30  // Mechanical check needed every 30 days
  }
}

// Priority Queue Class for Lift Jobs
function PriorityJob() {
  this.data = []
}

PriorityJob.prototype.push = function(elt, priority) {
  for (var i = 0; i < this.data.length && this.data[i][1].getTime() < priority.getTime(); i++);
  this.data.splice(i, 0, [elt, priority])
}

PriorityJob.prototype.pop = function() {
  return this.data.shift()[0]
}

PriorityJob.prototype.size = function() {
  return this.data.length
}

export {Weekly, Monthly, costDP, weatherDP, Crane, PriorityJob}