import {Weekly, Monthly, costDP, weatherDP, Crane, PriorityJob} from "./class.js"
import {getElementStocks, mergeParsedElements, getElementTable} from "./data/parse_elements.js"
import {getJobListing, getJobTable} from "./data/parse_jobs.js"
import {getCraneTypes, getCraneTable} from "./data/parse_cranes.js"
import getPickupLocations from "./data/parse_pickups"
import {getWeatherData, rayleighPDF} from "./data/parse_weather"
// var londonData = require('./data/_londonWeatherData.json')
// console.log(JSON.stringify(getWeatherData(londonData)))
var moment = require('moment')

// Instance variables needed for the algorihtm
var jobListing = null
var eltStocks = null
var craneTypes = null
var maxLiftSpeed = null
var minCraneCost = null
var pickupLocs = null
var jobList = null
var firstJob = null
var projectDates = null

// CONSTANTS FOR ALGORITHM
// ***Instance variables***
const hourlyRate = 14			// Hrly rate of const. workers
const jobDelayNegCost = 0.25		// Negative delay savings in £ k / day
const jobDelayPosCost = 20		// Positive delay costs in £ k / day
const projectDelayCost = 20		// Value in £ k / day
const projectSchedule = {
	startTime: 8,				// 24hrs clock (13 = 1PM)
	endTime: 17,				// 24hrs clock (13 = 1PM)
	startDay: 0,				// [0..6] for days of the week, aka 1 = Tuesday
	endDay: 5					// [0..6] for days of the week, aka 1 = Tuesday
}
const craneHookTime = 12 		// Fix time allocated to hooking in min
const craneunHookTime = 12		// Fix time allocated to un-hooking in min
const numOfCraneWorkers = 8		// Total number of construction workers per crane

// OUTPUT OF THE ALGORITHM
// *** Instance Variables ***
var dataPoints = null
var weeklyDP = null
var monthlyDP = null
var windDP = null
var scenarioMetrics = {}
var craneListing = 	{}

// LOADING JSON DATA
// *** TO COMMENT WHEN RUNNING THE APP
// *** USED FOR DEBUGGING THE ALGORITHM ONLY
// var revitJSON_1 = require('./data/_revitColumnsPodium.json');
// var revitJSON_2 = require('./data/_revitColumnsTowers.json');
// var pickupJSON = require('./data/_pickupLocation.json');
// var craneJSON = require('./data/_craneTypes.json');
// var jobsJSON = require('./data/_jobListing.json');

// Testing Algorithm
// getModelInputs(pickupJSON, jobsJSON, craneJSON)
// runAlgorithm()

function getModelInputs(pickups, jobs, cranes, elements) {
	// Parse JSON data for the algorithm
 	// pickupLocs = getPickupLocations(pickups)
	// jobListing = getJobListing(jobs)
	// craneTypes = getCraneTypes(cranes)
	// let eltStocks1 = getElementStocks(revitJSON_1, pickupLocs)
	// let eltStocks2 = getElementStocks(revitJSON_2, pickupLocs)
	// eltStocks = mergeParsedElements([eltStocks1, eltStocks2])
	pickupLocs = pickups
	jobListing = jobs
	craneTypes = cranes
	eltStocks = elements

	// Extracting project variables based on the job schedule
	jobList = jobListing.jobList
	firstJob = jobListing.firstJob
	projectDates = jobListing.projectDates

	// Extracting project baseline variables for KPI calculations
	maxLiftSpeed = getMaxLiftSpeed(craneTypes)
	minCraneCost = getMinCraneCost(craneTypes)

	return projectDates
}

function runAlgorithm(preparedCranes){
	let noCraneFlag = false
	let inactiveCranes = []
	resetInstanceVariables()

	// Initialize algorithm
	dataPoints = initializeDP()
	weeklyDP = dataPoints.weekly
	monthlyDP = dataPoints.monthly

	// Converting crane data from the UI
	if (preparedCranes.length){
		noCraneFlag = true
		for (var c in preparedCranes){
			// Need to convert to feet
			// HARD CODED FOR NOW - TO BE IMPROVED
			let c_ft = 3.28084
			let crane = preparedCranes[c]
			let location = [crane.location[0]*c_ft,crane.location[1]*c_ft,0]
			crane.location = location
			craneListing[crane.id] = crane
		}
	} else {
		craneListing = getRandomCraneData()
	}
	if (noCraneFlag){
		// Evaluating weather impact on cranes installed onsite
		windDP = getWeatherImpact()
		
		// Run algorithm
		allocateElementStocks()
		var headNode = jobList[firstJob]
		traverse(jobList, (node) => {distributeJob(node)})
		
		// Computing crane metrics
		let craneMetrics = computeCraneMetrics()
		let projectMetrics = computeProjectMetrics()
		
		// Update the scenario metrics
		scenarioMetrics["Site Coverage"] = Math.min(projectMetrics.coverage,100)
		scenarioMetrics["Crane Utilization"] = Math.min(craneMetrics.utilization,100)
		scenarioMetrics["Crane Activity"] = craneMetrics.activity
		scenarioMetrics["Number of days to first utilization"] = craneMetrics.numDayToFirstLift
		scenarioMetrics["Project Delay"] = projectMetrics.delay
		scenarioMetrics["Project Delay Breakdown"] = projectMetrics.delayBreakdown
		scenarioMetrics["Project Time"] = projectMetrics.time
		scenarioMetrics["Project Time Breakdown"] = projectMetrics.timeBreakdown
		scenarioMetrics["Project Cost"] = projectMetrics.cost
		scenarioMetrics["Project Cost Breakdown"] = projectMetrics.costBreakdown
		scenarioMetrics["Project Risk"] = projectMetrics.risk
		scenarioMetrics["Project Risk Breakdown"] = projectMetrics.riskBreakdown
		scenarioMetrics["KPI_Value"] = projectMetrics.KPI_cost
		scenarioMetrics["KPI_Speed"] = projectMetrics.KPI_time
		scenarioMetrics["KPI_Agility"] = projectMetrics.KPI_risk
		inactiveCranes = craneMetrics.inactive
	}


	console.log(jobList)
	// console.log(eltStocks)
	// console.log(windDP)
	// console.log(inactiveCranes)
	console.log(weeklyDP)
	console.log(scenarioMetrics)
	return {
		weeklyDP: weeklyDP,
		weatherDP: windDP,
		scenarioMetrics: scenarioMetrics,
		cranes: craneListing,
		inactive: inactiveCranes
	}
}

// Allocate each job's element to cranes
// Use variance of BFS tree traversing technique
// Always prioritize on the task with soonest start date
function traverse(jobList, callback){
	const head = jobList[firstJob]
	var queue = new PriorityJob()
	queue.push(head, head.start)
	var job
	while (queue.size() > 0){
		job = queue.pop()
		if (job.status != 2){
			callback(job)
			if(!job.successor){
				continue
			}
			for(var i=0; i<job.successor.length; i++){
				let succJob = jobList[job.successor[i]]
				queue.push(succJob, succJob.start)
			}
			jobList[job.id].status = 2
		}
	}
}

// Distribute element stocks to each jobs
function allocateElementStocks(){	
	let jobIDList = Object.keys(jobList)
	for (var i=0; i< jobIDList.length; i++){
		let job = jobList[jobIDList[i]]
		let jobElements = []
		let jobLevels = job.level
		for (var j=0; j<jobLevels.length; j++){
			if (eltStocks[job.zone]){
				let elementList = eltStocks[job.zone][jobLevels[j]]
				if(elementList){
					let elementIDs = Object.keys(elementList)
					for (var k in elementIDs){
						let id = elementIDs[k]
						if (elementList[id].family.category == job.type.element){
							jobElements.push(elementList[id])				
						}
					}
				}
			}
		}
		jobList[jobIDList[i]].elements = [].concat.apply([], jobElements)
	}
}

// Allocate each job's element to cranes
function distributeJob(job){
	let day = 60*1000*60*24
	let jobElts = job.elements
	let numElts = jobElts.length
	let numFlag = 0
	let totalLiftTime = 0
	let totalMinLiftTime = 0
	let uniqueCranes = []
	let craneTime = {}
	let expStart = null
	let expEnd = null

	// Loop over all the elements associated to this job
	if (numElts != 0){
		for (var i=0; i<numElts; i++){
			let lift = distributeElement(jobElts[i], job.id, job.start)
			if (lift.lift_flag){
				let liftStart = lift.start
				let liftEnd = lift.end
				if (expStart == null){
					expStart = liftStart
				} else if (expStart.getTime() > liftStart.getTime()){
					expStart = liftStart
				}
				if (expEnd == null){
					expEnd = liftEnd
				} else if (expEnd.getTime() < liftEnd.getTime()) {
					expEnd = liftEnd
				}
				totalLiftTime += lift.time
				if (!uniqueCranes.includes(lift.craneID)){
					uniqueCranes.push(lift.craneID)
					craneTime[lift.craneID] = 0
				}
				craneTime[lift.craneID] += lift.time
			} else {
				numFlag += 1
			}
			// Calculating job's min total lift time based max lift speed
			let eltTravel = jobElts[i].travelDist
			let minLiftTime = craneHookTime + eltTravel/maxLiftSpeed + craneunHookTime
			totalMinLiftTime += minLiftTime
		}
		// Update the job's attribute based on the calculated element distribution
		job.liftTime = totalLiftTime
		job.minLiftTime = totalMinLiftTime
		job.craneTime = craneTime
		// Compute general coverage & baseline values for the current job
		let jobCoverage = 100 - Math.round((numFlag/numElts)*1000)/10
		let baseline = getJobBaseline(job, uniqueCranes)
		job.baseline_time = baseline.time
		job.baseline_cost = baseline.cost
		job.baseline_risk = baseline.risk
		job.coverage = jobCoverage
		// Calculating job's attributes
		let expDelay = Math.round((expEnd - job.end)/day*100)/100
		let expTime = Math.round((expEnd - job.start)/day*100)/100
		if (expEnd != null && expStart != null){
			job.expStart = expStart
			job.expEnd = expEnd
			job.expDelay = expDelay
			job.expTime = expTime		
		}
		// Update project's expected end date with the new lift job
		if (projectDates.expectedEnd == null){
			projectDates.expectedEnd = expEnd
		} else if (expEnd != null && projectDates.expectedEnd.getTime() < expEnd.getTime()){
			projectDates.expectedEnd = expEnd
		}
		// If job is not entirely covered, throw a warning message to alert users
		if (numFlag != 0) {
			console.warn("Only " + jobCoverage + "% of job " + job.id +" can be performed with current crane configuration")
		}
	} else {
		job.flag = true
		console.log("No elements loaded for task " + job.id)
	}
}

// Assign lift job to cranes able to perform the job
function  distributeElement(element, jobID, jobStart){
	// Checks that some cranes are available to perform the lift first
	var soonestCranes = findCrane(element, jobStart)
	//console.log(soonestCranes)
	let day = 60*60*24*1000
	let weeks = Object.keys(weeklyDP)
	if (!soonestCranes.unfitFlag){
		// Computing total lift job time
		// -- Recover the travel distance either default or with newly assigned pickup location
		let travelDist = element.travelDist
		if (soonestCranes.newPickup.dist > 0){
			travelDist = soonestCranes.newPickup.dist
		}
		// -- Compute the travel time required based on the travel distance & crane operating speed
		let craneSpeed = craneListing[soonestCranes.fit].type.opSpeed
		let travelTime = travelDist / craneSpeed
		let totalLiftTime = 2*travelTime + craneHookTime + craneunHookTime		// Lift time given in minutes
		// Recover the crane's current availability
		let currAvailability = craneListing[soonestCranes.fit].availability
		
		// Updating the weekly data points
		let weekNum = getWeekNumber(currAvailability)
		let craneWorkersCost = hourlyRate*numOfCraneWorkers/60
		let totalLiftCost = totalLiftTime*craneWorkersCost
		if (!weeks.includes("Week " + weekNum)){
			let lastWeek = weeklyDP[weeks[weeks.length - 1]]
			let currNumWeeks = getWeekNumber(lastWeek.start)
			let currTime = lastWeek.start.getTime() + 7*day
			for(var i=currNumWeeks; i<weekNum; i++){
				let currDate = new Date(currTime)
				if (!weeks.includes("Week " + (i+1))){
					weeklyDP["Week "+(i+1)] = new Weekly('Week '+(i+1), currDate, new Date(currTime + 7*day))
				}
			}
		}
		jobList[jobID].expCost += totalLiftCost
		weeklyDP["Week " + weekNum].cost.opCost += totalLiftCost
		
		// Computing new crane availability based on construction site schedule
		if (jobStart.getTime() > currAvailability.getTime()){
			currAvailability = jobStart
		}
		if (craneListing[soonestCranes.fit].activeTime == 0){
			craneListing[soonestCranes.fit].expStart = currAvailability
		}
		let newAvailability = moment(currAvailability).add(totalLiftTime,'m')._d
		
		// If the crane as been onsite for more than 30 days, factor in serviceability & contingency before use
		let timeInterval = (newAvailability.getTime() - craneListing[soonestCranes.fit].availability.getTime())/day
		let timeToCheck = craneListing[soonestCranes.fit].timeToCheck - timeInterval
		let checkTime = craneListing[soonestCranes.fit].type.serviceability
				+ craneListing[soonestCranes.fit].type.contingency
		let workDay = projectSchedule.endTime - projectSchedule.startTime
		let maintenanceFlag = false
		if (timeToCheck <= 0){
			let temp = moment(currAvailability).add(checkTime*60,'m')._d
			let mechCheckCost = craneListing[soonestCranes.fit].type.opCost + checkTime*hourlyRate*6
			weeklyDP["Week " + weekNum].cost.opCost -= checkTime/workDay*craneListing[soonestCranes.fit].type.opCost
			weeklyDP["Week " + weekNum].cost.maintCost += mechCheckCost
			newAvailability = temp
			maintenanceFlag = true
			timeToCheck = 30
		}

		// Checking that crane schedule doesn't operate pass 5PM
		while (newAvailability.getHours() >= projectSchedule.endTime){
			let dayCount = currAvailability.getDate()
			// Checking that crane schedule doesn't operate pass Fridays
			if (newAvailability.getDay() >= projectSchedule.endDay){
				dayCount = currAvailability.getDate() + 2
			}
			let nextDay = new Date(currAvailability.getFullYear(), 
				currAvailability.getMonth(), dayCount + 1, 8, 0, 0)
			newAvailability = moment(nextDay).add(totalLiftTime,'m')._d
		}

		// Updating crane's attribute with the new lift job
		craneListing[soonestCranes.fit].schedule.push(element)
		craneListing[soonestCranes.fit].availability = newAvailability
		craneListing[soonestCranes.fit].activeTime += totalLiftTime
		craneListing[soonestCranes.fit].timeToCheck = timeToCheck
		if(maintenanceFlag){craneListing[soonestCranes.fit].lastMaintenance = newAvailability}

		return {
			lift_flag: true,
			start: currAvailability,
			end: newAvailability,
			time: totalLiftTime,
			craneID: soonestCranes.fit
		}
	// Return flag if no cranes are available to lift that particular element
	} else {
		return {
			lift_flag: false
		}
	}
}

// Find crane to allocate the element to
function findCrane(element, jobStart){
	// ID variables
	let closestCraneID = null
	let soonestCraneID = null
	// List variables
	let soonestCraneTime = 0
	let unfitFlag = true
	let newPickup = null
	for (var id in craneListing){
		let crane = craneListing[id]
		let check = checkCraneFit(element, crane, jobStart)
		// Check if crane can perform the lift
		if (check.flag && check.newPickup.flag){
			if (check.time < soonestCraneTime || soonestCraneTime == 0){
				soonestCraneID = id
				soonestCraneTime = check.time
				newPickup = check.newPickup
				unfitFlag = false
			}
		}
	}

	return {
		fit: soonestCraneID,
		newPickup: newPickup,
		unfitFlag: unfitFlag,
	}
}

// Checks whether or not the crane can perform the lift job
function checkCraneFit(element, crane, jobStart){
	let day = 60*60*24*1000
	let pickupDist = euclidianDist(element.pickupXYZ, crane.location)
	let dropoffDist = euclidianDist(element.dropoffXYZ, crane.location)
	let availability = crane.availability
	// Factor in the fact that the crane might require maintenance before use
	let time = availability
	let lastMaintenance = crane.lastMaintenance
	let maintenanceTime = crane.type.serviceability + crane.type.contingency
	let timeToCheck = Math.abs(lastMaintenance.getTime() - jobStart.getTime())/day
	if(timeToCheck > 30){
		// Add maintenance time to the crane's availability
		time = moment(availability).add(maintenanceTime*60,'m')._d
	}
	// If the crane doesn't cover planned pickup location, assign a new one
	let newPickup = {id:null, dist:-1, flag:true}
	if (pickupDist > crane.type.radiusMax){
		let cranePickups = getCranePickupLocations(crane)
		if (!cranePickups.length) {
			//console.warn("Cranes must at least cover one pickup location")
			newPickup.flag = false
		}
		for (var id in cranePickups){
			let newDist = euclidianDist(element.dropoffXYZ, pickupLocs[id])
			if (newPickup.dist < 0 || newDist < newPickup.dist){
				newPickup.dist = newDist
				newPickup.id = id
			}
		}
	}
	// Check if the crane can perform the lift location & time wise
	let loc_Flag = dropoffDist < crane.type.radiusMax
	let time_Flag = crane.end.getTime() > jobStart.getTime() 
			&& crane.start.getTime() < jobStart.getTime()
	return {
		flag: (loc_Flag && time_Flag),
		newPickup: newPickup,
		time: time
	}
}

// Get all pickup locations that the crane covers
function getCranePickupLocations(crane){
	let dist = 0
	let cranePickups = []
	for (var id in pickupLocs){
		dist = euclidianDist(pickupLocs[id].location,crane.location)
		if(dist < crane.type.radiusMax){
			cranePickups.push(id)
		}
	}
	return cranePickups
}

// Initialize data points storage for algorithm
function initializeDP(){
	let monthNum = 1
	let day = 60*60*24*1000
	let projectStart = projectDates.projectStart
	let projectEnd = projectDates.projectEnd
	let projectDuration = (projectEnd.getTime() - projectStart.getTime())
	let numWeeks = Math.floor(projectDuration/(day*7)) + 1
	let weeks = [...Array(numWeeks).keys()].map(function(e) {return 'Week ' + (e+1)})
	let monthNames = ["January", "February", "March", "April", "May", "June",
	  	"July", "August", "September", "October", "November", "December"];
	let currTime = projectStart.getTime()
	let weeklyDP = {}
	let monthlyDP = {}
	let firstMonth = (projectStart.getYear()+1900) + "-" + (projectStart.getMonth()+1) 
	monthlyDP[firstMonth] = new Monthly(monthNum, projectStart.getMonth())
	for(var i=0; i<weeks.length; i++){
		let currDate = new Date(currTime)
		let currMonth = currDate.getMonth()
		let currYear = currDate.getYear() + 1900
		weeklyDP[weeks[i]] = new Weekly(weeks[i], currDate, new Date(currTime + 7*day))
		currTime += 7*day
		if (!monthlyDP.currMonth){
			let name = currYear + "-" + (currMonth+1)
			monthlyDP[name] = new Monthly(monthNum, monthNames[currMonth], currYear)
			monthNum += 1
		}
	}
	return {
		weekly: weeklyDP,
		monthly: monthlyDP
	}
}

// Compute project metrics based on crane schedules
function computeCraneMetrics(){
	// Notes on Crane Metrics:
	// ** Activity = % of active time over total time onsite
	// ** Utilization = % of crane onsite over total project time
	// ** # Days to first Lift = Number of days before first utilization
	let day = 60*60*1000*24
	let min = 60*1000
	let cumCraneActivity = 0
	let cumCraneUtilization = 0
	let cumDayToFirstLift = 0
	let inactiveCranes = []
	let craneActiveNum = 0
	let projectWorkDay = Math.abs(projectSchedule.startDay - projectSchedule.endDay)
	let projectWorkHours = Math.abs(projectSchedule.endTime - projectSchedule.startTime)
	let projectDuration = (projectDates.projectEnd.getTime() - projectDates.projectStart.getTime()) / min

	for (var id in craneListing){
		let crane = craneListing[id]
		let craneOpCost = crane.type.opCost
		let craneActiveWeeks = getCraneActiveWeeks(crane)

		// Check when are cranes onsite and add the corresponding operating cost
		for (var i in craneActiveWeeks.weeks){
			let weeks = craneActiveWeeks.weeks[i]
			weeklyDP[weeks].cost.opCost += craneOpCost*craneActiveWeeks.dayCount[weeks]
		}

		// Export crane latest available time
		crane.expEnd = crane.availability

		// Compute total time onsite vs actual use time
		let onsiteTime = (crane.end.getTime() - crane.start.getTime()) / min
		let availableTime = onsiteTime*(projectWorkDay/7)*(projectWorkHours/24)
		let activeTime = crane.activeTime
		let dayToFirstLift = 0
		let usedTime = 0
		if (crane.expStart){
			// Only compute data for active cranes
			let usedSpan = (crane.expEnd.getTime() - crane.expStart.getTime()) / min
			usedTime = usedSpan*(projectWorkDay/7)*(projectWorkHours/24)
			dayToFirstLift = (crane.expStart.getTime() - crane.start.getTime()) / day
			craneActiveNum += 1
		} else {
			inactiveCranes.push(id)
			console.warn("Crane " + crane.id + " is not being used at all")
		}
		let activity = Math.round((activeTime / availableTime) * 1000)/10
		let utilization = Math.round((onsiteTime / projectDuration) * 1000)/10
		crane.activity = Math.min(activity,100)
		crane.utilization = Math.min(utilization,100)

		cumCraneActivity += activity
		cumCraneUtilization += utilization
		cumDayToFirstLift += dayToFirstLift

		// Add installation cost to the weekly data points
		let instCost = crane.type.instCost
		weeklyDP["Week " + getWeekNumber(crane.start)].cost.instCost += instCost
	}
	let craneActivity = 0
	let craneUtilization = 0
	let avgDayToFirstLift = 0
	if (craneActiveNum != 0){
		craneActivity = Math.round(cumCraneActivity*10 / craneActiveNum)/10
		craneUtilization = Math.round(cumCraneUtilization*10 / craneActiveNum)/10
		avgDayToFirstLift = Math.round(cumDayToFirstLift*10 / craneActiveNum)/10
	}
	
	return{
		activity: craneActivity,
		utilization: craneUtilization,
		numDayToFirstLift: avgDayToFirstLift,
		inactive: inactiveCranes
	}
}

// Extract project metrics based on outputs of the algorithm
function computeProjectMetrics(){
	let day = 60*60*1000*24
	let weeks = Object.keys(weeklyDP)
	let jobIDList = Object.keys(jobList)
	let planEnd = projectDates.projectEnd
	let expEnd = projectDates.expectedEnd
	let costBreakdown = new costDP(0,0,0,0,0)
	let timeBreakdown = {}
	let delayBreakdown = {}
	let riskBreakdown = {}

	// Check if the algorithm was actually run
	if (expEnd == null){
		console.warn("Current crane configuration does not allow for the jobs to be performed")
	}

	// Loop over list of jobs
	let cumSiteCoverage = 0
	let numElts = 0
	for (var j=0; j<jobIDList.length; j++){
		// Compute the total site coverage & num of list performed
		let job = jobList[jobIDList[j]]
		let zone = job.zone
		let coverage = job.coverage
		let jobNumElts = job.elements.length
		cumSiteCoverage += coverage * jobNumElts
		numElts += jobNumElts

		// Updating the data points for the graphs
		if (numElts != 0 && !job.flag){
			updateDataPoints(job)			
			// Breakdown the time per zones
			if (zone != "" && !Object.keys(timeBreakdown).includes(zone)){
				timeBreakdown[zone] = job.expTime
				delayBreakdown[zone] = job.expDelay
				riskBreakdown[zone] = (1 - job.expRisk)
			} else if (zone != "") {
				timeBreakdown[zone] += job.expTime
				delayBreakdown[zone] += job.expDelay
				riskBreakdown[zone] *= (1 - job.expRisk)
			}
		}
	}
	console.log(riskBreakdown)
	for (var z in Object.keys(riskBreakdown)){
		let zones = Object.keys(riskBreakdown)
		console.log(zones[z])
		riskBreakdown[zones[z]] = Math.round((1 - riskBreakdown[zones[z]])*1000)/1000
	}

	// Loop over list of weekly data points
	let cumCost = 0
	let cumDelay = 0
	let cumTime = 0
	let cumRisk = 1
	let eltCount = 0
	let cumWeeklyKPI_time = 0
	let cumWeeklyKPI_cost = 0
	let cumWeeklyKPI_risk = 0
	let cumWeeklySqKPI_time = 0
	let cumWeeklySqKPI_cost = 0
	let cumWeeklySqKPI_risk = 0
	for (var w=0; w<weeks.length; w++){
		let week = weeklyDP[weeks[w]]
		let totCost = 0
		
		// Updating the cumulative weekly metrics
		for (var type in costBreakdown){
			costBreakdown[type] += Math.round(week.cost[type])
			totCost += week.cost[type]
		}
		cumDelay += week.totDelay
		cumTime += week.totTime
		cumRisk *= week.totRisk
		cumCost += totCost
		eltCount += week.totalEltPlanned
		let weeklyKPI_time = 0
		let weeklyKPI_cost = 0
		let weeklyKPI_risk = 0
		
		// Updating average weekly metrics
		if (week.jobsDoneCount != 0){
			if (week.totalEltLifted != 0){
				weeklyKPI_time = week.KPI_time / week.totalEltLifted
				weeklyKPI_cost = week.KPI_cost / week.totalEltLifted
				weeklyKPI_risk = week.KPI_risk / week.totalEltLifted
			}
			weeklyDP[weeks[w]].avgDelay = week.totDelay / week.jobsDoneCount
			weeklyDP[weeks[w]].avgTime = week.totTime / week.jobsDoneCount
			weeklyDP[weeks[w]].avgCoverage = week.totalEltLifted / week.totalEltPlanned * 100
			weeklyDP[weeks[w]].KPI_time = weeklyKPI_time
			weeklyDP[weeks[w]].KPI_cost = weeklyKPI_cost
			weeklyDP[weeks[w]].KPI_risk = weeklyKPI_risk
		}

		cumWeeklyKPI_time += weeklyKPI_time * week.totalEltLifted
		cumWeeklyKPI_cost += weeklyKPI_cost * week.totalEltLifted
		cumWeeklyKPI_risk += weeklyKPI_risk * week.totalEltLifted
		cumWeeklySqKPI_time += weeklyKPI_time * weeklyKPI_time * week.totalEltLifted
		cumWeeklySqKPI_cost += weeklyKPI_cost * weeklyKPI_cost * week.totalEltLifted
		cumWeeklySqKPI_risk += weeklyKPI_risk * weeklyKPI_risk * week.totalEltLifted

		// Update the weeklyDP values
		weeklyDP[weeks[w]].costBreakdown = new costDP(costBreakdown.instCost, 
			costBreakdown.opCost, costBreakdown.delayCost, 
			costBreakdown.addCost, costBreakdown.maintCost)
		weeklyDP[weeks[w]].totCost = Math.round(totCost)
		weeklyDP[weeks[w]].cumCost = Math.round(cumCost)
		weeklyDP[weeks[w]].cumDelay = Math.round(cumDelay*10)/10
		weeklyDP[weeks[w]].cumTime = Math.round(cumTime*10)/10
		if (cumRisk < 0.01){
			weeklyDP[weeks[w]].cumRisk = Math.round((1 - cumRisk)*100000)/100000
			weeklyDP[weeks[w]].totRisk = Math.round((1 - week.totRisk)*100000)/100000	
		} else {
			weeklyDP[weeks[w]].cumRisk = Math.round((1 - cumRisk)*1000)/1000
			weeklyDP[weeks[w]].totRisk = Math.round((1 - week.totRisk)*1000)/1000
		}
		if (eltCount > 0){
			weeklyDP[weeks[w]].cumKPI_time = cumWeeklyKPI_time / eltCount
			weeklyDP[weeks[w]].cumKPI_cost = cumWeeklyKPI_cost / eltCount
			weeklyDP[weeks[w]].cumKPI_risk = cumWeeklyKPI_risk / eltCount
		}
	}
	console.log(eltCount)
	// Updating project total cost and KPIs
	let avgKPI_time = cumWeeklyKPI_time/eltCount
	let ProjectKPI_time = {avg: Math.round(avgKPI_time), std: 0.5*Math.round(Math.sqrt(cumWeeklySqKPI_time/eltCount - avgKPI_time*avgKPI_time))}
	let avgKPI_cost = cumWeeklyKPI_cost/eltCount
	let ProjectKPI_cost = {avg: Math.round(avgKPI_cost), std: 0.5*Math.round(Math.sqrt(cumWeeklySqKPI_cost/eltCount - avgKPI_cost*avgKPI_cost))}
	let avgKPI_risk = cumWeeklyKPI_risk/eltCount
	let ProjectKPI_risk = {avg: Math.round(avgKPI_risk), std: 0.5*Math.round(Math.sqrt(cumWeeklySqKPI_risk/eltCount - avgKPI_risk*avgKPI_risk))}

	// Round outputs
	Object.keys(costBreakdown).forEach(key => {costBreakdown[key] = Math.round(costBreakdown[key]/100)*100;})
	Object.keys(timeBreakdown).forEach(key => {timeBreakdown[key] = Math.round(timeBreakdown[key]*100)/100;})
	Object.keys(delayBreakdown).forEach(key => {delayBreakdown[key] = Math.round(delayBreakdown[key]*100)/100;})

	let projectCoverage = Math.round(cumSiteCoverage/numElts*10)/10
	// if (projectCoverage > 99.5){
	// 	projectCoverage = 100
	// }

	return{
		coverage: projectCoverage,
		delay: Math.round(cumDelay*10)/10,
		time: Math.round(cumTime*10)/10,
		cost: Math.round(cumCost),
		risk: Math.round((1 - cumRisk)*1000)/1000,
		costBreakdown: costBreakdown,
		timeBreakdown: timeBreakdown,
		delayBreakdown: delayBreakdown,
		riskBreakdown: riskBreakdown,
		KPI_time: ProjectKPI_time,
		KPI_cost: ProjectKPI_cost,
		KPI_risk: ProjectKPI_risk
	}
}

// Update the data points for the graph as the algorithm runs
function updateDataPoints(job){
	let day = 60*60*1000*24
	// Get job metrics
	let expStart = job.expStart
	let expEnd = job.expEnd
	let expDelay = job.expDelay
	let expTime = job.expTime
	let craneTime = job.craneTime
	let numElts = job.elements.length
	let coverage = job.coverage
	let weekNumStart = getWeekNumber(expStart)
	let weekNumEnd = getWeekNumber(expEnd)
	let weeks = Object.keys(weeklyDP)
	let jobISO_start = expStart.getWeekISONumber()
	let jobISO_end = expEnd.getWeekISONumber()
	let cumJobProbImpact = 0
	let weatherInit = require("./data/_parsedWeatherData.json")

	// If the project is overdelayed, we need to add extra data points
	// Section is deprecated and added in the 'distributeElement' function
	/*
	if(weeks[weeks.length - 1].end < expEnd){
		let lastWeek = weeks[weeks.length - 1]
		let currNumWeeks = getWeekNumber(lastWeek.start)
		let currTime = lastWeek.start.getTime() + 7*day
		for(var i=currNumWeeks; i<weeksNumEnd; i++){
			let currDate = new Date(currTime)
			weeklyDP["Week "+(i+1)] = new Weekly('Week '+(i+1), currDate, new Date(currTime + 7*day))
		}
	}
	*/

	// Update the expected cost & risk of a job by adding the distributed crane costs
	if (expDelay > 0){
		job.expCost += expDelay*jobDelayPosCost*1000
	} else {
		job.expCost += expDelay*jobDelayNegCost*1000
	}
	let avgUtilization = 0
	let cumUtilization = 0
	let avgActivity = 0
	let cumActivity = 0
	let expProbImpact = 1
	let numCrane = 0
	for (var id in craneTime){
		let cumCraneProb = 1
		let crane = craneListing[id]
		let activeTime = crane.activeTime
		let utilization = crane.utilization
		let activity = crane.activity
		let craneISO_start = crane.start.getWeekISONumber()
		let onsiteTime = (crane.end.getTime() - crane.start.getTime()) / day
		let factor = craneTime[id]/activeTime
		cumUtilization += utilization
		cumActivity += activity
		numCrane += 1
		// Retrieving crane's cost values
		let instCost = crane.type.instCost
		let opCost = crane.type.opCost
		job.expCost += factor*(onsiteTime*opCost + instCost)
		// Retrieving crane's risk prob
		let maxWindSpeed = crane.type.maxWindSpeed
		if (maxWindSpeed > 10) {
			maxWindSpeed = 10 + (maxWindSpeed - 10)/3
		}
		let ISO_start = Math.min(jobISO_start, craneISO_start)
		for (var i = ISO_start; i<=jobISO_end; i++){
			let sigma = weatherInit[i].rayleighSigma
			cumCraneProb *= (1 - rayleighPDF(maxWindSpeed, sigma*1.2))
		}
		cumJobProbImpact += 1 - cumCraneProb
	}
	if (numCrane != 0){
		avgUtilization = cumUtilization/numCrane
		avgActivity = cumActivity/numCrane		
		expProbImpact = cumJobProbImpact/numCrane
	}
	let expCost = job.expCost
	job.expRisk = expProbImpact

	// Compute the KPIs for this job factoring current coverage
	let duration = job.duration
	let baseline_time = job.baseline_time
	let baseline_cost = job.baseline_cost
	
	// ** Time KPI
	let KPI_time = Math.floor(Math.random()*100)
	if (expTime < baseline_time) {
		let val = expTime/baseline_time
		KPI_time = 100 - 30*val*val
	} else if (expTime < duration) {
		let val = (duration - expTime)/(duration - baseline_time)
		KPI_time = 70 - 50*val*val*val*val
	} else {
		KPI_time = 20*duration/expTime
	}
	job.KPI_time = KPI_time
	
	// ** Cost KPI
	let KPI_cost = Math.floor(Math.random()*100)
	let cap_cost = baseline_cost
	if (expCost < cap_cost) {
		KPI_cost = 100
	} else if (expCost < 2*cap_cost) {
		let val = (2*cap_cost - expCost)/cap_cost
		KPI_cost = 100 - 10*val*val
	} else if (expCost < 3*cap_cost) {
		let val = (3*cap_cost - expCost)/cap_cost
		KPI_cost = 90 - 50*val*val
	} else {
		let val = 3*cap_cost/expCost
		KPI_cost = 40*val*val
	}
	job.KPI_cost = KPI_cost
	console.log(Math.round(expCost), Math.round(cap_cost), Math.round(KPI_cost))

	// ** Risk KPI
	// KPI is currently computed using the prob & activity of the chosen crane mix
	let totalNumCrane = Object.keys(craneListing).length
	let fact_spareAct = Math.max(1 - avgActivity/90*avgActivity/90,0)
	let fact_spareCap = avgUtilization/100*avgUtilization/100
	let fact_probImp = Math.pow((1 - Math.min(expProbImpact*6,1)),2)
	let KPI_risk = 100*fact_spareCap*fact_probImp*fact_spareAct
	// console.log("probability_impact: " + expProbImpact)
	// console.log("cap:"+Math.round(fact_spareCap*1000)/1000 +"- prob:"+Math.round(fact_probImp*1000)/1000+"- act:"+Math.round(fact_spareAct*1000)/1000)
	// console.log("KPI:"+Math.round(KPI_risk*1000)/1000)
	job.KPI_risk = KPI_risk
	
	// Fill-in weekly data points
	let startWeek = "Week " + weekNumStart
	let endWeek = "Week " + weekNumEnd
	// Compute probability that site is impacted for this week assuming jobs are IID
	// -- Case 1: Job takes 1 week
	if (weekNumStart == weekNumEnd) {
		weeklyDP[startWeek].jobsDoneCount += 1
		weeklyDP[startWeek].jobsDone.push(job.id)
		weeklyDP[startWeek].totalEltLifted += numElts * coverage / 100
    	weeklyDP[startWeek].totalEltPlanned += numElts
		weeklyDP[startWeek].totDelay += expDelay
		weeklyDP[startWeek].totTime += expTime
		weeklyDP[startWeek].totRisk *= (1 - expProbImpact)
		weeklyDP[startWeek].KPI_time += KPI_time * numElts * coverage / 100
		weeklyDP[startWeek].KPI_cost += KPI_cost * numElts * coverage / 100
		weeklyDP[startWeek].KPI_risk += KPI_risk * numElts * coverage / 100
		// If delay > 0 => cost, if delay < 0 => savings
		if (expDelay > 0){
			weeklyDP[startWeek].cost.delayCost += expDelay*jobDelayPosCost*1000
		} else {
			weeklyDP[startWeek].cost.delayCost += expDelay*jobDelayNegCost*1000
		}
	// -- Case 2: Job takes more than 1 week
	} else {
		// Week counts are project weeks (not ISO weeks) so they are cummulative
		for(var i=weekNumStart; i< weekNumEnd; i++){
			weeklyDP["Week " + i].jobsInProgressCount += 1
			weeklyDP["Week " + i].jobsInProgress.push(job.id)
		}
		weeklyDP[endWeek].jobsDoneCount += 1
		weeklyDP[endWeek].jobsDone.push(job.id)
		weeklyDP[endWeek].totalEltLifted += numElts * coverage / 100
    	weeklyDP[endWeek].totalEltPlanned += numElts
		weeklyDP[endWeek].totDelay += expDelay
		weeklyDP[endWeek].totTime += expTime
		weeklyDP[endWeek].totRisk *= (1 - expProbImpact)
		weeklyDP[endWeek].KPI_time += KPI_time * numElts * coverage / 100
		weeklyDP[endWeek].KPI_cost += KPI_cost * numElts * coverage / 100
		weeklyDP[endWeek].KPI_risk += KPI_risk * numElts * coverage / 100
		// If delay > 0 => cost, if delay < 0 => savings
		if (expDelay > 0){
			weeklyDP[endWeek].cost.delayCost += expDelay*jobDelayPosCost*1000
		} else {
			weeklyDP[endWeek].cost.delayCost += expDelay*jobDelayNegCost*1000
		}
	}
}

// Get crane active week list and day count per week
function getCraneActiveWeeks(crane){
	let day = 60*60*1000*24
	let startDate = crane.start
	let startWeek = getWeekNumber(startDate)
	let endDate = crane.end
	let endWeek = getWeekNumber(endDate)
	let startWeekDayCount = 7 - Math.abs(Math.floor((weeklyDP["Week " + startWeek].start.getTime() - startDate.getTime())/day))
	let endWeekDayCount = 7 - Math.abs(Math.floor((weeklyDP["Week " + endWeek].end.getTime() - endDate.getTime())/day))
	let activeWeeks = {}
	for (var i=startWeek; i<=endWeek; i++){
		activeWeeks["Week " + i] = 7
	}
	activeWeeks["Week " + startWeek] = startWeekDayCount
	activeWeeks["Week " + endWeek] = endWeekDayCount
	return{
		weeks: Object.keys(activeWeeks),
		dayCount: activeWeeks
	}
}

// Compute the weather impact on the crane's activity
function getWeatherImpact(){
	let weatherData = {}
	let weatherInit = require("./data/_parsedWeatherData.json")
	for (var w in weatherInit){
		let wind = weatherInit[w]
		let avgWindSp = wind.avgWindSp
		let bandwidth = [wind.bandwidth[0],wind.bandwidth[1]]
		let weekWindDP = new weatherDP(avgWindSp, bandwidth)
		weatherData[w] = weekWindDP
	}
	let craneIDs = Object.keys(craneListing)
	let craneNum = craneIDs.length
	let pStart = projectDates.projectStart
	let pEnd = projectDates.projectEnd
	for (var i=0; i<craneNum; i++){
		let crane = craneListing[craneIDs[i]]
		let cStart = crane.start
		let cEnd = crane.end
		let maxWindSpeed = crane.type.maxWindSpeed
		// Tweaking wind resistance for greater visual impact
		if (maxWindSpeed > 10) {
			maxWindSpeed = 10 + (maxWindSpeed - 10)/3
		}
		for (var w in weatherInit){
			let wind = weatherInit[w]
			for (var y = pStart.getYear(); y<=pEnd.getYear(); y++){
				let d = getDateOfISOWeek(w, 1900 + y).getTime()
				if (d > pStart.getTime() && d < pEnd.getTime()){
					if (d > cStart.getTime() && d < cEnd.getTime()){
						let probImpact = rayleighPDF(maxWindSpeed, wind.rayleighSigma*1.2)
						weatherData[w].probNoCranes *= (1 - probImpact)
						weatherData[w].numOfCrane += 1
					}
				}
			}
		}
	}
	for (var w in weatherData){
		let p = weatherData[w].probNoCranes
		let numOfCranes = weatherData[w].numOfCrane * (1-p)
		weatherData[w].numOfCrane = Math.round(numOfCranes)
		weatherData[w].probOfImpact = Math.round((1 - p)*10000)/100
		weatherData[w].probNoCranes = Math.round(p*10000)/100
	}
	return weatherData
}

// Compute the minimum operating & installation cost out of all crane types
function getMinCraneCost(craneTypes){
	let minOpCost = Infinity
	let minInstCost = Infinity
	for(var id in craneTypes){
		if(craneTypes[id].opCost < minOpCost){
			minOpCost = craneTypes[id].opCost
		}
		if(craneTypes[id].instCost < minInstCost){
			minInstCost = craneTypes[id].opCost
		}
	}
	return {
		op: minOpCost,
		inst: minInstCost
	}
}

// Compute the maximum lift speed out of all crane types
function getMaxLiftSpeed(craneTypes){
	let maxLiftSpeed = 0
	for(var id in craneTypes){
		if(craneTypes[id].opSpeed > maxLiftSpeed){
			maxLiftSpeed = craneTypes[id].opSpeed
		}
	}
	return maxLiftSpeed
}

// Compute expected job lift baseline in terms of time, cost and risk
function getJobBaseline(job, uniqueCranes){
	let day = 60*60*1000*24
	let baseline_date = 0
	let baseline_time = 0
	let baseline_cost = 0
	let baseline_risk = 0
	let minLiftTime = job.minLiftTime
	let workDay = (projectSchedule.endTime - projectSchedule.startTime)*60

	// Computing the time baseline as 1 crane with maximum operating speed
	let numOfDays = Math.floor(minLiftTime/workDay)
	let numOfWeeks = Math.floor(numOfDays / 5)
	let daysLastWeek = numOfDays - numOfWeeks*5
	let minLastDay = minLiftTime - (numOfWeeks*5 + numOfDays)*60*24
	baseline_date = moment(job.start.getTime()).add(numOfWeeks,'weeks').add(numOfDays,'d').add(minLastDay,'minutes')._d
	baseline_time = Math.round((baseline_date - job.start)/day*100)/100

	// Computing the cost baseline as 1 crane with minimum costs
	let workersCost = minLiftTime*hourlyRate*numOfCraneWorkers/60
	baseline_cost = job.duration*minCraneCost.op + workersCost + minCraneCost.inst
	let delay = baseline_time - job.duration
	if (delay > 0){
		baseline_cost += delay*jobDelayPosCost*1000
	} else {
		baseline_cost += delay*jobDelayNegCost*1000
	}

	return {
		time: baseline_time,
		cost: baseline_cost,
		risk: baseline_risk
	}
}

// Compute the number of the week in the project
function getWeekNumber(date){
	let day = 60*60*1000*24
	return Math.floor((date.getTime() - projectDates.projectStart.getTime())/(7*day)) + 1
}

// Compute the Euclidian Distance between 2 arrays of the same size
function euclidianDist(A,B){
	if (A.length != B.length){
		return undefined
	}
	// Hardcoding X-Y distance only
	let dist = (A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1])
	/*
	for (var i=0; i<A.length; i++){
		dist += (A[i]-B[i])*(A[i]-B[i])
	}
	*/
	return Math.sqrt(dist)
}

// Compute the start date of an ISO week for a given year
function getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
}

// Reset the algorithm's variables 
function resetInstanceVariables(){
	dataPoints = null
	weeklyDP = null
	monthlyDP = null
	windDP = null
	scenarioMetrics = {}
	craneListing = 	{}
	projectDates.expectedEnd = null

	// Reseting Jobs
	for (var id in jobList){
		jobList[id].status = 0 // -1: delayed, 0: not done, 1: processing, 2: done
		jobList[id].flag = false
    	jobList[id].elements = []
    	jobList[id].expStart = new Date()
    	jobList[id].expEnd = new Date()
    	jobList[id].liftTime = 0
    	jobList[id].minLiftTime = 0
	    jobList[id].baseline_time = 0
	    jobList[id].baseline_cost = 0
	    jobList[id].baseline_risk = 0
	    jobList[id].KPI_time = -1
	    jobList[id].KPI_cost = -1
	    jobList[id].KPI_risk = -1
	    jobList[id].coverage = 0
	    jobList[id].expDelay = 0
	    jobList[id].expTime = 0
	    jobList[id].expCost = 0
	    jobList[id].expRisk = 0
	}
}

//*** GENERATING DUMMY CRANE PLACEMENT DATA ***//

// Get crane data to run algorithm
function getRandomCraneData(){
	var numOfCranes = 40
	var craneListing = generateCraneData(numOfCranes)
	return craneListing
}

function getMinMaxXYZ(){
	let minX = pickupLocs[0].location[0]
	let minY = pickupLocs[0].location[1]
	let maxX = pickupLocs[1].location[0]
	let maxY = pickupLocs[1].location[1]
	for (var i=2; i<pickupLocs.length; i++){
		if(pickupLocs[i].location[0] < minX){
			minX = pickupLocs[i].location[0]
		}
		if(pickupLocs[i].location[1] < minY){
			minY = pickupLocs[i].location[1]
		}
		if(pickupLocs[i].location[0] > maxX){
			maxX = pickupLocs[i].location[0]
		}
		if(pickupLocs[i].location[1] > maxY){
			maxY = pickupLocs[i].location[1]
		}
	}
	return{
		min: [minX, minY, 0],
		max: [maxX, maxY, 0]
	}
}

function generateCraneData(numOfCranes){
	// Inputs
	var bound = getMinMaxXYZ()
	// Initialize
	var listX = []
	var listY = []
	var incX = (bound.max[0] - bound.min[0])/numOfCranes
	var incY = (bound.max[1] - bound.min[1])/numOfCranes
	for (var i = 0; i < numOfCranes; i++) {
	    listX.push(bound.min[0] + i*incX)
	    listY.push(bound.min[1] + i*incY)
	}
	// Shuffle Y positions
	var listYRand = shuffle(listY)
	// Generate new crane objects
	var craneListing = {}
	var types = Object.keys(craneTypes)
	for (var i=0; i<numOfCranes; i++){
		let id = i
		let typeNum = Math.floor((Math.random()*types.length) + 1) - 1
		let location = [listX[i], listYRand[i], 0]
		let firstAvailability = new Date(projectDates.steelStart.getFullYear(), 
				projectDates.steelStart.getMonth(), projectDates.steelStart.getDate()+3, 8, 0, 0)
		craneListing[id] = new Crane(id, craneTypes[types[typeNum]], firstAvailability, projectDates.projectEnd, location)
	}
	return craneListing
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

//*** END OF SAMPLE DATA GENERATOR ***//

export { getModelInputs, runAlgorithm }