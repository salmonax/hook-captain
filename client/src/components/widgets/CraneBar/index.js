import React, { Component } from 'react';
import { Icon } from 'flux-ui';
import './index.scss';
import { inject, observer } from 'mobx-react';
import d3 from 'd3'; // for darker colors only

import { renewCranes } from 'viewports/cranes/lib';

const gbDate = (dateString) => {
  const [ mm, dd, yy ] = new Date(dateString).toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: '2-digit'}).split('/');
  return [ dd, mm, yy ].join('/');
};



// Note: should really extract the base click/drag events
// Could be: 1) the basic drag component 2) a domain wrapper 3) the actual component
// This was originally intended to be usable in the cranes GANTT component as well
@inject('common') @observer
export default class TimeBar extends Component {
  left = '0%'
  right = '0%'

  componentDidMount() {
    // Prevent sticky mouse on window leave
    window.addEventListener('mouseup', this.onMouseUp, true);
    // Allow mouse movement to persist when outside of element
    window.addEventListener('mousemove', this.onMouseMove, true);
    // Sorry, React
  }

  componentWillUnmount() {
    window.removeEventListener('mouseup', this.onMouseUp, true);
    window.addEventListener('mousemove', this.onMouseMove, true);
  }

  componentWillMount() {
    const { crane } = this.props;
    this.left = this.stringToPercents(crane.start).left;
    this.right = this.stringToPercents(crane.end).right;
  }

  moveBar = (e) => {
    // if (!e.clientX) return;
    const offset = e.clientX - this._startX;
    const { left, right } = this._startRects;
    const rightMost = right;
    const leftMost = left;
    let newLeft = this._startLeft + offset;
    let newRight = this._startRight - offset;
    if (newRight < 0) {
      newLeft = newRight + newLeft;
      newRight = 0;
    }
    if (newLeft < 0) {
      newRight = newRight + newLeft;
      newLeft = 0;
    }
    this.left = this.toPercent(newLeft);
    this.right = this.toPercent(newRight);
    // console.log(this.leftDate, this.rightDate);

  }

  resizeLeft = (e) => {
    const { min, max } = Math;
    const { toPercent, toPixels } = this;
    const offset = e.clientX - this._startX;
    const newPosition = this._startLeft + offset;
    const rightClamp = this._startRects.width-toPixels(this.right)-5;
    this.left = toPercent(min(max(newPosition, 0), rightClamp));
    // const leftPercent = toPixels(this.left)/this._startRects.width;
    // const leftDomain = Math.round(this.domainStart + leftPercent*this.domainWidth);

    // console.log('??', this.leftDate);
  }

  get leftDate() {
    const leftPercent = this.toPixels(this.left)/this._startRects.width;
    const leftDomain = Math.round(this.domainStart + leftPercent*this.domainWidth);
    return new Date(leftDomain);
  }
  get rightDate() {
    const rightPercent = (this._startRects.width - this.toPixels(this.right))/this._startRects.width;
    const rightDomain = Math.round(this.domainStart + rightPercent*this.domainWidth);
    return new Date(rightDomain);
  }

  resizeRight = (e) => {
    const { min, max } = Math;
    const { toPercent, toPixels } = this;
    const offset = e.clientX - this._startX;
    const newPosition = this._startRight - offset;
    const leftClamp = this._startRects.width-toPixels(this.left)-5;
    this.right = toPercent(min(max(newPosition, 0), leftClamp));
    // console.log('!!', this.rightDate);
  }

  stringToPercents(dateString) {
    const dateNum = new Date(dateString).getTime();
    const left = (Math.min(Math.max(0,(dateNum-this.domainStart)/this.domainWidth * 100),100)).toFixed(2)  + '%';
    const right = (Math.min(Math.max(0, (this.domainEnd - dateNum)/this.domainWidth * 100),100)).toFixed(2) + '%';
    return { left, right };
  }

  toPercent = (pixels) => pixels/this._startRects.width * 100 + '%'
  toPixels = (percent) => parseFloat(percent)/100 * this._startRects.width
  // Will make these @computed later. Lazy now.
  get domainStart() {
    const { start } = this.props.common.projectDates;
    return start ? start.getTime() : 0;
  }

  get domainEnd() {
    const { end } = this.props.common.projectDates;
    return end ? end.getTime() : 100;
  }

  get domainWidth() {
    return this.domainEnd - this.domainStart;
  }

  _initClick = (e) => {
    this._startRects = this.container.getClientRects()[0];
    const { left, width } = this._startRects;
    this._startOffsetLeft = e.clientX - left;
    this._startOffsetRight = width - this._startOffsetLeft;
    this._startX = e.clientX;
    // this.left = this.stringToPercents(this.props.crane.start).left;
    // this.right = this.stringToPercents(this.props.crane.end).right;
    this._startLeft = this.toPixels(this.left);
    this._startRight = this.toPixels(this.right);
  }

  outerResize = (e) => {
    if (this._startOffsetLeft < this._startLeft) {
      this.resizeLeft(e);
    } else if (this._startOffsetRight < this._startRight) {
      this.resizeRight(e);
    }
  }

  onMouseDown = (e) => {
    const className = e.target.className.split(' ')[0];
    this._action = {
      'inner-bar-left-handle': 'resizeLeft',
      'inner-bar-right-handle': 'resizeRight',
      'inner-bar': 'moveBar',
      'outer-bar-container': 'outerResize',
    }[className];
    if (!this._action) return;
    this.props.common.animateTimeBar = false;
    e.preventDefault();
    this._initClick(e);
  }

  onMouseUp = (e) => {
    if (!this._action) return;
    this.props.common.animateTimeBar = true;
    e.preventDefault();
    // Maybe this should be passed in as an 'onEnd' prop
    this._updateCranes();
    this._action = null
  }

  _updateCranes = () => {
    const { common, crane, scenario } = this.props;
    common.updateCraneWindow(crane, scenario, this.leftDate, this.rightDate);
    if (common.focusedCrane && common.focusedCrane.data.uuid === crane.uuid) {
      // console.log('I need business to happen here, as the focused crane!!!');
      common.movingFocusedCrane = Date.now();
    }
  }

  onMouseMove = (e) => {
    // WARNING: this is getting attached to the window and 
    // running *constantly* when the TimeBar component is loaded
    if (!this._action) return;
    const action = this[this._action];
    action(e);
    this._updateCranes();
    e.preventDefault();
  }

  render() {  
    const { crane, common } = this.props;
    const left = this.left = this.stringToPercents(crane.start).left;
    const right = this.right = this.stringToPercents(crane.end).right;
    const { color } = crane;
    const darker = d3.hcl(color).darker(0.70).toString();
    let handleColor = { backgroundColor: d3.hcl(color).brighter(0.35).toString() };
    let innerColor = { backgroundColor: d3.hcl(color).brighter(0.35).toString(), border: `solid 1px ${darker}` };
    let gripColor = { borderLeft: `1px solid ${darker}`, borderRight: `1px solid ${darker}` };

    let expStartPercent, expEndPercent, candidateStartPercent;
    if (common.algoMap.size) {
      const algoCrane = common.algoMap.get('cranes')[crane.uuid];
      if (algoCrane) {
        let { expStart, expEnd } = algoCrane;
        // For visualization purposes, if gap is < 72 hours, don't set expStartPercent
        // This will effectively make the crane inactive
        // This might make it tough to stay consistent with other features... But we'll see
        if (expEnd) expEndPercent = this.stringToPercents(expEnd).left;
        if (expStart) candidateStartPercent = this.stringToPercents(expStart).right;
        if (expStart && expEnd-expStart >= 2.592e8 && 
              parseFloat(expEndPercent) > parseFloat(this.left) &&
              parseFloat(candidateStartPercent) > parseFloat(this.right)) {
          expStartPercent = this.stringToPercents(expStart).right;
        } else {
          handleColor = { backgroundColor: '#bbb' };
          innerColor = { backgroundColor: '#bbb', border: `solid 1px #555` };
          gripColor = { borderLeft: `1px solid #555`, borderRight: `1px solid #555` };
          // update the crane in question here
        }

        // console.warn("CRANEBAR DATE", expStart, expEnd);
        // console.warn('CRANEBAR PERCENT', expStartPercent, expEndPercent);
      } 
    }
    



    // const handleColor = d3.hcl(crane.color).darker(0.35).toString();
    // const craneColor = d3.hcl(crane.color).brighter(0.35).toString();
    // const handleColor = crane.color;

    return (
      <div style={{display: 'flex', flexDirection: 'row'}}> 
        <div style={{textAlign: 'right', minWidth: '21px', margin: '3px 7px 2px 12px'}}>#{crane.count}</div>
        <div className="outer-bar-container crane-bar" 
          onMouseDown={this.onMouseDown}
          onMouseMove={this.onMouseMove}
          ref={ref => this.container = ref}
        >
          <div className="outer-bar" style={{border: 'none'}}/>
          <div className="inner-bar" style={Object.assign({ left, right}, innerColor)}>
            <div className="inner-bar-left-handle" style={handleColor}>
              <div className="inner-bar-grip" style={gripColor}/>
            </div>
            <div className="inner-bar-right-handle" style={handleColor}>
              <div className="inner-bar-grip" style={gripColor}/>
            </div>
          </div>
          <div key={`overlay-left-${crane.uuid}`} className="inner-bar-inactivity-overlay left" style={{left, right: expStartPercent || '0%', mixBlendMode: 'hard-light'}}/>
          <div key={`overlay--right-${crane.uuid}`} className="inner-bar-inactivity-overlay right" style={{left: expStartPercent ? expEndPercent : '0%', right, mixBlendMode: 'hard-light'}}/>
        </div>
        <div style={{display: 'flex', flexWrap: 'nowrap', margin: '3px 6px 2px 6px'}}>{`${gbDate(crane.start)} - ${gbDate(crane.end)}`}</div>
      </div>
    );
  }

}
