import React, { Component } from 'react';
import { Icon } from 'flux-ui';
import './index.scss';
import { inject, observer } from 'mobx-react';
import d3 from 'd3'; // for darker color in edit mode
import { reaction } from 'mobx';

// Note: should really extract the base click/drag events
// Could be: 1) the basic drag component 2) a domain wrapper 3) the actual component
// This was originally intended to be usable in the cranes GANTT component as well
@inject('common') @observer
export default class TimeBar extends Component {
  _animate = true
  left = '0%'
  right = '0%'
  componentDidMount() {
    // Prevent sticky mouse on window leave
    window.addEventListener('mouseup', this.onMouseUp, true);
    // Allow mouse movement to persist when outside of element
    window.addEventListener('mousemove', this.onMouseMove, true);
    // Sorry, React

    this.focusedReaction = reaction(() => this.props.common.focusedCrane, () => {
      const { common } = this.props;
      this._startRects = this.container.getClientRects()[0];
      if (common.focusedCrane) {
        const crane = this._getFocusedCrane();
        this.left = this.stringToPercents(crane.start).left;
        this.right = this.stringToPercents(crane.end).right;
      } else {
        this.left = common.timeBarBounds.left;
        this.right = common.timeBarBounds.right;
      }
      // WARNING: forces render; avoids a bug where click on a craneHandle isn't detected
      // after a switch out of focus mode
      // WARNING: second argument prevents algorithm from running
      // this prevent a superfluous updates to algo-dependent components when entering focus mode
      this._updateCranes(true, true); 
    });

    // This observable is literally just take in a new Date.now()
    // values from the CraneBar component; the ONLY reason I'm not
    // triggering an old fashioned event is in hopes that MobX's synchronous nature will
    // be less likely to introduce bugs
    this.remoteFocusMoveReaction = reaction(() => this.props.common.movingFocusedCrane, () => {
      const { common } = this.props;
      this._startRects = this.container.getClientRects()[0];
      const crane = this._getFocusedCrane();
      this.left = this.stringToPercents(crane.start).left;
      this.right = this.stringToPercents(crane.end).right;
      this._updateCranes();
    });

  }

  componentWillUnmount() {
    window.removeEventListener('mouseup', this.onMouseUp, true);
    window.removeEventListener('mousemove', this.onMouseMove, true);
    this.focusedReaction();
    this.remoteFocusMoveReaction();
  }

  moveBar = (e) => {
    // if (!e.clientX) return;
    const offset = e.clientX - this._startX;
    const { left, right } = this._startRects;
    const { common } = this.props;
    const rightMost = right;
    const leftMost = left;
    let newLeft = this._startLeft + offset;
    let newRight = this._startRight - offset;
    if (newRight < 0) {
      newLeft = newRight + newLeft;
      newRight = 0;
    }
    if (newLeft < 0) {
      newRight = newRight + newLeft;
      newLeft = 0;
    }

    this.left = this.toPercent(newLeft);
    this.right = this.toPercent(newRight);
    // common.timeBarBounds = {
    //   left: this.toPercent(newLeft),
    //   right: this.toPercent(newRight),
    // };

  }

  resizeLeft = (e) => {
    const { min, max } = Math;
    const { toPercent, toPixels } = this;
    const { common } = this.props;
    const offset = e.clientX - this._startX;
    const newPosition = this._startLeft + offset;
    const rightClamp = this._startRects.width-toPixels(this.right)-5;

    this.left = toPercent(min(max(newPosition, 0), rightClamp));
    // console.log('????', common.timeBarBounds);
    // common.timeBarBounds = Object.assign(common.timeBarBounds, { left: newLeftPercent });
  }

  resizeRight = (e) => {
    const { min, max } = Math;
    const { toPercent, toPixels } = this;
    const { common } = this.props;
    const offset = e.clientX - this._startX;
    const newPosition = this._startRight - offset;
    const leftClamp = this._startRects.width-toPixels(this.left)-5;

    this.right = toPercent(min(max(newPosition, 0), leftClamp));
    // common.timeBarBounds = Object.assign(common.timeBarBounds, { right: newRightPercent });
  }

  get leftDate() {
    const { common } = this.props;
    const leftPercent = this.toPixels(this.left)/this._startRects.width;
    const leftDomain = Math.round(this.domainStart + leftPercent*this.domainWidth);
    return new Date(leftDomain);
  }
  get rightDate() {
    const { common } = this.props;
    const rightPercent = (this._startRects.width - this.toPixels(this.right))/this._startRects.width;
    const rightDomain = Math.round(this.domainStart + rightPercent*this.domainWidth);
    return new Date(rightDomain);
  }


  toPercent = (pixels) => pixels/this._startRects.width * 100 + '%'
  toPixels = (percent) => parseFloat(percent)/100 * this._startRects.width
  // Will make these @computed later. Lazy now.
  get domainStart() {
    const { start } = this.props.common.projectDates;
    return start ? start.getTime() : 0;
  }

  get domainEnd() {
    const { end } = this.props.common.projectDates;
    return end ? end.getTime() : 100;
  }

  get domainWidth() {
    return this.domainEnd - this.domainStart;
  }

  _initClick = (e) => {
    this._startRects = this.container.getClientRects()[0];
    const { left, width } = this._startRects;
    const { common } = this.props;
    this._startOffsetLeft = e.clientX - left;
    this._startOffsetRight = width - this._startOffsetLeft;
    this._startX = e.clientX;

    this._startLeft = this.toPixels(this.left);
    this._startRight = this.toPixels(this.right);
  }

  outerResize = (e) => {
    if (this._startOffsetLeft < this._startLeft) {
      this.resizeLeft(e);
    } else if (this._startOffsetRight < this._startRight) {
      this.resizeRight(e);
    }
  }

  onMouseDown = (e) => {
    const className = e.target.className.split(' ')[0];
    this._action = {
      'inner-bar-left-handle': 'resizeLeft',
      'inner-bar-right-handle': 'resizeRight',
      'inner-bar': 'moveBar',
      'outer-bar-container': 'outerResize',
    }[className];
    if (!this._action) return;
    this.props.common.animateTimeBar = false;
    e.preventDefault();
    this._initClick(e);
  }

  onMouseUp = (e) => {
    if (!this._action) return;
    e.preventDefault();
    // Maybe this should be passed in as an 'onEnd' prop
    this._updateCranes();
    this._action = null;
    this.props.common.animateTimeBar = true;
  }

  _updateCranes = (forceRender, skipAlgo) => {
    const { common, scenario } = this.props;
    const { leftDate, rightDate } = this;
    common.cranesWindow = { 
      start: leftDate,
      end: rightDate
    };
    // WARNING: ok... filterScenario and updateCranesWindow both get called 
    // if there's a focused crane, which definitely SHOULDN'T happen...
    // For now, forceRender is avoiding a bug (see reaction above)
    common.setCranesFormFromDate(leftDate, rightDate);
    if (common.focusedCrane) {
      common.updateCraneWindow(this._getFocusedCrane(), scenario, leftDate, rightDate, forceRender, skipAlgo);
    } else {
      //NOTE: filterScenario doesn't call the algorithm, so no need to pass skipAlgo
      common.filterScenario(leftDate, rightDate, forceRender);
      const { left, right } = this;
      common.timeBarBounds = { left, right };
    }
  }

  onMouseMove = (e) => {
    // WARNING: this is getting attached to the window and 
    // running *constantly* when the TimeBar component is loaded
    if (!this._action) return;
    const action = this[this._action];
    action(e);
    this._updateCranes();
    e.preventDefault();
  }

  stringToPercents(dateString) {
    const dateNum = new Date(dateString).getTime();
    const left = Math.min(Math.max(0,(dateNum-this.domainStart)/this.domainWidth * 100),100)  + '%';
    const right = Math.min(Math.max(0, (this.domainEnd - dateNum)/this.domainWidth * 100),100) + '%';
    return { left, right };
  }

  _getFocusedCrane() {
    // This is to make sure that the focused crane has the latest data
    // WARNING: it would really be better to make the common.focusedCrane JUST be a UUID
    const { common } = this.props;
    if (!common.focusedCrane) return null;
    return common.currentScenario.filter(crane => crane.uuid === common.focusedCrane.data.uuid)[0];
  }

  render() {
    const { common } = this.props;
    let handleColor, innerColor, gripColor;
    let expStartPercent, expEndPercent, candidateStartPercent;
    if (common.focusedCrane) {
      const crane = this._getFocusedCrane();
      const { color, start, end } = crane;
      const darker = d3.hcl(color).darker(0.70).toString();
      handleColor = { backgroundColor: d3.hcl(color).brighter(0.35).toString() };
      innerColor = { backgroundColor: d3.hcl(color).brighter(0.35).toString(), border: `solid 1px ${darker}` };
      gripColor = { borderLeft: `1px solid ${darker}`, borderRight: `1px solid ${darker}` };
      this.left = this.stringToPercents(crane.start).left;
      this.right = this.stringToPercents(crane.end).right;

      if (common.algoMap.size) {
        const algoCrane = common.algoMap.get('cranes')[crane.uuid];
        if (algoCrane) {
          let { expStart, expEnd } = algoCrane;
          // For visualization purposes, if gap is < 72 hours, don't set expStartPercent
          // This will effectively make the crane inactive
          // This might make it tough to stay consistent with other features... But we'll see
          if (expEnd) expEndPercent = this.stringToPercents(expEnd).left;
          if (expStart) candidateStartPercent = this.stringToPercents(expStart).right;
          if (expStart && expEnd-expStart >= 2.592e8 && 
                parseFloat(expEndPercent) > parseFloat(this.left) && 
                parseFloat(candidateStartPercent) > parseFloat(this.right)) {
            expStartPercent = candidateStartPercent;
          } else {
            handleColor = { backgroundColor: '#ddd' };
            innerColor = { backgroundColor: '#ddd', border: `solid 1px #aaa` };
            gripColor = { borderLeft: `1px solid #aaa`, borderRight: `1px solid #aaa` };
            // update the crane in question heree
          }

          // console.warn("WOOT", expStart, expEnd);
          // console.warn('PERCENT', expStartPercent, expEndPercent);
        } 
      }


    } else {
      this.left = common.timeBarBounds.left;
      this.right = common.timeBarBounds.right;
    }
    const { left, right } = this;

    // console.warn('LEFT DRAG FIX:', parseFloat(left), parseFloat(expEndPercent));
    // console.warn('RIGHT DRAG FIX: ', parseFloat(expStartPercent), parseFloat(right));

    // if (common.focusedCrane) {
    //   expStartPercent = '80%';
    //   expEndPercent = '80%';
    // }

    // This is garbage for animating schedule entry from the center of the TimeBar
    // const leftFloat = parseFloat(left);
    // const rightFromLeftFloat = 100-parseFloat(right);
    // const centerWidthFloat = (rightFromLeftFloat-leftFloat)/2;
    // const centerLeft = leftFloat + centerWidthFloat;
    // const centerRight = parseFloat(right) + centerWidthFloat;

    const overlayRight = expStartPercent && parseFloat(right) < parseFloat(expStartPercent) ? expStartPercent : 
                          100-parseFloat(left) + '%';
                          // Math.max(parseFloat(right), centerRight) + '%';
    const overlayLeft = expStartPercent && parseFloat(left) < parseFloat(expEndPercent) ? expEndPercent : 
                          100-parseFloat(right) + '%';
                          // Math.max(parseFloat(left), centerLeft) + '%';


    return (
      <div className="outer-bar-container time-bar"
        onMouseDown={this.onMouseDown}
        onMouseMove={this.onMouseMove}
        ref={ref => this.container = ref}
      >
        <div className="outer-bar"/>
        <div 
          className={`inner-bar ${common.animateTimeBar ? 'animate' : ''}`}
          style={Object.assign({ left, right }, innerColor ? innerColor : null)}
        >
          <div className="inner-bar-left-handle" style={handleColor ? handleColor : {}}>
            <div className="inner-bar-grip" style={gripColor ? gripColor : {}}/>
          </div>
          <div className="inner-bar-right-handle" style={handleColor ? handleColor : {}}>
            <div className="inner-bar-grip" style={gripColor ? gripColor : {}}/>
          </div>
        </div>
        <div 
          key={`overlay-left-timebar`} 
          className={`inner-bar-inactivity-overlay left ${expStartPercent && parseFloat(right) < parseFloat(expStartPercent) ? 'scheduled' : ''} ${common.animateTimeBar ? 'animate' : ''}`}
          style={{
            left, 
            right: overlayRight, 
            mixBlendMode: 'hard-light',
            opacity: expStartPercent && common.focusedCrane ? 1 : 0,
          }}/>
        <div 
          key={`overlay--right-timebar`}
          className={`inner-bar-inactivity-overlay right ${expStartPercent && parseFloat(left) < parseFloat(expEndPercent) ? 'scheduled' : ''} ${common.animateTimeBar ? 'animate' : ''}`}
          style={{
            left: overlayLeft, 
            right, 
            mixBlendMode: 'hard-light',
            opacity: expStartPercent && common.focusedCrane ? 1 : 0,
          }}
        />
      </div>
    );
  }

}
