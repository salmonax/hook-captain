import React, { Component } from 'react';

import Optimal from 'routes/optimal';
import Editor from 'routes/editor';
import Performance from 'routes/performance';
import Data from 'routes/data';

import { Route, Link } from 'react-router-dom';
import { Layout, Menu, Breadcrumb, Icon, Row, Col, Spin } from 'flux-ui';
import 'flux-ui/lib/styles'
import './index.scss';
const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

import appLogo from 'icon@2x.png';
// import clientLogo from 'pwc-placeholder.png';
import clientLogo from 'logo-new.png';


import { fluxHelpers } from 'services';

import { parse } from 'query-string';

import { observer, inject } from 'mobx-react';

import { withRouter } from 'react-router';
import { toJS } from 'mobx';

const SCENARIOS = `Scenario A, Scenario B, Scenario C`.split(', ')

// Handle some bootstrapping stuff
require('lib/workerLoader');
import { initFont } from 'components/viewports/cranes/lib';
initFont('assets/proxima-nova-semibold.json');

let _authenticated = false;
let __FLUX__ = `{"credentials":{"clientId":"6fe335b6-7eba-4606-ad2c-9adee2363464","fluxToken":"MTUyMjM2MDA4MXxwRzVlMFYtUms2X3NLSVV5TGZBNS1ObThjdHVDXzNVRFBZcUNzblNzam5fc1B4R0t4b1U0UjVoNDRuTU9LbTNxUGNYNjQ1cVFXVUhKTU0tdkRiVGJveV9LZWF1cTlBVmVBeTBydjBmVjhCcEQ2SmZBZ0phZEhZQ2x5T192MktENDIzSDBnNHh1bUhaeFo5eGlLUkk9fMkWrnKXvzNLfhG4S57lYzcZRZUVU5C-r0M3Zsf0Ttel","idToken":{"header":{"alg":"RS256","typ":"JWT"},"payload":{"aud":"6fe335b6-7eba-4606-ad2c-9adee2363464","display_name":"George M.","email":"george@flux.io","exp":1525046211,"family_name":"Michel","flux_id_hash":"a2pAlmRkv7g5OdZWe","given_name":"George","iat":1522454211,"iss":"https://flux.io/","nonce":"Mjg0Mjc4ODMyOTIxNTg2NDk0ODc1MTY4NTA5NjUxOTgwNzgw","preferred_username":"george@flux.io","sub":"a2pAlmRkv7g5OdZWe"},"signature":"sb8sYht7YS894zGilB7sQERdaEnzPczI4ObYmof81CBTuTz3BRpR8zr3pfRHHxsmmLMoW2Etmjz8sYQIOaPtD75vervsjcUT6qIGubzv_AuIfzvSHF1fhmz7kbfXAolX7lvLtFCQ60PckeS6HASIozq_Ly9D71L9_peEFZzT3yXaP6fY5ic57PNQvaeJ8phC8Xb7_A9l8WrvbOy53Piz_9wO3vTcao_ctF6LVmI5huZoQRbVjNLkg7DQKs-z-Xy0LPJ9SOF5pz-MOYw8Q7nYDElCEFm31_cmhhz9t4S5_XY8WInhaGWw2CTYoImktHC3WwALmazZCcKCf4amGN6dvA"},"accessToken":"1~4bfee2da-844f-460b-84cf-607185372849","tokenExpiry":1525046211,"tokenType":"Bearer","clientInfo":{"ClientId":"6fe335b6-7eba-4606-ad2c-9adee2363464","ClientVersion":"","SDKName":"Flux Javascript SDK","SDKVersion":"0.4.9","OS":"browser/js-sdk/0.4.9"}}}`
window.localStorage.setItem('__FLUX__', __FLUX__)
Object.assign(fluxHelpers, {
  isLoggedIn: () => true,
  storeFluxUser: () => {
    return Promise.resolve()
    // if (_authenticated) return Promise.resolve()
    // return Promise.reject()
  },
  redirectToFluxLogin: () => {
    window.location = '/login'
  }
})

window.fluxHelpers = fluxHelpers


@withRouter @inject('common') @observer
export default class App extends Component {

  state = {
    collapsed: false,
    mode: 'inline',
    projects: [],
    isLoggedIn: false,
    isHoveringOn: [false, false, false],
    optimalIndex: -1,
  };

  onCollapse = (collapsed) => {
    setTimeout(() => 
      window.dispatchEvent(new Event('resize')), 200); // recalculate all canvases
    this.setState({
      collapsed,
      mode: collapsed ? 'vertical' : 'inline',
    });
  };

  store = this.props.common;

  __getTitleFromPath() {
    // note: kludgy and gross; could refactor headers into routes or just use this
    const { pathname, search } = window.location;
    const titleMap = {
      '/optimal': 'Optimal Scenario Primary',
      '/performance': 'Comparing Scenario Performance',
      '/editor': 'Scenario Editor',
      '/': 'Source Data',
    };
    const primary = titleMap[pathname] || '';
    let secondary = parse(search).item || '';
    if (pathname === '/data') {
      const { selectedProject } = this.props.common;
      if (selectedProject) secondary = selectedProject.name;
    } 
    return { primary, secondary };
  }


  handleLogin = () => {
    console.error('weoweiwe')
    fluxHelpers.redirectToFluxLogin();
  }

  handleLogout = () => {
    fluxHelpers.logout();
    this.setState({
      isLoggedIn: false,
    });
    this._doLogin(); // no actual "logged out screen", so putting this here for now
  }

  _doLogin() {
    // Check if we're coming back from Flux with the login credentials.
    // Note: have store handle this at some point
    return fluxHelpers.storeFluxUser()
      .then(() => {
        // console.log('!!!')
        return fluxHelpers.getUser().listProjects()
        // console.log(foo)

      })
      .then(data => {
        const projects = data.entities;
        this.setState({
          isLoggedIn: fluxHelpers.isLoggedIn(),
          projects,
        });
        this.store.projects = this.state.projects; // Hmmmmm... 
        console.warn(this.store.projects)
        // console.log(this.store.projects.forEach(item => console.log( toJS(item) )))
        return projects;
      })
      .catch((err) => {
        this.handleLogin();
        // if (/(No user credentials stored|fetch)/.test(err)) this.handleLogin();
        throw new Error(err);
      });
  }

  componentDidMount() {
    if (window.location.hash === '#authenticated') {
      _authenticated = true
      history.pushState(null, null, window.location.href.split('#')[0]);
    }

    window._s = this.props.common;
    // console.log('here we are back from whoknowswhere')
    this._doLogin().then(() => {
      const { setDefaults, setDefaultSelections } = this.props.common;
      setDefaultSelections();
      setDefaults();
    });
  }
  goToSourceData = () => {
    this.props.history.push('/');
  }

  goToScenario = (scenarioName, e) => {
    this.props.history.push(`/editor?item=${encodeURI(scenarioName)}`)
  }
  showStar = (i, e) => {
    const { isHoveringOn } = this.state;
    const newHoverState = [false, false, false];
    newHoverState[i] = true;
    console.log(newHoverState);
    this.setState({
      isHoveringOn: newHoverState
    });
  }
  hideStar = (i, e) => {
    const { isHoveringOn } = this.state;
    this.setState({
      isHoveringOn: [false, false, false]
    });
  }
  selectOptimal = (i, e) => {
    // Prevent all the flux-ui from firing
    e.preventDefault();
    e.stopPropagation();
    const { optimalIndex } = this.state;
    this.setState({
      optimalIndex: optimalIndex === i ? -1 : i
    });    
  }
  noScenarioAlert = () => alert(`No optimal scenario has been selected yet. Please select a preferred scenario from the sidebar in order to view this page.`);

  render() {
    // Note: the spin should render until everything is ready!
    if (!this.state.isLoggedIn) {
      return (
        <div id="loading-screen">
          <Spin size="large"/>  
        </div>
      );
    }

    const title = this.__getTitleFromPath();
    console.log('props: ', this.props)

    const optimalScenario = SCENARIOS[this.state.optimalIndex];
    return (
      <Layout>
        <Sider
          collapsible
          width={250}
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <div className="app-title">
            <img src={appLogo} style={{float: 'left', width: '34px', height: 'auto' }} onClick={this.goToSourceData} />
            {this.state.collapsed ? null : <div> Hook Captain </div>}
          </div>
          <Menu theme="light" mode={this.state.mode} defaultSelectedKeys={['10']}>
            <Menu.Item key="1">
              <Link to={optimalScenario ? `/optimal?item=${encodeURI(optimalScenario)}` : this.props.location} style={optimalScenario ? {} : {color: '#aaa'}}>
                <Icon type="star-o" />
                <span className="nav-text">Optimal Scenario</span>
              </Link>
            </Menu.Item>
            <SubMenu
              key="sub1"
              title={<span><Icon type="area-chart" /><span className="nav-text">Scenario Performance</span></span>}
            >
              {`Value, Speed, Agility`.split(', ').map((subItem, i) =>
                <Menu.Item key={'perf' + i}>
                  <Link to={`/performance?item=${encodeURI(subItem)}`}>{subItem}</Link>
                </Menu.Item> 
              )}
            </SubMenu>
            <SubMenu
              key="sub2"
              title={<span><Icon type="share-alt" /><span className="nav-text">Scenario Editor</span></span>}
            >
              { SCENARIOS.map((subItem, i) => 
                <Menu.Item key={'editor-' + i} className='nightmare-sider'>
                  <div 
                    key={'editor-' + i} 
                    style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyItems: 'left'}} 
                    onClick={this.goToScenario.bind(null, subItem)}
                    onMouseEnter={this.showStar.bind(null, i)}
                    onMouseLeave={this.hideStar.bind(null, i)}
                  >
                    <div style={{flexGrow: 0, userSelect: 'none', paddingLeft: '63px', paddingRight: '40px', fontSize: '13px'}}>{subItem}</div>
                    <div 
                      style={{
                        marginBottom: '0px', 
                        marginLeft: '0px', 
                        display: (this.state.isHoveringOn[i] || this.state.optimalIndex === i) ? 'block' : 'none'
                      }}
                      onClick={this.selectOptimal.bind(null, i)}
                      onMouseEnter={e => window.target = e.target }
                    >
                      {this.state.optimalIndex !== i ?
                        <Icon type="star-o" className='star-icon' style={{color: 'gray', opacity: 0.5}} /> :
                        <Icon type="star" className='star-icon' style={{color: '#b70025'}} />
                      }
                    </div>
                  </div>   
                </Menu.Item>
              )}
            </SubMenu>
            <Menu.Item key="10">
              <Link to='/'>
                <Icon type="folder-open" />
                <span className="nav-text">Source Data</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          {/*<Header> 
            <div className="route-title"> 
              <span style={{marginLeft: '20px'}}>{title.primary}</span>
              <span style={{margin: '6px', color: '#aaa'}}>{title.secondary}</span>
            </div>
            <div className="logo">
              <img src={clientLogo} style={{width: 'auto', height: '35px'}}/>
            </div>
          </Header>*/}
          <Content>
            <Route path="/optimal" component={Optimal} />
            <Route path="/editor" component={Editor} />
            <Route path="/performance" component={Performance} />
            <Route exact path="/" component={Data} />
          </Content>
        </Layout>
      </Layout>
    );
  }
}
