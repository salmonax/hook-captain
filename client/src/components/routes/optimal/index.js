import React from 'react';
import './index.scss';
import { Grid, Card, Layout } from 'flux-ui';
const { Header } = Layout;
import clientLogo from 'logo-new.png';

import LineChart from 'charts/line';
import PieChart from 'charts/pie';
const { Row, Col } = Grid;

import { parse } from 'query-string';

import { summary } from 'fixtures/data';
import SummaryTable from 'cards/summaryTable';
import CranesViewport from 'viewports/cranes';

import EditorKPIChart from 'charts/editorKPI';
import EditorPerfChart from 'charts/editorPerf';

import { inject, observer } from 'mobx-react';
import EditorTable from 'cards/editorTable';

import OptimalChart from 'charts/optimalChart';


@inject('common') @observer
export default class Optimal extends React.Component {
  constructor(props) {
    super(props);
    this.scenario = null;
  }
  componentDidMount() {
    this.scenario = parse(this.props.location.search).item;
    this.props.common.renewScenario(this.scenario);
  }
  render() {
    const { scenario } = this;
    const { currentScenario, algoScenario, renewScenario } = this.props.common
    const title = {
      primary: 'Optimal Scenario Primary',
      secondary: this.scenario,
    }
    return (
      <div className="container scenario">
        <Header> 
          <div className="route-title"> 
            <span style={{marginLeft: '2px'}}>{title.primary}</span>
            <span style={{margin: '6px', color: '#aaa', marginLeft: '10px' }}>{title.secondary}</span>
          </div>
          <div className="logo">
            <img src={clientLogo} style={{width: 'auto', height: '35px'}}/>
          </div>
        </Header>
        <Row gutter={20} style={{minHeight: '370px'}}>
          <Col span={8}>
            <Card 
              className="viewport"
              title={`Site Plan for ${scenario}`}
              bordered={false}
            >
              <CranesViewport scenario={scenario}/>
            </Card>
          </Col>
          <Col span={8}>
            <Card 
              title={`${scenario} Performance`}
              bordered={false}
            >
              <EditorPerfChart />
            </Card>
          </Col>
          <Col span={8}> 
            <Card 
              title={`KPI Summary for ${scenario}`}
              bordered={false}
            >
            <EditorKPIChart />
            </Card>
          </Col>
        </Row>
        <Row gutter={20}>
          <Col span={12}>
            <Card 
              title="Optimal Scenario Summary"
              bordered={false}
            >
              <EditorTable data={summary} scenario={algoScenario} craneTable={true} />
            </Card>
          </Col>
          <Col span={12}>
            <Card 
              title="Weekly Performance by Scenario"
              bordered={false}
            >
            <OptimalChart scenarioName={this.scenario} />
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}