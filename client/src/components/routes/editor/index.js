import React from 'react';
import './index.scss';
import { Grid, Card, Layout, Icon } from 'flux-ui';
const { Header } = Layout;
import clientLogo from 'logo-new.png';

const { Row, Col } = Grid;

import { parse } from 'query-string';

import { summary } from 'fixtures/data';

import EditorTable from 'cards/editorTable';

import PieChart from 'charts/pie';
import LineChart from 'charts/line';
import StackedAreaChart from 'charts/stackedArea';


import CranesChart from 'charts/cranesChartNew';
import RiskChart from  'charts/risk';
import EditorKPIChart from 'charts/editorKPI';
import EditorPerfChart from 'charts/editorPerf';
import UtilizationChart from 'charts/utilization';
import PriceChart from 'charts/price';
import WeatherChart from 'charts/weather';

import CranesGantt from 'cards/cranesGantt';

import CranesViewport from 'viewports/cranes';
import { inject, observer } from 'mobx-react';



export default inject('common')(observer(Editor));
function Editor(props) {
  const scenario = parse(props.location.search).item;
  // ToDo: only pass currentScenario to cards.
  const { 
    toggleEditorMode, 
    currentScenario, 
    algoScenario, 
    clearCranes,
   } = props.common;

  const { common } = props; // switching to this; was over destructuring

  const craneEditorTitle = (
    <div style={{display: 'flex', flexDirection: 'row'}}> 
      <div>Site Plan for {scenario}</div>
      <div className='flux-focus' style={{marginLeft: 'auto', cursor: 'pointer', userSelect: 'none' }} onClick={toggleEditorMode}>
        + Edit Cranes
      </div>
      <div style={{marginLeft: '10px', cursor: 'pointer', userSelect: 'none', color: 'rgb(150,50,50)'}} onClick={clearCranes.bind(null, scenario)}>
        - Clear Cranes
      </div>
    </div>

  );
  const summaryTableTitle = (
    <div style={{display: 'flex', flexDirection: 'row'}}>
      <div>Scenario Summary</div>
      <div 
        style={Object.assign({
            marginLeft: 'auto',
            cursor: 'pointer',
            userSelect: 'none' 
          },
          common.showCranesSchedule ?
            {} :
            { color: '#aaa'}
        )} 
        onClick={() => common.showCranesSchedule = true }
      >
        <Icon type="clock-circle-o" />
      </div>
      <div 
        style={Object.assign({
            marginLeft: '10px', 
            cursor: 'pointer', 
            userSelect: 'none', 
          },
          !common.showCranesSchedule ? 
            {} : 
            { color: '#aaa' }
        )} 
        onClick={() => common.showCranesSchedule = false }
      >
        <Icon type="calendar" />
      </div>
    </div>
  );

  const title = {
    primary: 'Scenario Editor',
    secondary: scenario,
  };

  return (
    <div className="container scenario editor">
      <Header> 
        <div className="route-title"> 
          <span style={{marginLeft: '2px'}}>{title.primary}</span>
          <span style={{margin: '6px', color: '#aaa', marginLeft: '10px'}}>{title.secondary}</span>
        </div>
        <div className="logo">
          <img src={clientLogo} style={{width: 'auto', height: '35px'}}/>
        </div>
      </Header>
      <Row gutter={20} className='viewport-row'>
        <Col span={16}>
          <Card 
            title={craneEditorTitle}
            bordered={false}
            className='cranes-viewport-card'
          >
            <CranesViewport editable={true} scenario={scenario} />
          </Card>
        </Col>
        <Col span={8}> 
          <Card 
            title={`KPI Summary for ${scenario}`}
            bordered={false}
          >
            <EditorKPIChart/>
          </Card>
        </Col>
      </Row>
      <Row gutter={20} style={{minHeight: '350px'}}>
        <Col span={6}>
          <Card 
            title="Performance"
            bordered={false}
          >
            <EditorPerfChart/>
          </Card>
        </Col>
        <Col span={6}>
          <Card 
            title="Crane Types"
            bordered={false}
          >
            <div className="subtitle" style={{fontWeight: 'bold', fontSize: '13px', paddingTop: '3px', paddingLeft: '12px'}}>Percentage of crane types placed in Scenario B:</div>
            <CranesChart scenario={scenario}/>
          </Card>
        </Col>
        <Col span={12}>
          <Card 
            className="summary"
            title={summaryTableTitle}
            bordered={false}
          > 
            { common.showCranesSchedule ? 
                <CranesGantt scenario={scenario} /> :
                <EditorTable data={summary} scenario={algoScenario} craneTable={true} />    
            }
            
            {/*<EditorTable data={summary} scenario={algoScenario} craneTable={true} />*/}
          </Card>
        </Col>
      </Row>
      <Row gutter={20}>
        <Col span={8}>
          <Card 
            title="Wind Risk"
            bordered={false}
          >
            <WeatherChart />
          </Card>
        </Col>
        <Col span={8}>
          <Card 
            title="Total Estimated Cost"
            bordered={false}
          >
            <StackedAreaChart />
          </Card>
        </Col>
        <Col span={8}>
          <Card 
            title="Crane Activity"
            bordered={false}
          >
            <UtilizationChart scenario={currentScenario} />
          </Card>
        </Col>
      </Row>
    </div>
  );
}
