import React from 'react';
import './index.scss';
import { Grid, Card, Layout } from 'flux-ui';

// IMPORTANT: move the header to its own component
const { Header } = Layout;
import clientLogo from 'logo-new.png';

import LineChart from 'charts/line';
import PieChart from 'charts/pie';
const { Row, Col } = Grid;

import { parse } from 'query-string';

import BubbleChart from 'charts/bubble';
import StackedChart from 'charts/stacked';
import BreakdownChart from 'charts/breakdown';

import NewLine from 'charts/newline';

import { inject, observer } from 'mobx-react';

import d3 from 'd3';

const dummyCategories = [
  { label: 'Zone A', color: '#f00' },
  { label: 'Zone B', color: '#0f0' },
  { label: 'Zone C', color: '#00f' },
];


const COLORS = {
  Value: {
    'Scenario A': '#3EE6AC',
    'Scenario B': '#19C388',
    'Scenario C': '#0D835A',
  },
  Speed: {
    'Scenario A': '#87ABFF',
    'Scenario B': '#497DF9',
    'Scenario C': '#214AA6',
  },
  Agility: {
    'Scenario A': '#FDA3D2',
    'Scenario B': '#F758AB',
    'Scenario C': '#B6186A',
    
  }
};

const LEGEND_COLORS = {
  Value: '#19C388',
  Speed: '#497DF9',
  Agility: '#E82089',
};

// NOTE: colors are generated dynamically via d3
const LEGEND_MAP = {
  Value: [
    { label: 'Installation' },
    { label: 'Operation' },
    { label: 'Delay' },
    { label: 'Maintenance' },
  ],
  Speed: [
    { label: 'Zone A' },
    { label: 'Zone B' },
    { label: 'Zone C' }
  ],
  Agility: [
    { label: 'Zone A' },
    { label: 'Zone B' },
    { label: 'Zone C' }
  ],
};


function Legend(props) {
  const { assign } = Object;
  const flexRowStyle = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  };
  return (
    <div style={assign(flexRowStyle, props.style)}>
      {props.labels.map(item => 
        <div key={item.label} style={assign(flexRowStyle, {fontSize: '10px'})}>
          <div style={{backgroundColor: item.color, width: '8px', height: '8px'}}></div>
          <div style={{margin: '0px 15px 0px 5px'}}>{item.label}</div>
        </div>
      )}
    </div>
  );
}

export default inject('common')(observer(Scenario));
function Scenario(props) {
  const indicator = parse(props.location.search).item;
  for (let key in LEGEND_MAP) {
    LEGEND_MAP[key].forEach((item, i) => {
      item.color = d3.hcl(LEGEND_COLORS[indicator]).brighter(i/2).toString();
    });
  }
  // console.warn(LEGEND_MAP);

  const effect = { 'Speed': 'Time', 'Agility': 'Risk' }[indicator] || 'Cost';
  // const chartKey = { 'Value': 'KPI_cost', 'Speed': 'KPI_time', 'Agility': 'KPI_risk' }[indicator];
  const weeklyKey = { 'Value': 'cumCost', 'Speed': 'cumTime', 'Agility': 'cumRisk' }[indicator];
  const bubbleKey = { 'Value': 'KPI_Value', 'Speed': 'KPI_Speed', 'Agility': 'KPI_Agility' }[indicator];
  const breakdownsKey = {
    Value: 'Project Cost Breakdown',
    Speed: 'Project Time Breakdown',
    Agility: 'Project Risk Breakdown',
  }[indicator];

  const title = {
    primary: 'Comparing Scenario Performance',
    secondary: indicator
  }

  return (
    <div className="container scenario">
      <Header> 
        <div className="route-title"> 
          <span style={{marginLeft: '2px'}}>{title.primary}</span>
          <span style={{margin: '6px', color: '#aaa', marginLeft: '10px'}}>{title.secondary}</span>
        </div>
        <div className="logo">
          <img src={clientLogo} style={{width: 'auto', height: '35px'}}/>
        </div>
      </Header>
      <Row gutter={20}>
        <Col span={24}>
          <Card 
            title={`Weekly Project Total ${effect}`}
            bordered={false}
            className="summary-noscroll"
            style={{overflowY: 'hidden !important'}}
          >
            <NewLine chartKey={weeklyKey} indicator={indicator} />
          </Card>
        </Col>
      </Row>
      <Row gutter={20}>
        <Col span={12}>
          <Card 
            title={`Final KPI ${indicator}`}
            bordered={false}
          >
            {/*<Legend labels={dummyCategories}/>*/}
            <BubbleChart chartKey={bubbleKey} indicator={indicator} />
          </Card>
        </Col>
        <Col span={12}>
          <Card 
            title={`Total ${effect} Breakdown`}
            bordered={false}
          >
            <Legend labels={LEGEND_MAP[indicator]} style={{marginTop: '1px'}} />
            <StackedChart chartKey={breakdownsKey} indicator={indicator} />
          </Card>
        </Col>
      </Row>
    </div>
  );
}
