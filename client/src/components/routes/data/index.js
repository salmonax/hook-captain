import React from 'react';
import './index.scss';
import { Grid, Card, Spin } from 'flux-ui';
const { Row, Col } = Grid;

import { Select, Layout } from 'flux-ui';
const Option = Select.Option;
const { Header } = Layout;
import clientLogo from 'logo-new.png';

import LineChart from 'components/charts/line';
import PieChart from 'components/charts/pie';

import { parse } from 'query-string';

import SummaryTable from 'cards/summaryTable';
import StockViewport from 'viewports/stock';
import { observer, inject } from 'mobx-react';
import { toJS } from 'mobx';




function handleChange(value) {
  console.log(`selected ${value}`);
}

const pwcOrange = '#c42d00';
function ProgressModal(props) {
  return (
    <div style={{
      position: 'absolute',
      background: 'rgba(0,0,0,0.5)',
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      zIndex: 30
    }}>
      {!props.spinner ? 
        <div style={{
          zIndex: 40, 
          background: 'white', 
          marginBottom: '48px', 
          padding: '15px 20px', 
          width: '50%',
          borderRadius: '4px',
        }}>
          <div style={{ width: '100%', borderRadius: '4px', position: 'relative', height: '24px' }}>
            <div style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              borderRadius: '4px',
              border: '1px solid #c5c5c5',
            }}/>
            <div style={{
              position: 'absolute',
              margin: '0px',
              borderRadius: props.progress < 100 ? '4px 0px 0px 4px' : '4px',
              width: props.progress + '%', 
              height: '100%',
              background: '#90132c',
              mixBlendMode: 'multiply',
            }}/>
          </div>
          <div style={{textAlign: 'center', width: '100%', paddingTop: '10px'}}>
            {props.loading ? `Loading: ${props.loading}` : 'Processing...'}
          </div>
        </div> :
        <div> 
          <div style={{
            borderRadius: '6px',
            padding: '20px 25px',
            paddingTop: '24px',
            background: 'white',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: '48px',
          }}>
            <Spin/>
            <div style={{marginTop: '16px'}}> Loading BIM model... </div>
            <div> This may take a few moments. </div>
          </div>
        </div>
      }
    </div>
  );

}


export default inject('common')(observer(Data));
function Data(props) {
  const queryProject = parse(props.location.search).item;
  const storeProject = (this.props.common.selectedProject) ? this.props.common.selectedProject.name : null;
  const project = storeProject || queryProject;

  const store = this.props.common;

  const { projects, pwc, selectProject, selectedProject, cells, selectCell, jobs, cranes, stock, selectElementCell, loadSourceData } = props.common; // bleh
  const sourceTitle = (
    <div style={{display: 'flex', flexDirection: 'row'}}>
      <div>
        {`Resource Selector `}
      </div>
      <div style={{marginLeft: 'auto', marginRight: '15px' }}>
        Flux Project: 
      </div>
      <div>
        <Select value={selectedProject.id || "Select a Project" } style={{ width: 250 }} onChange={selectProject}>
          {projects.map(project => 
            <Option key={project.id} value={project.id}>
              {`${project.name}`}
            </Option>
          )}
        </Select>
      </div>
    </div>
  );

  const { selections } = store;

  const __foo = (label) => label === 'Job Listing' && 'Lift Schedule' || label;

  const selectionOptions = cells.length ? 
    cells.map(cell => <Option key={cell.id} value={cell.id}>{`${__foo(cell.label)}`}</Option>) :
    (<Option key={'default'} value={'default'}>
      <span style={{color: 'rgba(0,0,0,0.25)'}}>No items.</span>
    </Option>);

  const title = {
    primary: 'Source Data',
    secondary: queryProject
  }

  return (
    <div className="container data">
      <Header> 
        <div className="route-title"> 
          <span style={{marginLeft: '2px'}}>{title.primary}</span>
          <span style={{margin: '6px', color: '#aaa', marginLeft: '10px'}}>{title.secondary}</span>
        </div>
        <div className="logo">
          <img src={clientLogo} style={{width: 'auto', height: '35px'}}/>
        </div>
      </Header>
      <Row gutter={20} style={{minHeight: '260px'}}>
        <Col span={24}>
          <Card 
            className="source"
            title={sourceTitle}
            bordered={false}
          >
            {<Col span={12} style={{display: 'flex', flexDirection: 'column', paddingTop: '4px'}}>
              <h3>Element in Stock</h3>
              <div style={{display: 'flex', flexDirection: 'row'}}>
                {Array(Math.ceil(props.common.elementCount/3)).fill().map((_, i) => {
                  return (
                    <Col key={'outer-'+i} span={24/Math.ceil(props.common.elementCount/3)} style={{margin: 0, padding: 0 }}>
                      {Array(props.common.elementCount).fill().slice(i*3,Math.min(props.common.elementCount, i*3+3)).map((_, j) => {
                        const index = 3*i+j;
                        return (
                          <div key={i+'-'+j} style={{marginLeft: i ? '4px' : '0px', marginBottom: '10px'}}>
                            <Select value={index < selections.elements.length ? selections.elements[index] : "Select a Key"} 
                                    style={{ width: '100%' }} 
                                    onChange={selectCell.bind(null, 'elements', index )} 
                            >
                              { selectionOptions }
                            </Select>
                          </div>
                        );
                      })}
                    </Col>
                  );
                })}
              </div>
              <div style={{display: 'flex', flexDirection: 'row', marginTop: 'auto'}}>
                <div onClick={store.addElement} className='flux-focus' style={{fontSize: '14px', fontWeight: 'bold', cursor: 'pointer', userSelect: 'none' }}>+ Add Flux Key</div>
                <div onClick={store.removeElement} className='flux-focus' style={{marginLeft: '20px', marginTop: 'auto', fontSize: '14px', fontWeight: 'bold', cursor: 'pointer', userSelect: 'none' }}>- Remove Flux Key</div>
              </div>
            </Col>}
            <Col span={6} style={{paddingTop: '4px'}}>
              <h3>Lift Schedule</h3>
              <Select value={selections['jobs'] || "Select a Key"} style={{ width: '100%' }} onChange={selectCell.bind(null, 'jobs', null)}>
                { selectionOptions }
              </Select>

              <h3 style={{marginTop: '16px'}}>Crane Types</h3>
              <Select value={selections['cranes'] || "Select a Key"} style={{ width: '100%' }} onChange={selectCell.bind(null, 'cranes', null)}>
                { selectionOptions }
              </Select>
            </Col>
            <Col span={6} style={{display: 'flex', flexDirection: 'column', paddingTop: '4px'}}>
              <h3>Site Plan</h3>
              <Select value={selections['plan'] || "Select a Key"} style={{ width: '100%' }} onChange={selectCell.bind(null, 'plan', null)}>
                { selectionOptions }
              </Select>
              <h3 style={{marginTop: '16px'}}>Pickup Locations</h3>
              <Select value={selections['pickups'] || "Select a Key"} style={{ width: '100%' }} onChange={selectCell.bind(null, 'pickups', null)}>
                { selectionOptions }
              </Select>
              <div onClick={loadSourceData} className='flux-focus' style={{marginLeft: 'auto',marginTop: 'auto', fontSize: '14px', fontWeight: 'bold', cursor: 'pointer', userSelect: 'none', paddingRight: '6px'}}>Load</div>
            </Col>
          </Card>
        </Col>
      </Row>
      <Row gutter={20} style={{minHeight: '425px'}}>
        <Col span={24}>
          <Card 
            className='summary-noscroll'
            title="Element in Stock"
            bordered={false}
          >
            {this.props.common.showElementProgress ? 
              <ProgressModal 
                progress={this.props.common.elementProgress} 
                loading={this.props.common.elementLoading}
                spinner={this.props.common.elementSpinner}
              /> : 
              null }
            <Col span={12}>
              <StockViewport/>
            </Col>
            <Col span={12} style={{
              paddingLeft: '15px',
              borderLeft: '1px solid #e9e9e9', 
              overflowY: 'auto',
              zIndex: '10', 
              background: 'white' 
            }}>
              <SummaryTable data={stock} />
            </Col>

            }
          </Card>
        </Col>
      </Row>
      <Row gutter={20}>
        <Col span={24}>
          <Card 
            className='summary'
            title="Lift Schedule"
            bordered={false}
          >
            <SummaryTable data={jobs}/>
          </Card>
        </Col>
      </Row>
      <Row gutter={20}>
        <Col span={24}>
          <Card 
            className='summary'
            title="Crane Types"
            bordered={false}
          >
            <SummaryTable data={cranes}/>
          </Card>
        </Col>
      </Row>
    </div>
  );
}