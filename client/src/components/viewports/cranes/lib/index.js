const { 
  Mesh, 
  CircleBufferGeometry,
  PlaneBufferGeometry,
  RingBufferGeometry,
  MeshBasicMaterial, 
  LineBasicMaterial, 
  Group,
  TextureLoader, 
  Raycaster,
  Vector2,
  // Late additions for selection box
  Geometry,
  Vector3,
  LineDashedMaterial,
  Line,
  FontLoader,
  TextGeometry,
} = THREE;

// Need to load fonts asynchronously, but need addCrane to be synchronous for now
// Going to load this as early in the app lifecycle as possible
// and just log a warning if it happens to not be loaded by the time a crane is placed
// Didn't want to make this file stateful and dependent on module caching, but too bad.
let _font = null;

export const initFont = (filePath) => new FontLoader().load(filePath, (font) => _font = font);


export const addCrane = ({ radius, color, coords, scene, uuid, count, _isInactive}, newScene, focused) => {
  const innerRadius = 9;

  const innerGeo = new CircleBufferGeometry(innerRadius, 32);
  const outerGeo = new RingBufferGeometry(innerRadius, innerRadius + 1, 32);
  const haloGeo = new RingBufferGeometry(innerRadius + 1, innerRadius + 6, 32);
  
  const radiusGeo = new CircleBufferGeometry(radius, 64);
  const radiusBorderGeo = new RingBufferGeometry(radius, radius + 0.75, 64);

  const innerMat = new MeshBasicMaterial({ color });
  const outerMat = new MeshBasicMaterial({
    color: 0xffffff,
  });

  const maybeColor = _isInactive ?'#aaa' : color;
  const haloMat = new MeshBasicMaterial({ 
    color: maybeColor, 
    transparent: true,
    opacity: 0.4,
    depthWrite: false,
  });

  const radiusMat = new MeshBasicMaterial({
    color: maybeColor,
    transparent: true,
    opacity: 0.2,
    depthWrite: false,
  });

  console.warn(focused);

  const radiusBorderMat = new MeshBasicMaterial({ color: maybeColor });

  const innerCircle = new Mesh(innerGeo, innerMat);
  const outerCircle = new Mesh(outerGeo, outerMat);
  
  /* BEGIN invisible dummy meshes */
  const handleGeo = new CircleBufferGeometry(innerRadius, 32);
  const handleMat = new MeshBasicMaterial({ visible: false, color: '#f00' })
  const handleMesh = new Mesh(handleGeo, handleMat);
  handleMesh.userData = { class: 'crane' };

  const outerHandleGeo = new RingBufferGeometry(innerRadius, radius + 0.75, 64);
  const outerHandleMesh = new Mesh(outerHandleGeo, handleMat);
  outerHandleMesh.userData = { class: 'craneHalo' };
  /* END invisible dummy meshes */

  const haloCircle = new Mesh(haloGeo, haloMat);
  haloCircle.userData = { class: 'craneInnerHalo' };
  const radiusCircle = new Mesh(radiusGeo, radiusMat);
  radiusCircle.userData = { class: 'craneHaloMesh' };
  const radiusBorderCircle = new Mesh(radiusBorderGeo, radiusBorderMat);
  radiusBorderCircle.userData = { class: 'craneHaloBorder' };

  // Selection container
  var selectionGeo = new Geometry();
  const bound = radius+2;
  selectionGeo.vertices.push(
    new Vector3(-bound, bound, 0),
    new Vector3(bound, bound, 0),
    new Vector3(bound, -bound, 0),
    new Vector3(-bound, -bound, 0),
    new Vector3(-bound, bound, 0),
  );
  selectionGeo.computeLineDistances();

  var selectionMat = new LineDashedMaterial({ 
    color: '#900', 
    dashSize: 6, 
    gapSize: 3, 
    linewidth: 1, 
    scale: 1, 
    visible: true 
  });
  var selectionLine = new Line(selectionGeo, selectionMat);
  selectionLine.userData = { class: 'selectionBox' };
  console.warn(focused && focused.uuid === uuid);
  console.warn('!!', focused, uuid);
  selectionLine.visible = !!focused && focused.uuid === uuid;

  const { x, y , z } = coords;
  const zOrderedMeshes = [
    selectionLine,
    radiusCircle, 
    radiusBorderCircle, 
    innerCircle, 
    outerCircle, 
    haloCircle,
  ];
  if (_font) {
    const textGeo = new TextGeometry(count, {
      font: _font,
      size: innerRadius,
      height: 1,
      curveSegments: 3,
    });
    const textMat = new MeshBasicMaterial({ color: '#fff' });
    const textMesh = new Mesh(textGeo, textMat);
    textGeo.computeBoundingBox();
    const { x, y } = textGeo.boundingBox.max;
    const haloRadius = 6;
    const countString = count.toString();
    const nudgeMap = {
      1: count > 10 || count === 11 ? 0.5 : 0.7,
      3: 0.01,
      4: 0.4,
      7: 0.01,
    };
    const xNudge = nudgeMap[count.toString()[0]] || 0.15;
    window.textMesh = textMesh;
    window.textGeo = textGeo;
    textMesh.position.setX(-x/2-xNudge);
    textMesh.position.setY(-y/2+0.14);
    zOrderedMeshes.push(textMesh);
  } else {
    console.warn('WARNING: font not yet loaded.');
  }


  const group = new Group();
  zOrderedMeshes.forEach((mesh, i) => {
    // move cranes up, to ensure they're above the site
    mesh.position.setZ(z + i + 1 + 10000);
    group.add(mesh);
  });

  // move the handle down so it's always below the camera
  // (remember: handleMesh and outerHandleMesh are invisible dummies)
  handleMesh.position.setZ(-3000);
  group.add(handleMesh);
  outerHandleMesh.position.setZ(-3000);
  group.add(outerHandleMesh);

  group.position.set(x, y, z);
  group.userData = { class: 'craneGroup', uuid, count, color };
  // Note: this is a quick kludge; scenes shouldn't be associated with cranes
  // What they really need is a Scenario ID
  if (newScene) return newScene.add(group);
  scene.add(group);
};

// Note: not sure I want anything on this layer handling scene renders
// I think the component-centric handlers should render the scene when this
// layer is done operating on it
export const addRaycastPlane = (scene, fluxViewport) => {
  const geo = new PlaneBufferGeometry(20000, 20000, 8, 8);
  const mat = new MeshBasicMaterial({ 
    visible: false, 
    color: 0xff0000,
  });
  const plane = new Mesh(geo, mat);
  plane.userData = { class: '_rayPlane' };
  plane.position.z -= 3000;
  scene.add(plane);
  fluxViewport.render();
};

export const addDummySite = (scene) => {
  return new Promise((resolve, reject) => {
    const loader = new TextureLoader();
    loader.load(
      'assets/site-plan-new-large-2.png',
      texture => {
        const mat = new MeshBasicMaterial({
          map: texture,
        });
        const geo = new PlaneBufferGeometry(1188, 1188, 4, 4);
        const plane = new Mesh(geo, mat);
        plane.userData = { class: '_dummySite' };
        // Magic numbers to match site plan to geometry
        plane.position.x = 2092.6;
        plane.position.y = 105.3;
        plane.position.z -= 200;
        scene.add(plane);
        resolve(plane);
      }
    );
  });
}

// Replace cranes in the scene with the new set given
export const renewCranes = (cranes, scene, focused) => {
  console.warn('renew cranes called');
  const toRemove = [];
  scene.traverse((node) => {
    if (node instanceof THREE.Group && node.userData.class === 'craneGroup') {
      toRemove.push(node);
    }
  });
  toRemove.forEach(node => scene.remove(node));
  if (cranes.length) {
    cranes.forEach(crane => addCrane(crane, scene, focused));
  }
};

export const getMeshes = (scene) => {
  // console.log('SCENE FROM GETMESHES', scene);
  let meshes = [];
  scene.traverse((node) => {
    if (node instanceof THREE.Mesh) meshes.push(node);
  });
  return meshes;
};


export const getMeshAt = (coords, fluxViewport, filter) => {
  const { _cameras, _scene } = fluxViewport._renderer;
  const raycaster = new Raycaster();
  const mouse = new Vector2(coords.x, coords.y);
  const camera = _cameras.getCamera();
  raycaster.setFromCamera(mouse, camera);

  const meshes = getMeshes(_scene);
  const intersects = raycaster.intersectObjects( meshes, true );
  const filters = Array.isArray(filter) ? filter : [filter];
  if (intersects.length) {
    return filter ? 
      intersects.filter(data => filters.indexOf(data.object.userData.class) !== -1)[0] :
      intersects[0];
  }
  return null;
};
