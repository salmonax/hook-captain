import React from 'react';
import box from 'fixtures/box';
import { viewport } from 'services';
import { 
  addCrane, 
  addRaycastPlane, 
  renewCranes, 
  addDummySite,
  getMeshAt,
} from './lib'; // sceneLayer
import { observer, inject } from 'mobx-react';
import { toJS, reaction } from 'mobx';

import { Select } from 'flux-ui';

import TimeBar from 'widgets/TimeBar';


const Option = Select.Option;

function genUUID () { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

const gbDate = (dateString) => {
  const [ mm, dd, yy ] = new Date(dateString).toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: '2-digit'}).split('/');
  return [ dd, mm, yy ].join('/');
};



@inject('common') @observer
export default class CranesViewport extends React.Component {
  constructor(props) {
    super(props);
    this.viewport = null;
    this.camera = null;
    this.state = {
      width: 300,
      height: 300,
    };
    this.intersects = {};
    this.actualSite = null;
    this.simpleSite = null;
    this.mouse = {}; // for coordinating mouse events

    this.__forceSilentLoad = true;
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.scenario !== this.props.scenario && this.viewport) {
      const { _scene } = this.viewport._renderer;
      const { scenario } = nextProps;
      // const { scenarios, renewScenario, cranesByWindow } = this.props.common;
      const { common } = this.props;
      const cranes = common.cranesByWindow(common.scenarios[scenario]) || [];
      renewCranes(cranes, _scene);
      common.renewScenario(scenario);
      common.focusedCrane = null;
      this.viewport.render();
    }
  }

  componentDidMount() {
    // possibily refactor viewport mount as callback with computed dimensions signature
    this.props.common.editorViewport = this.viewport = viewport.create('#view');
    // eliminate a bit of rendering ceremony, especially when dragging a crane
    this.viewport.__quickRender = () => {
      this.viewport._renderer._context.renderer.clear();
      this.viewport._renderer._context.renderer.render(this.viewport._renderer._scene, this.viewport._renderer._cameras.getCamera());
    }
    this.node = this.viewport._renderer._domParent;
    window.viewport = this.viewport;
    // this.viewport.setGeometryEntity(box);
    window.addEventListener('resize', this._updateDimensions);
    this._updateDimensions();

    // select the first crane in the store, populating common.craneInfo
    this.props.common.selectCrane();


    this._initViewportEvents();

    if (this.props.scenario && this.viewport) {
      const { _scene } = this.viewport._renderer;
      const { scenario } = this.props;
      const { scenarios, renewScenario } = this.props.common;
      const cranes = scenarios[scenario] || [];
      renewCranes(cranes, _scene);
      renewScenario(scenario);
      this.viewport.render();
    }

    const { common } = this.props;
    /* NOTES
       As usual, old tech-debt on common.scenarios[scenarios] and common.currentScenario necessitates a write to the former
       Here, we'll:
        1) iterate across the "write-only" scenario[scenarios] cranes
          1.a) check if each is active by squaring them with the info in the algoMap
          1.b) modify the mesh in three.js scene directly, in order to immediately change the halo color
          1.c) modify the scenario crane, which will persist its state in future scene re-renders triggered by cranesLib.renewCranes()
        2) call renewScenario to update common.currentScenario and re-render the viewport
    */
    this._craneInactivityReaction = reaction(() => common.algoMap.get('cranes'), cranes => {
      if (!cranes) return;
      const { scenarios } = common;
      const { scenario } = this.props;
      let _needsUpdate = false;
      const writeOnlyScenario = scenarios[scenario];
      if (!writeOnlyScenario) return;
      writeOnlyScenario.forEach(sceneCrane => {
        const algoCrane = cranes[sceneCrane.uuid];
        if (!algoCrane) return;
        const { expStart, expEnd } = algoCrane;
        const wasCraneActive = !sceneCrane._isInactive;
        const isCraneActive = !!(expStart && expEnd-expStart >= 2.592e8); // ignoring 'inactive' key in algoMap, for consistency with TimeBar and CraneBar
        if (isCraneActive === wasCraneActive) return;
        const viewportCrane = this.viewport._renderer._scene.children.filter(item => item.userData.uuid === sceneCrane.uuid)[0];
        if (!viewportCrane) { 
          console.warn('WARNING: no viewportCrane found in _craneInactivityReaction');
          return; 
        }
        _needsUpdate = true;
        const color = isCraneActive ? viewportCrane.userData.color : '#aaa';
        const craneMeshes = viewportCrane.children.filter(item =>  ['craneHaloMesh', 'craneInnerHalo', 'craneHaloBorder'].includes(item.userData.class) );
        craneMeshes.forEach(mesh => mesh.material.color = new THREE.Color(color) );
        sceneCrane._isInactive = !isCraneActive;
      });
      if (!_needsUpdate) return;
      // console.warn('----------------------- RENDERED -------------------');
      common.renewScenario(scenario);
      this.viewport.render();  
    });

  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
    this._craneInactivityReaction();
  }
  // Note: would rather this be in viewport service wrapper itself, but it'll do the job for now
  _initViewportEvents() {
    const { _domParent, _cameras, _scene, _controls } = this.viewport._renderer;
    this.viewport._renderer.setView(1);
    const camera = _cameras.getCamera();
    this.camera = camera;
    // this.scene = _scene;

    // camera.zoom = 0.01;

    addRaycastPlane(_scene, this.viewport); 

    // WARNING: this is assuming we're using the right site plan!
    addDummySite(_scene).then(plane => {
      this.simpleSite = plane;
      this.viewport._renderer.focus(plane);
      this.zoomByFrustum(6); // Note: calls render
    });

    // These are the four events dispatched in CameraControls.zoom and .pan
    ['flux-zoom', 'flux-pan'].map(n => 
      document.addEventListener(n, this.showSimpleSite));
    ['flux-pan-end', 'flux-zoom-end'].map(n => 
      document.addEventListener(n, this.showActualSite));

    if (!this.props.editable) return;

    // Try to do these with synthetic events once moved into the wrapper
    this.node.addEventListener('mousemove', this.onMouseMove, false);
    this.node.addEventListener('mousedown', this.onMouseDown);
    this.node.addEventListener('mouseup', this.onMouseUp);
  }

  _updateDimensions = () => {
    if (!this.container instanceof Element) return;
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    this.setState({ width, height });
    this.viewport.setSize(width, height);
  }

  // viewport manipulators

  zoomByFrustum(amount) {
    if (!this.camera) return
    ['top','bottom', 'left', 'right']
      .forEach(n => this.camera[n] /= amount);
    this.camera.updateProjectionMatrix();
    this.viewport.render();
  }

  // NOTE: showSimpleSite and showActualSite rely on monkeypatched flux viewport
  showSimpleSite = () => {
    if (!this.simpleSite || !this.actualSite || this.simpleSite.visible) return;
    if (this.camera.right - this.camera.left > 250) {
      this.actualSite.visible = false;
      this.simpleSite.visible = true;
      this.viewport.render();
    }
  }

  showActualSite = () => {
    if (!this.simpleSite || !this.actualSite || this.actualSite.visible) return;
    if (this.camera.right - this.camera.left < 500) { 
      this.actualSite.visible = true;
      this.simpleSite.visible = false;
      this.viewport.render();
    }
  }

  // controller-likes

  getClickIntersects = ({ offsetX, offsetY}, filter) => {
    // NOTE: this way of getting clientWidth is pre-refactor, could be better
    const x = ( (offsetX + 1) / this.node.clientWidth ) * 2 - 1;
    const y = - ( (offsetY + 1) / this.node.clientHeight ) * 2 + 1;
    return getMeshAt({ x, y }, this.viewport, filter);
  }

  placeCrane = (e) => {
    const { craneInfo, notifyAlgorithm, cranesForm } = this.props.common;
    const intersects = this.getClickIntersects(e);
    const { x, y, z } = intersects.point;
    const { userData } = intersects.object;
    if ( userData.class === 'craneHandle') return;
    // Note: could probably do more scene management through MobX

    const { scenarios, renewScenario, currentScenario } = this.props.common;
    const { scenario } = this.props;
    scenarios[scenario] = scenarios[scenario] || [];
    const crane = {
      count: scenarios[scenario].length + 1,
      id: craneInfo.id || 'default',
      uuid: genUUID(),
      radius: craneInfo.radius || 50,
      color: craneInfo.color || '#000000',
      coords: intersects.point,
      start: cranesForm.start,
      end: cranesForm.end,
      activity: 0,
      scene: this.viewport._renderer._scene, // this is a HUGE antipattern! GET RID OF THIS
    };
    addCrane(crane);
    scenarios[scenario] = scenarios[scenario] || [];
    scenarios[scenario].push(crane); // Note: pushing scene ref into mobx
    renewScenario(scenario);
    this.viewport.render();
    // window.scenarios = scenarios[scenario];
    notifyAlgorithm(this.props.scenario);
  }

  /* Click handlers */

  onMouseMove = (e) => {
    const { common } = this.props;
    if (!toJS(common.isEditing)) return;
    if (!this._isMouseDown) return;
    this._isDragging = true;
    // The next few lines are specifically
    // for UI hiding. It allows there to be some drag
    // before hiding is initiated. (set pixelDistance > 12)
    // Currently, though, this flag is used only for setting
    // UI elements ignore pointer events, so pixelDistance > 0 is fine
    /* BEGIN UI Hiding Code */
    const { start } = this.mouse;
    const { offsetX, offsetY } = e;
    const pixelDistance = Math.sqrt((start.x-offsetX)**2+(start.y-offsetY)**2);
    if (pixelDistance > 0) {
      common.hideEditorUI = this._isDragging;
    }
    /* END UI Hiding Code */

    if (!this._clickedGroup) return;
    e.stopPropagation();
    if (this._isMouseDown !== 'left') return;
    this.showSimpleSite();
    const intersects = this.getClickIntersects(e);
    if (!intersects) return;
    const { x, y } = intersects.point;
    this._clickedGroup.position.setX(x);
    this._clickedGroup.position.setY(y);
    const storeCrane = common.scenarios[this.props.scenario].filter(crane => crane.uuid ===  this._clickedGroup.userData.uuid)[0];
    storeCrane.coords = this._clickedGroup.position;
    // window._clickedGroup = this._clickedGroup;

    this.props.common.random = x+y;
    // Necessary to circumvent the FluxRenderer EditorControls:
    // this.viewport.render();
    this.viewport.__quickRender();

    common.notifyAlgorithm(this.props.scenario);
    common.renewScenario(this.props.scenario);
  }

  onMouseDown = (e) => {
    const { common } = this.props;
    if (this._isMouseDown) this._isMouseDown = false;
    // Show simple plan even if dragging crane
    // Will do nothing if any site is missing
    // showSimpleSite(); 
    if (!toJS(common.isEditing) || !toJS(common.isDataLoaded)) return; // Hmm...
    this.mouse.start = { x: e.offsetX, y: e.offsetY };
    const { which } = e;
    const button = [,'left','middle','right'];
    this._isMouseDown = button[which];
    e.preventDefault();
    // If a crane is focused, also detect halo in order to only cancel on no-crane click
    const intersects = this.getClickIntersects(e, common.focusedCrane ? ['crane', 'craneHalo'] : 'crane');
    console.log('!!!!', intersects);
    if (intersects) {
      this._clickedGroup = intersects.object.parent;
    }
  }

  onMouseUp = (e) => {
    const { common } = this.props;
    if (!toJS(common.isEditing)) return; // Hmm...
    // Show actual plan immediately on mouseup
    // Will do nothing if any site is missing
    this.showActualSite();
    const { _clickedGroup, _isMouseDown } = this;
    if (!_clickedGroup && _isMouseDown === 'left') {
      // Place crane, canceling placement over 6 pixels of movement
        const { x, y } = this.mouse.start;
        const { offsetX, offsetY } = e;
        const pixelDistance = Math.sqrt((x-offsetX)**2+(y-offsetY)**2);
        if (pixelDistance > 6) {
          this._isMouseDown = false;
          common.hideEditorUI = this._isDragging = false;
          return;
        }
      if (!common.focusedCrane) {
        this.placeCrane(e);
        this._lastClickTime = -1; // prevents double click on 1st placement
      } else {
        const { scenarios } = common;
        const { scenario } = this.props;
        const crane = scenarios[scenario].filter(crane => crane.uuid === common.focusedCrane.data.uuid)[0];
        common.focusedCrane = null;
        common.refreshAllCranes(scenario);
        this.viewport.render();
      }
    } else if (_clickedGroup && _isMouseDown === 'right') {
      const { scenarios, renewScenario } = common;
      const { scenario } = this.props;
      const remaining = scenarios[scenario].filter(crane => {
        return (crane.uuid !== _clickedGroup.userData.uuid);
      });
      // Recalculate crane indices on removal
      // Alternative: don't recalculate, but always assign max + 1
      // when creating
      remaining.forEach((crane, i) => crane.count = i+1);

      // TODO: handle focusedCrane business here
      // 1) Only delete if focused, for instance
      common.focusedCrane = null;

      scenarios[scenario] = remaining;
      renewScenario(scenario);
      // For some reason, MobX reaction in TimeBar component breaks normal crane deletion
      // UNLESS I call renewCranes instead of _scene.remove; this is getting difficult to reason about, but
      // I suspsect that it's because I've overused renewCranes (which removes and re-ads scene cranes) to such an extent
      // for time-window filtering that I can no longer guarantee that _clickedGroup is actually still in the viewport
      // even if it represents the same crane... Going to try to tread lightly
      // this.viewport._renderer._scene.remove(_clickedGroup);
      renewCranes(remaining, this.viewport._renderer._scene);

      this.viewport.render();
      common.notifyAlgorithm(this.props.scenario);
    } else if (_clickedGroup && _isMouseDown === 'left') {
      //  WARNING: only detecting double click in this narrow condition
      const { scenarios } = common;
      const { scenario } = this.props;
      if (this._lastClickTime) {
        console.log('LAST CLICK TIME: ', this._lastClickTime)
        if (Date.now() - this._lastClickTime <= 500) {
          console.log('DOUBLE CLICK!', typeof common.editorMode);
          // WARNING: really ought to clean up the crane model so it isn't all over the place like this
          const crane = scenarios[scenario].filter(crane => crane.uuid === _clickedGroup.userData.uuid)[0];
          common.focusedCrane = {
            group: _clickedGroup,
            data: crane,
          };
          common.selectCrane({ target: { value: crane.id }});
          common.refreshAllCranes(scenario);
          this.viewport.render();
        } 
      }
    }
    this._isMouseDown = false;
    common.hideEditorUI = this._isDragging = false;
    this._clickedGroup = null;
    // Note: this doesn't get triggered if there's no crane underneath the mouse on mousedown
    // UNLESS distance is less than 6, which doesn't really make sense; fix later
    this._lastClickTime = this._lastClickTime === -1 ? null : Date.now(); // prevents double click on 1st placement
  }

  render() {
    const { height, width } = this.state;
    const { common, scenario } = this.props;

    // console.log('SHOULD ACTIVATE ACTUAL SITE HERE: ', !this.actualSite, !!common.plan, !!this.viewport, !!common.isPlanLoaded);
    if (common.isPlanLoaded && !this.actualSite && common.plan && this.viewport && !this.planLoadedOnce) {
      console.log('running setGeometryEntity...')
      // WARNING: relying on a 'noRender' 2nd parameter monkeypatch in flux viewport
      const loadSilently = !!this.simpleSite || this.__forceSilentLoad; // prevent re-renders only if an image is pre-loaded
      this.planLoadedOnce = true;
      this.viewport.setGeometryEntity(common.plan.value, loadSilently).then(data => {
        console.log('loaded actual site into scene')
        this.actualSite = data._object;
        if (loadSilently) { 
          this.actualSite.visible = false;
        } else {
          this.zoomByFrustum(6);      
        }
      }).catch(err => this.planLoadedOnce = false);
      
    }
    return (
      <div style={{width: '100%', height: '100%', position: 'relative'}}>
        <CranesSidebar
          visible={common.isEditing && this.props.editable && common.isDataLoaded} 
          cranes={common.cranes} 
          select={common.focusedCrane ? common.updateFocusedCrane.bind(null, scenario) : common.selectCrane }
        />
        <div style={Object.assign({position: 'absolute', bottom: 10, left: 20, right: 20, zIndex: 100}, common.hideEditorUI ? { pointerEvents: 'none' } : {})}> 
          <div style={{display: 'flex'}}>
            <div style={{pointerEvents: 'none', userSelect: 'none'}}>
              {common.projectDates.start ? 
                gbDate(common.projectDates.start) :
                null
              }            
            </div>
            <div style={{marginLeft: 'auto', pointerEvents: 'none', userSelect: 'none'}}>
              {common.projectDates.end ? 
                gbDate(common.projectDates.end) : 
                null
              }
            </div>
          </div>
          <TimeBar scenario={scenario} />
        </div>
        <div 
          id='view' 
          style={{
            display: 'flex',
            justifyContent: 'center',
            width: `${width}px`,
            height: `${height}px`,
            zIndex: -1,
          }}
          ref={(ref) => this.container = ref ? ref.parentNode : null }
        />
      </div>
    );
  }
}

@inject('common') @observer
class CranesSidebar extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (!this.props.cranes.length && nextProps.cranes.length) {
      this.props.select();
    }
  }
  render() {
    const { props } = this;
    const { cranesForm, updateCranesForm } = props.common;
    const { common } = props;
    const today = (new Date(Date.now())).toISOString().split('T')[0];
    const className = (props.visible ? 'cranes-sidebar' : 'cranes-sidebar-hidden') + (common.focusedCrane ? '' : ' focused');
    const craneOptions = props.cranes.length ? 
      props.cranes.map(crane => 
        <option key={crane['Crane ID'] || crane['ID']} value={crane['Crane ID'] || crane['ID']}>
          {`${crane['Crane ID'] || crane['ID']} - ${crane['Type']} (${crane['Maximum Radius'] || crane['Op. Radius']})`}
        </option>
      ) :
      <option key="loading" value="loading">
        Loading...
      </option> 

    return (
      <div className={className} style={common.hideEditorUI ? { pointerEvents: 'none' } : {}}>
        <form>
          <div>
            <label>Type</label>
            <select onChange={props.select} value={common.craneInfo.id}>
              {craneOptions}
            </select>
          </div>
          <div> 
            <label>Start</label>
            <input style={{paddingLeft: '3px'}} type="date" value={cranesForm.start} onChange={updateCranesForm.bind(null, 'start')}></input>
          </div>
          <div>
            <label>End</label>
            <input style={{paddingLeft: '3px'}} type="date" value={cranesForm.end} onChange={updateCranesForm.bind(null, 'end')}></input>
          </div>
        </form>
      </div>
    );
  }
}


@inject('common') @observer
class CraneButtons extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (!this.props.cranes.length && nextProps.cranes.length) {
      this.props.select();
    }
  }
  render() {
    const { props } = this;
    const { common } = props;
    const className = props.visible ? 'crane-buttons' : 'crane-buttons-hidden';
    const craneButtons = props.cranes.length ?
      props.cranes.map(crane => {
        // console.log('!!!!', crane.color);
        return (<div 
          style={{
            margin: '2px',
            padding: '2px 14px',
            whiteSpace: 'nowrap',
            color: 'white',
            backgroundColor: common.createColor(crane['Crane ID'] || crane['ID'])
          }}
          key={crane['Crane ID'] || crane['ID']} 
          value={crane['Crane ID'] || crane['ID']}
          onClick={props.select}
        >
          {`${crane['Crane ID'] || crane['ID']} - ${crane['Type']} (${crane['Maximum Radius'] || crane['Op. Radius']})`}
        </div>);
      }) :
      <div> Loading cranes types... </div>

    return (
      <div className={className}>
        {craneButtons}
      </div>
    );
  }
}