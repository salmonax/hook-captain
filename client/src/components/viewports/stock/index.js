import React from 'react';
import box from 'fixtures/box';
import { viewport } from 'services';
import { inject, observer } from 'mobx-react';

@inject('common') @observer
export default class StocksViewport extends React.Component {
  constructor(props) {
    super(props);
    this.viewport = null;
    this.state = {
      width: 300,
      height: 300,
    };
  }
  componentDidMount() {
    // possibily refactor viewport mount as callback with computed dimensions signature
    window.viewport = this.props.common.stocksViewport = this.viewport = viewport.create('#view');
    this.camera = this.viewport._renderer._cameras.getCamera();
    const { common } = this.props;
    // If the massing should be rendered on mount, show the spinner instead of the box
    if (!common.isMassingReady) {
      this.viewport.setGeometryEntity(box);
    } else {
      common.elementSpinner = true;
      common.showElementProgress = true;
      this._deferMassing = true;
    }
    console.log(this.viewport._latestSceneResults);
    this._initViewportEvents();
    this._updateDimensions();

    window.addEventListener('resize', this._updateDimensions);
    document.addEventListener('flux-zoom', this.showSimpleMassing);
    document.addEventListener('flux-zoom-end', this.showFullMassing);
    document.addEventListener('mouseup', this.onMouseUp);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
    document.removeEventListener('flux-zoom', this.showSimpleMassing);
    document.removeEventListener('flux-zoom-end', this.showFullMassing);
    document.removeEventListener('mouseup', this.onMouseUp);

  }
  // Note: would rather this be in viewport service wrapper itself, but it'll do the job for now
  _initViewportEvents() {
    // Note: brittle and not coded to interface
    console.log("Here's where initViewportEvents gets called FFS");
    const { _domParent, _cameras, _scene } = this.viewport._renderer;
    const node = _domParent;
    const camera = _cameras.getCamera();
    const raycaster = new THREE.Raycaster();
    const mouse = new THREE.Vector2();

    // Only take mouse down on node, but detect mouse up everwhere: 
    node.addEventListener('mousedown', this.onMouseDown);

  }

  showSimpleMassing = () => {
    // console.log('should show simplified: ', this.massing)
    if (!this.massing || this.isShowingSimple) return;
    this.isShowingFull = false;
    this.isShowingSimple = true;
    const childLength = this.massing.children.length;
    this.massing.children.forEach((line, i) => { if (i % 4) line.visible = false });
    this.massing.children.slice(0, childLength-1).forEach((line, i) => { line.visible = false });
    this.viewport.render()

  };
  showFullMassing = () => {
    // console.log('should show full: ', this.massing)
    if (!this.massing || this.isShowingFull || this._isMouseDown) return;
    this.isShowingFull = true;
    this.isShowingSimple = false;
    this.massing.children.forEach(line => line.visible = true);
    this.viewport.render();
  }
  onMouseDown = (e) => {
    this._isMouseDown = true;
    this.showSimpleMassing();
    e.preventDefault();
  }
  onMouseUp = (e) => {
    this._isMouseDown = false;
    this.showFullMassing();
  }


  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    this.setState({ width, height });
    this.viewport.setSize(width, height);
    // this.viewport.render();
  }

  _renderMassing() {
    const { common } = this.props;
    this.viewport.setGeometryEntity(common.massing.value)
    .then(({_object}) => {
      console.log('----> we at least tried to load the geometry');
      this.massing = _object;
      this.camera.zoom = 2;
      // this.camera.position.z -= 1;
      const lights = this.viewport._renderer._scene.children[0];
      const childLength = this.massing.children.length;
      this.massing.children.forEach(line => {
        const { material } = line;
        material.transparent = true;
        material.color.b = 1.2;
        material.opacity = 0.35;
      });
      // "{"x":1598.8935084792251,"y":1207.5152038665362,"z":444.17606327341014}"
      // "{"x":1805.257305232358,"y":766.8570834190886,"z":268.3007161216896}"
      this.viewport._renderer.focus(_object);
      this.camera.position.x = 1805.25;
      this.camera.position.y = 766.86;
      this.camera.position.z = 268.30;
      this.camera.rotation.x = -1.1910490149881174;
      this.camera.rotation.y = -0.4102639896049163;
      this.camera.rotation.z = -2.9837277910578206;
      this.camera.updateProjectionMatrix();
      this.massing.visible = true;
      this.viewport.render();

      // this.camera.position = new THREE.Vector3(1948.176869480373,910.5052074570307,293.951309839901);
      window.lights = lights;
      window.massing = _object;
      window.camera = this.camera;
      console.log('zoomed to: ', this.camera.zoom);
      common.showElementProgress = false;
      common.elementSpinner = false;
      common.elementProgress = 0;
    });
  }

  render() {
    const { height, width } = this.state;
    const { common } = this.props;
    // console.log('triggering these just in case, isMassingLoaded, massing, isDataLoaded: ', !!common.isMassingLoaded, !!common.massing, !!common.isDataLoaded);
    if (common.isMassingReady && !this.massing && this.viewport) {
      // Defer render of massing if it was ready to go on mount
      // This will allow the page to display and the progressbar to render before FluxViewport freezes the browser
      // It would eventually need to be replaced with a complete overhaul of the way the viewport is loaded
      this._deferMassing ?
        setTimeout(() => this._renderMassing(), 0) : 
        this._renderMassing();
    }
    return (
      <div 
        id='view' 
        style={{
          display: 'flex',
          justifyContent: 'center',
          width: `${width}px`,
          height: `${height}px`,
          zIndex: -1
        }}
        ref={(ref) => this.container = ref ? ref.parentNode : null }
      />);
  }
}
