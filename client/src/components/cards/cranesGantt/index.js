import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx';
import CraneBar from 'components/widgets/CraneBar'  ;


@inject('common') @observer
export default class CranesGantt extends Component {
  
  render() {
    const { currentScenario } = this.props.common;
    if (!currentScenario.length) return (<div style={{width: '100%', textAlign: 'center', paddingTop: '10px'}}> No cranes yet. </div>);
    return (
      <div style={{marginTop: '10px', marginRight: '10px'}}>
        {currentScenario.map((crane, i) => {
          // WARNING: only destructuring until scene is excized from the crane object
          const { count, id, start, end, color, uuid } = crane;
          crane = { count, id, start, end, color, uuid };
          return (<div key={`craneGantt-${uuid}`}>
            <CraneBar crane={crane} scenario={this.props.scenario} />
          </div>);

        })}
      </div>
    );
  }
}
    
