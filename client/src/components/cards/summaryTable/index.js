import React, { Component } from 'react';
import { Table } from 'flux-ui';
import './index.scss';
import { toJS } from 'mobx';


// Just for mock data; quikcy hashCode function
function kludgeHash() { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

export default class SummaryTable extends Component {
  constructor(props) {
    super(props);
  }

  // Makes G2-formatted columns straight from data
  makeCols(datum) {
    if (!datum) return [];
    return Object.keys(datum)
    .filter(key => key !== 'key')
    .map(key => ({
      title: key.toUpperCase(),
      dataIndex: key,
      sorter: (a, b) => parseFloat(a[key]) === NaN ? parseFloat(b[key]) - parseFloat(a[key]) : b[key] === a[key] ? 0 : b[key] < a[key] ? -1 : 1,
    }));
  }

  addKeys(data) {
    return data.map(datum => {
      return Object.assign({},datum, {
        key: kludgeHash(JSON.stringify(datum)),
      });
    });
  }

  tablify(scenario) {
    return scenario.map(({ count, id, startDate, endDate, utilization }) => ({
      key: kludgeHash(),
      '#': count,
      type: id,
      'start date': startDate || '12/12/1999',
      'end date': endDate || '12/30/1999',
      utilization: utilization || Math.random().toFixed(2),
    }))
  }


  render() {
    // const empty = [{
    //   key: ' ',
    //   '#': ' ',
    //   type: ' ',
    //   'start date': ' ',
    //   'end date': ' ',
    //   utilization: ' ',
    // }];
    // let { scenario } = this.props;
    // const data = (scenario && scenario.length) ? this.tablify(scenario) : empty;
    // const columns = this.makeCols(data[0]);
    if (!this.props.data.length) return (<div className='empty-summary-table'>No data loaded.</div>)

    const data = this.addKeys(this.props.data);
    const columns = this.makeCols(data[0]);

    return (

      <Table columns={columns} dataSource={data} pagination={false} />
    );
  }

}