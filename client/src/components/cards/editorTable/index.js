import React, { Component } from 'react';
import { Table } from 'flux-ui';
import './index.scss';
import { toJS } from 'mobx';
import { observer, inject } from 'mobx-react';

// Just for mock data; quikcy hashCode function
function kludgeHash() { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

@inject('common') @observer
class SummaryTable extends Component {
  constructor(props) {
    super(props);
  }

  // Makes G2-formatted columns straight from data
  makeCols(datum) {
    if (!datum) return [];
    return Object.keys(datum)
    .filter(key => key !== 'key')
    .map(key => ({
      title: key.toUpperCase(),
      dataIndex: key,
      sorter: (a, b) => parseFloat(a[key]) === NaN ? parseFloat(b[key]) - parseFloat(a[key]) : b[key] === a[key] ? 0 : b[key] < a[key] ? -1 : 1,
    }));
  }

  addKeys(data) {
    return data.map(datum => {
      return Object.assign({},datum, {
        key: kludgeHash(JSON.stringify(datum)),
      });
    });
  }

  tablify(scenario) {
    return scenario.map(({ count, id, start, end, activity }, i) => ({
      key: kludgeHash(),
      '#': count,
      type: id,
      'start date': start || '12/12/1999',
      'end date': end || '12/30/1999',
      activity: activity !== undefined ? (activity*100).toFixed(1) + '%' : 0,
    }))
  }


  render() {
    const empty = [{
      key: ' ',
      '#': ' ',
      type: ' ',
      'start date': ' ',
      'end date': ' ',
      activity: ' ',
    }];
    let { scenario } = this.props;
    const { algoScenario } = this.props.common;
    const data = algoScenario.length ? this.tablify(algoScenario) : empty;
    const columns = this.makeCols(data[0]);

    return (
      <Table style={{paddingLeft: '13px' }} columns={columns} dataSource={data} pagination={false} />
    );
  }

}

export default SummaryTable