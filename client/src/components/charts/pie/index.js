import createG2 from 'g2-react';
import { Stat } from 'g2';
import React, { Component } from 'react';
// import data from 'fixtures/g2-pie-example.json';
import { observer, inject } from 'mobx-react';

class HigherChart extends Component {
  constructor(props) {
    super(props);
    this.Chart = createG2(chart => {
      this.chart = chart;
      chart.coord('theta');
      chart.intervalStack().position(Stat.summary.proportion()).color('clarity');
      chart.render();
    });
  }
  render() {
    return (<this.Chart {...this.props} />);
  }
}

@inject('common') @observer
class PieChart extends Component {
  state = {
    data: data.slice(0, data.length / 2 - 1),
    width: 500,
    height: 250,
    plotCfg: {
      margin: [10, 100, 50, 60],
    },
  };

  changeHandler() {
    const { chart } = this.refs.myChart;
    chart.clear();
    chart.intervalStack().position(Stat.summary.proportion()).color('price');      // operation
    chart.render();
  }

  // Note: refactor all of this:
  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    this.setState({ width, height });
  }

  componentDidMount() {
    window.addEventListener('resize',this._updateDimensions);
    this._updateDimensions();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  }

  render() {
    const { height, width } = this.state;

    const { abs, sin } = Math;
    const store = this.props.common;
    // No idea why this doesn't update, except that 
    const data = this.state.data.map((datum,i) => {
      return Object.assign({}, datum, { 
        carat: Math.random(),
        depth: Math.random(), 
        price: Math.random(),
        x: Math.random(),
        y: Math.random(),
        z: Math.random(),
        table: Math.random(),
        name: Math.random(),
      });
    });

    return (<div ref={(ref) => this.container = ref ? ref.parentNode : null }>
      { height && width ?
          <HigherChart
            shape={this.state.shape}
            data={data}
            width={this.state.width}
            height={this.state.height}
            plotCfg={this.state.plotCfg}
        /> : null
      }
        {/*<button onClick={this.changeHandler.bind(this)}>change</button>*/}
      </div>
    );
  }
}

export default PieChart;