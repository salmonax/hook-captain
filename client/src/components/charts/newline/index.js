import createG2 from 'g2-react';
import { Stat } from 'g2';
import React, { Component } from 'react';
import data from 'fixtures/g2-line-example.json';
import { observer, inject } from 'mobx-react';

// import algoOutput from 'fixtures/algo.json';

const LABELS = {
  'Scenario A': 'Scenario A',
  'Scenario B': 'Scenario B',
  'Scenario C': 'Scenario C',
};

// const aColor = "#ff9900";
// const bColor = "#ff0099";
// const cColor = "#0000ff";

// const aColor = '#7DFBCF';
// const bColor = '#19C388';
// const cColor = '#0D835A';

const COLORS = {
  Value: {
    aColor: '#7DFBCF', 
    bColor: '#19C388',
    cColor: '#0D835A',
  },


  Speed: {
    aColor: '#C4D6FF',
    bColor: '#497DF9',
    cColor: '#214AA6',
  },

  Agility: {
    aColor: '#FDA3D2',
    bColor: '#F758AB',
    cColor: '#B6186A',
  },
};

const formatterMap = {
  Value: d => '£ '+(d/1000000).toFixed() + 'M',
  Speed: d => d + ' days',
  Agility: d => (d*100).toFixed() + '%', 
};

// const dummyScenarios = {
//   'Scenario A': algoOutput,
//   'Scenario B': algoOutput,
//   'Scenario C': algoOutput,
// };

const SCENARIOS = ['Scenario A', 'Scenario B', 'Scenario C'];
const starterData = SCENARIOS.map(label => ({
  week: 0,
  scenario: label,
  value: 0,
}))


// var Stat = G2.Stat;
// var chart = new G2.Chart({
//   id: 'c1',
//   forceFit: true,
//   height: 450
// });
// chart.source(data, {
//   year: {
//     type: 'linear',
//     tickInterval: 25
//   }
// });
// chart.areaStack().position('year*value').color('country');
// chart.render();

// Note: not sure this pattern is really necessary, but leaving it for now
class HigherChart extends Component {
  constructor(props) {
    super(props);
    this.Chart = createG2(chart => {
      this.chart = chart;
      chart.source(props.data, {
        week: {
          type: 'linear',
          tickInterval: 10,
        }
      });
      const { aColor, bColor, cColor } = props.colors;
      chart.line().position('week*value').color('scenario', [aColor, bColor, cColor]).size(3);
      chart.axis('week', { 
        title: null,
        labels: {
          label: {
            fontSize: 10,
            fill: 'gray',
          }
        }
      });
      chart.axis('value', { 
        title: null,
        formatter: props.formatter,
        // formatter: d => '£ '+(d/1000000).toFixed(1) + 'M',
        // labels: {
        //   label: {
        //     fontSize: 10,
        //     fill: 'gray'  
        //   }
          
        // }
      });
      chart.col('week',{
        min: 0,
        max: 53,
        ticks: [0, 10, 20, 30, 40, 50]
      })
      if (this.props.width < 325) {
        this.chart.legend(false);
      } else {
        chart.legend({
          dy: -5,
          // dx: -5,
          position: 'top',
          title: null,
          marker: 'square',
          word: {
            fill: 'gray',
            fontSize: 10,
          }
        });
      }
      chart.render();
    });
  }

  componentWillReceiveProps(nextProps) {
    // NOTE: cares about props.formatter; should pass indicator as props and check for that
    if (nextProps.colors !== this.props.colors) {
      this.chart.clear();
      // console.warn('WEWEWEW',nextProps.colors);
      const { aColor, bColor, cColor } = nextProps.colors;
      // console.warn('wewieoiwe', aColor, bColor, cColor );
      // console.warn(this.props.data);
      this.chart.line().position('week*value').color('scenario', [aColor, bColor, cColor]).size(3);
      // this.chart.line().position('week*value').size(2);
      this.chart.axis('value', {
        title: null,
        formatter: nextProps.formatter,
      });
      if (nextProps.data === starterData) this.chart.render();
    }
    if (nextProps.width < 325) {
      this.chart.legend(false);
    } else if (this.props.width < 325) {
      this.chart.legend({
        dy: -5,
        // dx: -5,
        position: 'top', 
        title: null,
        marker: 'square',
        word: {
          fill: 'gray',
          fontSize: 10,
        }
      });
    }
  }

  render() {
    return (<this.Chart {...this.props} />);
  }
}

@inject('common') @observer
class StackedAreaChart extends Component {
  state = {
    shape: 'spline',
    width: 0,
    height: 0,
    plotCfg: {
      margin: [40, 60, 50, 70],
    },
  }
  changeHandler = () => {
    this.setState({
      shape: 'line',
    });
  }

  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    this.setState({ width, height });
  }

  componentDidMount() {
    window.addEventListener('resize',this._updateDimensions);
    this._updateDimensions();

    // setInterval(() => {
    //   const { round, random } = Math;
    //   this.setState({
    //     data: stackedData.map(n => Object.assign({}, n, { value: round(random()*5000) }) )
    //   });
    // },200);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  }

  extractCost(weeklyDP) {
    // console.log('extract cost: ', weeklyDP)
    const costArray = [];
    if (weeklyDP) {
      for (let i = 1; i <= 54; i++) {
        const costObj = weeklyDP[`Week ${i}`].cumCost;
        for (let costType in costObj) {
          if (costType !== 'addCost') {
            // NOTE: preventing values from going below zero
            costArray.push({ week: i-1, type: LABELS[costType], cost: Math.max(0,costObj[costType]) });
          }
        }
      }
    }
    return costArray;
  }

  extractComparison(scenarioMap, key) {
    const output = [];
    SCENARIOS.forEach(scenario => {
      if (!scenarioMap[scenario]) { 
        output.push({
          scenario,
          week: 0,
          value: 0,
        });
        return;
      }

      const weeklyDP = scenarioMap[scenario].get ? scenarioMap[scenario].get('weeklyDP') : scenarioMap[scenario]['weeklyDP'];

      for (let i = 1; i <= 54; i++) {
        const dataPoint = { 
          scenario,
          week: i-1,
          value: weeklyDP[`Week ${i}`][key],
        };
        output.push(dataPoint);
      }
    });
    return output;
  }

  render() {
    const { height, width } = this.state;
    const { indicator } = this.props;

    const { common } = this.props;
    // const weeklyDP = common.algoMap.get('weeklyDP');
    // console.log('weeklyDP:', weeklyDP);



    let costData = this.extractComparison(common.algoMapByScenario, this.props.chartKey);

    // console.log('???VCVDSDFSDF',JSON.stringify(costData, null, 2));
    // console.log('start data', JSON.stringify(starterData))
    // console.log('wewew',this.props.chartKey);

    return (<div ref={(ref) => this.container = ref ? ref.parentNode : null }>
      { height && width ?
          <HigherChart
            formatter={formatterMap[indicator]}
            colors={COLORS[indicator]}
            shape={this.state.shape}
            data={costData.length ? costData : starterData}
            width={this.state.width}
            height={this.state.height}
            plotCfg={this.state.plotCfg}
        /> : null
      }
      {/*<button onClick={this.changeHandler}>Change shape</button>*/}
    </div>);
  }
}

export default StackedAreaChart;