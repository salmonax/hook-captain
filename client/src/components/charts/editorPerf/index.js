import React from 'react';
import _Bar from 'charts/_bar';
import { observer, inject } from 'mobx-react';

const defaultData = [
  {
    label: 'Site Coverage',
    value: 0,
  },
  {
    label: 'Crane Activity',
    value: 0,
  },
  {
    label: 'Crane Utilisation',
    value: 0,
  },
];

// Ugh, hopefully I don't regret dumping these styles in here in a rush later..
// Note: you will

export default inject('common')(observer(Perf));

function Bar(props) {
  const { label, value } = props;

  return (
    <div style={{marginBottom: '15px'}}>
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
       <div className="subtitle" style={{fontWeight: 'bold', fontSize: '13px', marginBottom: '8px' }}>{label}</div>
       <div style={{fontSize: '13px', fontWeight: 'bold' }}>{value}%</div>
      </div>
      <_Bar value={value} />
    </div>
  );
}

function Perf(props) {  
  const scenarioMetrics = props.common.algoMap.get('scenarioMetrics');
  console.log('from perf: ', scenarioMetrics);
  const data = scenarioMetrics ?
    ['Site Coverage', 'Crane Activity', 'Crane Utilization'].map(label => {
      return {
        label: label === 'Crane Utilization' ? 'Crane Utilisation' : label,
        value: scenarioMetrics[label] || 0,
      }; 
    }) :
    defaultData;

  return (
    <div style={{padding: '3px 15px'}}>
      <div className="subtitle" style={{fontWeight: 'bold', fontSize: '13px', marginBottom: '15px'}}>Current estimated performance of Scenario B:</div>
      {data.map(datum => <Bar key={datum.label} {...datum} />)}
      {scenarioMetrics && scenarioMetrics['Number of days to first utilization'] ? 
        <div style={{width: '100%', display: 'flex', flexDirection: 'row', fontSize: '12px', paddingTop: '4px', fontWeight: 'bold', justifyContent: 'flex-end'}}>
          <div className="subtitle" >
            Wasted days:
          </div>
          <div style={{marginLeft: '6px', marginRight: '3px', textAlign: 'right'}}>
            {scenarioMetrics['Number of days to first utilization']}
          </div>
        </div> : null
      }
    </div>
  );

}