import React from 'react';
import _Bar from 'charts/_bar';

import { inject, observer } from 'mobx-react';

// import valueIcon from 'valueIcon-placeholder-cool.png';
// import speedIcon from 'speedIcon-placeholder.png';
// import agilityIcon from 'agilityIcon-placeholder-cool.png';
const valueIcon = 'assets/value.svg';
const speedIcon = 'assets/time.svg';
const agilityIcon = 'assets/agility.svg';

// Doubled up with variables, maybe import them later
// Also, this inlining of icons is really terrible; serve them and URL them instead
const defaultData = [
  {
    label: 'Value',
    quantity: '',
    value: 0,
    delta: 0,
    fill: '#19C388',
    // fill: '#33c7c1',
    // rear: '#d8f2f1',
    icon: valueIcon,
  },
  {
    label: 'Speed',
    quantity: '',
    value: 0,
    delta: 0,
    fill: '#709AFE',
    // fill: '#709afe',
    // rear: '#e2ebff',
    icon: speedIcon,
  },
  {
    label: 'Agility',
    quantity: '',
    value: 0,
    delta: 0,
    fill: '#E82089',
    // fill: '#de56c6',
    // rear: '#f8def4',
    icon: agilityIcon,
  },
];

let lastData = null;

export default inject('common')(observer(KPI));

function Bar(props) {
  const { label, value, delta, fill, rear, icon, quantity } = props;
  // Refactor styles out
  const rowContainer = {
    display: 'flex', 
    flexDirection: 'row', 
    fontSize: '18px', 
    fontWeight: 'bold', 
    alignItems: 'center',
    marginBottom: '4px',
  };
  return (
    <div style={{padding: '6px 15px', paddingBottom: '20px'}}>
      <div className='subtitle' style={{textTransform: 'uppercase', fontWeight: 'bold', fontSize: '12px'}}>{label}</div>
      <div style={rowContainer}>
        <div style={{color: fill}}>{quantity}</div>
        <img src={icon} style={{marginLeft: 'auto', width: '22px', alignSelf: 'center'}}/>  
        <div style={{marginLeft: '5px'}}>{value}</div>
      </div>
      <_Bar value={value} fill={fill} rear={rear} />
      <div>{delta} since last update</div>
    </div>
  );
}

const quantityMap = {
  KPI_Value: {
    quantityKey: 'Project Cost',
    format: d => '£ ' + d.toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
    deltaFormat: (a, b) => {
      const sign = a - b >= 0 ? '+' : '-';
      const absDelta = Math.abs(a-b);
      const figure = absDelta >= 1e3 ? (absDelta/1e3).toFixed() + 'k' : absDelta.toFixed();
      return sign + ' £ ' + figure;
    }
  },
  KPI_Speed: {
    quantityKey: 'Project Time',
    format: d => Math.abs(d).toFixed(1) + ' days',
    deltaFormat: (a, b) => {
      const delta = Math.abs(a) - Math.abs(b);
      const sign = delta >= 0 ? '+' : '-';
      return sign + ' ' + Math.abs(delta).toFixed() + ' days';
    },
  }, 
  KPI_Agility: {
    quantityKey: 'Project Risk',
    format: d => (100 * (1 - d)).toFixed() + '% no wind risk',
    deltaFormat: (a, b) => {
      const delta = (-a + b);
      const sign = delta >= 0 ? '+' : '-';
      return sign + ' ' + (100 * Math.abs(delta)).toFixed() + '%';
    }
  }
};




function KPI(props) {
  const scenarioMetrics = props.common.algoMap.get('scenarioMetrics');
  const data = scenarioMetrics && Object.keys(scenarioMetrics).length ? 
  ['KPI_Value', 'KPI_Speed', 'KPI_Agility'].map((keyName, i) => {
    let newValue = scenarioMetrics[keyName] ? scenarioMetrics[keyName].avg : 0;
    const { quantityKey, format, deltaFormat } = quantityMap[keyName];
    const rawQuantity = scenarioMetrics[quantityKey];
    const quantity = format(rawQuantity);
    console.log('!!!!', quantity, newValue);

    if (newValue === undefined) newValue = 0;
    const lastRawQuantity = lastData && lastData[i].rawQuantity ? lastData[i].rawQuantity : 0;
    return Object.assign({}, defaultData[i], {
      value: newValue,
      delta: deltaFormat(rawQuantity, lastRawQuantity),
      quantity,
      rawQuantity, // used only for delta
    });
  }) :
  defaultData;
  lastData = data;
  return (
    <div>
      {data.map(datum => <Bar key={datum.label} {...datum} />)}
    </div>
  );

}