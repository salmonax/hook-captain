import createG2 from 'g2-react';
import { Stat } from 'g2';
import React, { Component } from 'react';
import data from 'fixtures/g2-line-example.json';
import { observer, inject } from 'mobx-react';

// import algoOutput from 'fixtures/algo.json';

const LABELS = {
  'Scenario A': 'Scenario A',
  'Scenario B': 'Scenario B',
  'Scenario C': 'Scenario C',
};

const aColor = "#ff9900";
const bColor = "#ff0099";
const cColor = "#0000ff";

// const valueColor = "#33c7c1";
// const speedColor = "#709afe";
// const agilityColor = "#de56c6";

const valueColor = "#19C388";
const speedColor = "#709AFE";
const agilityColor = "#E82089";

// const dummyScenarios = {
//   'Scenario A': algoOutput,
//   'Scenario B': algoOutput,
//   'Scenario C': algoOutput,
// };

const SCENARIOS = ['Scenario A', 'Scenario B', 'Scenario C'];
const perfData = SCENARIOS.map(label => ({
  week: 0,
  scenario: label,
  value: 0,
}))

const optimalData = `Value Speed Agility`.split(' ').map(metric => ({
  week: 0,
  scenario: metric, 
  value: 0,
}));



// var Stat = G2.Stat;
// var chart = new G2.Chart({
//   id: 'c1',
//   forceFit: true,
//   height: 450
// });
// chart.source(data, {
//   year: {
//     type: 'linear',
//     tickInterval: 25
//   }
// });
// chart.areaStack().position('year*value').color('country');
// chart.render();

// Note: not sure this pattern is really necessary, but leaving it for now
class HigherChart extends Component {
  constructor(props) {
    super(props);
    this.Chart = createG2(chart => {
      this.chart = chart;
      chart.source(props.data, {
        week: {
          type: 'linear',
          tickInterval: 10,
        }
      });
      chart.line().position('week*value').color('scenario', [valueColor, speedColor, agilityColor]).size(2);
      chart.axis('week', { 
        title: null,
        labels: {
          label: {
            fontSize: 10,
            fill: 'gray',
          }
        }
      });
      chart.axis('value', { 
        title: null,
        // formatter: d => '£ '+(d/1000000).toFixed(1) + 'M',
        // labels: {
        //   label: {
        //     fontSize: 10,
        //     fill: 'gray'  
        //   }
          
        // }
      });
      chart.col('week',{
        min: 0,
        max: 53,
        ticks: [0, 10, 20, 30, 40, 50]
      })
      if (this.props.width < 325) {
        this.chart.legend(false);
      } else {
        chart.legend({
          dy: -5,
          // dx: -5,
          position: 'top', 
          title: null,
          marker: 'square',
          word: {
            fill: 'gray',
            fontSize: 10,
          }
        });
      }
      chart.render();
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.shape !== this.props.shape) {
      this.chart.clear();
      this.chart.line().position('week*cost').color('type');
      // this.chart.render();
    }
    if (nextProps.width < 325) {
      this.chart.legend(false);
    } else if (this.props.width < 325) {
      this.chart.legend({
        dy: -5,
        // dx: -5,
        position: 'top', 
        title: null,
        marker: 'square',
        word: {
          fill: 'gray',
          fontSize: 10,
        }
      });
    }
  }

  render() {
    return (<this.Chart {...this.props} />);
  }
}

@inject('common') @observer
class StackedAreaChart extends Component {
  state = {
    shape: 'spline',
    width: 0,
    height: 0,
    plotCfg: {
      margin: [40, 50, 30, 50],
    },
  }
  changeHandler = () => {
    this.setState({
      shape: 'line',
    });
  }

  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    this.setState({ width, height });
  }

  componentDidMount() {
    window.addEventListener('resize',this._updateDimensions);
    this._updateDimensions();

    // setInterval(() => {
    //   const { round, random } = Math;
    //   this.setState({
    //     data: stackedData.map(n => Object.assign({}, n, { value: round(random()*5000) }) )
    //   });
    // },200);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  }

  extractKPIs(weeklyDP) {
    // console.log('extract cost: ', weeklyDP)
    const LABELS = {
      'cumKPI_cost': 'Value',
      'cumKPI_time': 'Speed',
      'cumKPI_risk': 'Agility',
    };

    const kpiArray = [];
    if (weeklyDP) {
      for (let i = 1; i <= 54; i++) {
        const weekData = weeklyDP[`Week ${i}`];
        `cumKPI_cost cumKPI_time cumKPI_risk`.split(' ').forEach(kpi => {
          kpiArray.push({ scenario: LABELS[kpi], value: weekData[kpi], week: i-1 });
        });
      }
    }
    return kpiArray;
  }

  extractComparison(scenarioMap, key) {
    const output = [];
    SCENARIOS.forEach(scenario => {
      if (!scenarioMap[scenario]) return;
      const weeklyDP = scenarioMap[scenario].get ? scenarioMap[scenario].get('weeklyDP') : scenarioMap[scenario]['weeklyDP'];

      for (let i = 1; i <= 54; i++) {
        const dataPoint = { 
          scenario,
          week: i-1,
          value: weeklyDP[`Week ${i}`][key],
        };
        output.push(dataPoint);
      }
    });
    // console.log('OUTPOUT PTUTPUTsfasdoifuasdofiuas', output);
    return output;
  }

  render() {
    const { height, width } = this.state;

    const { common } = this.props;
    // const weeklyDP = common.algoMap.get('weeklyDP');
    // console.log('weeklyDP:', weeklyDP);

    const starterData = this.props.scenarioName ? optimalData : perfData;
    const storeData = common.algoMapByScenario[this.props.scenarioName];
    console.log('!!!!!!!!', storeData, common.algoMapByScenario, this.props.scenarioName);

    const costData = storeData ? this.extractKPIs(storeData.get('weeklyDP')) : starterData;

    // console.log('????????????????????', costData)
    // console.log('???VCVDSDFSDF',JSON.stringify(costData, null, 2));
    // console.log('start data', JSON.stringify(starterData))
    // console.log('wewew',this.props.chartKey);

  

    return (<div ref={(ref) => this.container = ref ? ref.parentNode : null }>
      { height && width ?
          <HigherChart
            shape={this.state.shape}
            data={costData.length ? costData : starterData}
            width={this.state.width}
            height={this.state.height}
            plotCfg={this.state.plotCfg}
        /> : null
      }
      {/*<button onClick={this.changeHandler}>Change shape</button>*/}
    </div>);
  }
}

export default StackedAreaChart;