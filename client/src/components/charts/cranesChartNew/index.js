import createG2 from 'g2-react';
import { Stat } from 'g2';
import React, { Component } from 'react';
// import data from '../../../fixtures/g2-pie-example.json';
import { observer, inject } from 'mobx-react';
import { toJS } from 'mobx';

import cranesGraph from './cranes';

const dummyData = [
  {
    "count": 2,
    "color": "#ff9900",
    "type": "MB001"
  },
  {
    "count": 1,
    "color": "#ff0099",
    "type": "TC001"
  },
  {
    "count": 3,
    "color": "#0000ff",
    "type": "TC003"
  },
];

class D3Wrapper extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { data, width, height } = this.props;
    console.log('ON MOUNT:', width, height);
    cranesGraph.init(data, width, height);
  }


  componentWillReceiveProps(nextProps) {
    // console.log(nextProps);
    const { data, width, height } = nextProps;
    if (this.props.data !== nextProps.data) {
      cranesGraph.update(data, width, height);
    } 
    if (this.props.width !== nextProps.width || this.props.height !== nextProps.height) {
      cranesGraph.resize(data, width, height);
    }

  }

  render() {
    const { data } = this.props;
    const totalCount = data.reduce((total, d) => total += d.count, 0);
    const numTypes = data.length-1; // subtract one for the null element
    const percentageEl = count => (<div className="subtitle" style={{marginLeft: '8px'}}>{Math.round(count/totalCount*100)+'%'}</div>);
    const typeEl = type => (<div>{type}</div>)

    return (
      <div>
        <div id="cranes-chart" style={{marginTop: '8px'}}/>
        <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap'}} id='crane-percentages'> 
          {data.map((datum, i) => {
            if (datum.count) {
              const { type, color, count } = datum;
              return (
                <div key={`crane-chart-${i}`} style={{margin: '0px 8px', fontSize: '14px', fontWeight: 'bold'}}>
                  <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                    <div style={{borderRadius: '10px', height: '6px', width: '6px', background: color, marginRight: '5px' }}/>
                    {numTypes <= 3 && this.props.width > 280 ? typeEl(type) : percentageEl(count) }
                  </div>
                  {numTypes <= 3 && this.props.width > 280 ? percentageEl(count) : null}
                </div>
              );
            }
          })}
        </div>
      </div>
    );
  }

}

@inject('common') @observer
class CranesChart extends Component {
  state = {
    width: 0,
    height: 0,
  };

  changeHandler() {
    const { chart } = this.refs.myChart;
    chart.clear();
    chart.intervalStack().position(Stat.summary.proportion()).color('price');      // operation
    chart.render();
  }

  // Note: refactor all of this:
  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = (offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom))*0.93;
    console.log('ON UPDATEDIMENSIONS', width, height);
    this.setState({ width, height });
  }

  componentDidMount() {
    window.addEventListener('resize',this._updateDimensions);
    this._updateDimensions();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  }

  mapData(scenario) {
    const tally = {};
    if (!scenario) return [];
    scenario.forEach(crane => {
      const { id, color } = crane;
      if (!tally[id]) tally[id] = { count: 0, color }
      tally[id].count++;
    });
    return Object.keys(tally).map(type => Object.assign(tally[type], { type }) )
  }

  render() {
    const { height, width } = this.state;

    const { abs, sin } = Math;
    const store = this.props.common;

    const { scenarios, currentScenario } = store;
    const { scenario } = this.props;

    const cranes = scenarios[scenario];
    
    const chartData = this.mapData(currentScenario);

    return (<div id="cranes-chart-container" ref={(ref) => this.container = ref ? ref.parentNode : null }>
      { height && width ?
          <D3Wrapper
            width={this.state.width}
            height={this.state.height*.7}
            data={chartData}
        /> : null
      }
        {/*<button onClick={this.changeHandler.bind(this)}>change</button>*/}
      </div>
    );
  }
}

export default CranesChart;


