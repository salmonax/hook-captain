import d3 from 'd3';


const dummyData = [
  {
    "count": 2,
    "color": "#ff9900",
    "type": "MB001"
  },
  {
    "count": 1,
    "color": "#ff0099",
    "type": "TC001"
  },
  {
    "count": 3,
    "color": "#0000ff",
    "type": "TC003"
  },
];

const dummyData2 = [
  {
    "count": 2,
    "color": "#ff9900",
    "type": "MB001"
  },
  {
    "count": 1,
    "color": "#ff0099",
    "type": "TC001"
  },
  {
    "count": 3,
    "color": "#00ff00",
    "type": "TC005"
  },
  {
    "count": 3,
    "color": "#0000ff",
    "type": "TC003"
  },
];

const dummyData3 = [
  {
    "count": 5,
    "color": "#ff9900",
    "type": "CB001"
  },
  {
    "count": 1,
    "color": "#ff0099",
    "type": "TC001"
  },
  {
    "count": 3,
    "color": "#00ff00",
    "type": "TC005"
  },
  {
    "count": 3,
    "color": "#0000ff",
    "type": "TC003"
  },
];



var svg;

// var color = d3.scale.category10(), svg;

const key = d => d.data ? d.data.type : null;

const colorMap = (data) => {
  const output = {};
  data.forEach(item => {
    output[item.type] = item.color;
  })
  return output;
}

function arcTween(d) {
  var i = d3.interpolate(this._current, d);
  this._current = i(0);
  return function(t) { return arc(i(t)); };
}

var pie = d3.layout.pie()
      .value(d => d.count)
        .sort(null);

var arc = d3.svg.arc();
var inner = d3.svg.arc();

var circleG;

var radius

function init(data, width, height) {
    radius = Math.min(width, height) / 2;
    console.log('???', width, height, radius);
    svg = d3.select("#cranes-chart").append("svg")
          .attr("width", width)
          .attr("height", height)
          .append("g")
          .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    arc
      .innerRadius(radius - radius*.11)
      .outerRadius(radius - radius*.18);

    inner
      .outerRadius(radius - radius*.18)
      .innerRadius(radius - radius*.32);

    var g2 = svg.selectAll(".inner-path")
      .data(pie([{count: 1}]))
      .enter().append("g")
      .style("fill", "#eee")

    circleG = svg.append('g');

    circleG.append('circle')
        .attr('cx', 0)
        .attr('cy', 0)
        .attr('r', radius - radius*.34)
        .attr('fill', 'none')
    circleG.append('text')
        .text('0')
        .attr('class', 'crane-label')
        .attr('x', 0)
        .attr('y', 0)
        .attr('text-anchor', 'middle')
        .attr('fill', 'black')
        
      
    g2.append("path")
      .attr("class", "inner-path")
      .attr("d", inner)
      .style("fill", "#eee")
    update(data, width, height);
}

function resize(data, width, height) {
  d3.select("#cranes-chart").selectAll("*").remove();
  init(data, width, height);
  update(data, width, height);
}

function update(data, width, height) {
  var lastData = svg.selectAll(".outer-path").data();


  const count = data.reduce((acc, n) => acc += n.count, 0);
  svg.selectAll('.crane-label').text(count)
    .attr('text-anchor', 'middle')
    .attr('transform', 'translate(0,'+radius/4.1+')')
    .attr('font-size', radius/1.5)
    .attr('fill', '#777');

  if (!data.length) {
    data = [{
    "count": 1,
    "color": "#aaa",
    "type": "_none"
  }]} else if (data[0].type !== '_none') {
    data.unshift({
      "count": 0,
      "color": '#aaa',
      "type": '_none'
    })
  }

  var currentData = pie(data);



  const colors = colorMap(data);
  const color = (key) => colors[key];

  var path = svg.selectAll(".outer-path").data(currentData, key);
          
  path.exit()
    .datum(function (d, i) {
      return findNeighborArc(i, currentData, lastData, key) || d;
    })
    .transition()
      .duration(500)
      .attrTween("d", arcTween)
      .remove();

  path.enter().append("path")
      .attr("class", "outer-path")
      .attr("d", arc)
      .each(function(d, i) { 
        this._current = findNeighborArc(i, lastData, currentData, key) || d;
      })
      .attr("fill", d => { 
        return color(d.data.type);
      });

  path.transition()
        .duration(500)
        .attrTween("d", arcTween);
}  


function findNeighborArc(i, data0, data1, key) {
  var d;
  return (d = findPreceding(i, data0, data1, key)) ? {startAngle: d.endAngle, endAngle: d.endAngle}
      : (d = findFollowing(i, data0, data1, key)) ? {startAngle: d.startAngle, endAngle: d.startAngle}
      : null;
}

// Find the element in data0 that joins the highest preceding element in data1.
function findPreceding(i, data0, data1, key) {
  var m = data0.length;
  while (--i >= 0) {
    var k = key(data1[i]);
    for (var j = 0; j < m; ++j) {
      if (key(data0[j]) === k) return data0[j];
    }
  }
}

// Find the element in data0 that joins the lowest following element in data1.
function findFollowing(i, data0, data1, key) {
  var n = data1.length, m = data0.length;
  while (++i < n) {
    var k = key(data1[i]);
    for (var j = 0; j < m; ++j) {
      if (key(data0[j]) === k) return data0[j];
    }
  }
}

export default { init, update, resize }