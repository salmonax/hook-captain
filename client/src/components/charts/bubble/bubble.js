import d3 from 'd3';

var dummyCranes = [
  {
    name: 'Scenario A',
    halo: .8,
    height: 0.6,
    color: "#ff9900",
  },
  {
    name: 'Scenario B',
    halo: 0.6,
    height: 0.2,
    color: "#ff0099",
  },
  {
    name: 'Scenario C',
    halo: 0.3,
    height: 0.4,
    color: "#0000ff",
  }
];

const haloMax = 35;
// Take out
const colorMap = (data) => {
  const output = {};
  data.forEach(item => {
    output[item.type] = item.color;
  })
  return output;
}

var margin = {top: 30, right: 50, bottom: 30, left: 50};

const gridLines = '#d8d8d8';

var x, y, xAxis, yAxis, svg;
var r1, r2;

var handlePercent = 0.2;


function init(data, width, height) {
  width = width - margin.left - margin.right;
  height = height - margin.top - margin.bottom;
  // data = dummyCranes;

  // Adds the svg canvas
  svg = d3.select("#bubble-chart")
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");

  x = d3.scale.ordinal()
      .rangeRoundBands([0, width]);

  // window.x = x

  y = d3.scale.linear()
      .range([height, 0]);

  xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .outerTickSize(1)
      .innerTickSize(-height)
      .tickPadding(10);

  yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(5)
    .innerTickSize(-width)
    .outerTickSize(1)
    .tickPadding(10)
    .tickFormat(d => {
      return d > 0 ? d.toFixed() : ''
    });
  
  x.domain(data.map(function(d) { return d.name; }));
  y.domain([0, 100]);

  r1 = d3.scale.linear()
          .range([0, x.rangeBand()/2 ]); // radius for the handle

  r2 = d3.scale.linear()
           .range([r1(handlePercent), x.rangeBand()/2]); // radius for quantities



  

  const colors = colorMap(data);
  const color = key => colors[key];

  // if (!isDrawn) {
  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
        .selectAll('line')
          .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)'); 

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

  // }
  const enterSelection = svg.selectAll(".circle").data(data).enter();
  // Not right
  enterSelection.append("circle")
        .attr("class", "halo")
        .attr("r", d=> d.halo && d.height ? r2(d.halo/haloMax) : 0)
        .attr('fill', d => d.color)
        .attr("cx", function(d) { return x(d.name); })
        .attr("cy", function(d) { return y(d.height); })
        .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)')
        .attr("opacity", 0.4)
  enterSelection.append("circle")
        .attr("class", "handle-outline")
        .attr("r", d => d.halo && d.height ? r1(handlePercent) : 0)
        .attr('fill', d => '#fff')
        .attr("cx", function(d) { return x(d.name); })
        .attr("cy", function(d) { return y(d.height); })
        .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)');
  enterSelection.append("circle")
        .attr("class", "handle")
        .attr("r", d => d.halo && d.height ? r1(handlePercent*0.75) : 0)
        .attr('fill', d => d.color)
        .attr("cx", function(d) { return x(d.name); })
        .attr("cy", function(d) { return y(d.height); })
        .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)');


  svg.selectAll('text')
    .style('fill', 'gray');

  // Do other stuff
  // update(data, width, height);

  // setInterval(() => {
  //   const newData = data.map(item => Object.assign({}, item, { halo: Math.random(), height: Math.random() }))
  //   // console.log(JSON.stringify(newData, null, 2));
  //   update(newData, width, height);

  // },500);
}


function update(data, width, height) {
  // width = width - margin.left - margin.right;
  // height = height - margin.top - margin.bottom;
  x.domain(data.map(function(d) { return d.name; }));
  y.domain([0, 100]);

  const halo = svg.selectAll('.halo').data(data).transition();
  const handleOutline = svg.selectAll('.handle-outline').data(data).transition();
  const handle = svg.selectAll('.handle').data(data).transition();

  halo
    .duration(150)
    .attr("r", d => d.halo && d.height ? r2(d.halo/haloMax) : 0)
    .attr('fill', d => d.color)
    .attr("cx", function(d) { return x(d.name); })
    .attr("cy", function(d) { return y(d.height); })
    .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)')
    .attr("opacity", 0.4)
  handleOutline
    .duration(150)
    .attr("r", d => d.halo && d.height ? r1(handlePercent) : 0)
    .attr('fill', d => '#fff')
    .attr("cx", function(d) { return x(d.name); })
    .attr("cy", function(d) { return y(d.height); })
    .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)');
  handle
    .duration(150)
    .attr("r", d => d.halo && d.height ? r1(handlePercent*0.75) : 0)
    .attr('fill', d => d.color)
    .attr("cx", function(d) { return x(d.name); })
    .attr("cy", function(d) { return y(d.height); })
    .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)');

}

function type(d) {
  d.utilization = +d.utilization; // coerce to number
  return d;
}

function resize(data, width, height) {
  d3.select("#bubble-chart").selectAll("*").remove();
  init(data, width, height);
  // update(data, width, height);
}

export default { init, update, resize }
