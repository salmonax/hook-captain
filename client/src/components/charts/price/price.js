import d3 from 'd3';
import throttle from 'lodash.throttle';

var dummyData = [
  {
    "date": "1-May-12",
    "close": 58.13
  },
  {
    "date": "30-Apr-12",
    "close": 53.98
  },
  {
    "date": "27-Apr-12",
    "close": 67
  },
  {
    "date": "26-Apr-12",
    "close": 89.7
  },
  {
    "date": "25-Apr-12",
    "close": 99
  },
  {
    "date": "24-Apr-12",
    "close": 130.28
  },
  {
    "date": "23-Apr-12",
    "close": 166.7
  },
  {
    "date": "20-Apr-12",
    "close": 234.98
  },
  {
    "date": "19-Apr-12",
    "close": 345.44
  },
  {
    "date": "18-Apr-12",
    "close": 443.34
  },
  {
    "date": "17-Apr-12",
    "close": 543.7
  },
  {
    "date": "16-Apr-12",
    "close": 580.13
  },
  {
    "date": "13-Apr-12",
    "close": 605.23
  },
  {
    "date": "12-Apr-12",
    "close": 622.77
  },
  {
    "date": "11-Apr-12",
    "close": 626.2
  },
  {
    "date": "10-Apr-12",
    "close": 628.44
  },
  {
    "date": "9-Apr-12",
    "close": 636.23
  },
  {
    "date": "5-Apr-12",
    "close": 633.68
  },
  {
    "date": "4-Apr-12",
    "close": 624.31
  },
  {
    "date": "3-Apr-12",
    "close": 629.32
  },
  {
    "date": "2-Apr-12",
    "close": 618.63
  },
  {
    "date": "30-Mar-12",
    "close": 599.55
  },
  {
    "date": "29-Mar-12",
    "close": 609.86
  },
  {
    "date": "28-Mar-12",
    "close": 617.62
  },
  {
    "date": "27-Mar-12",
    "close": 614.48
  },
  {
    "date": "26-Mar-12",
    "close": 606.98
  }
];

const greenLine = '#41c274';
const orangeLine = '#ffac2f';
const gridLines = '#d8d8d8';

const invertData = (data) => {
    const reversed = data.slice().reverse();
    return data.map((item, i) => Object.assign({}, item, { close: reversed[i].close/1.5 }));
};

const normalize = (d) => {
  if (typeof d.date === 'object') return;
  d.date = parseDate(d.date);
  d.close = +d.close;
};



var reversedData = invertData(dummyData);

var margin = {top: 30, right: 0, bottom: 30, left: 0};

var x, y, xAxis, yAxis, xAxisTop, svg, id;
var firstLine, secondLine;
var xAxis, xAxisTop, yAxis;
// Parse the date / time
var parseDate = d3.time.format("%d-%b-%y").parse;
var mount;

function init(multidata, width, height, _mount) {
  width = width - margin.left - margin.right;
  height = height - margin.top - margin.bottom;


  multidata = [dummyData, reversedData];

  if (_mount) mount = '#' + _mount;


  // Set the ranges
  x = d3.time.scale().range([0, width]);
  y = d3.scale.linear().range([height, 0]);

  // Define the line
  firstLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.close); })
      .interpolate('basis');

  // Define the line
  secondLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.close); })
      .interpolate('basis');

  // Adds the svg canvas
  svg = d3.select("#price")
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");

  // Define the axes
  xAxis = d3.svg.axis().scale(x)
      .orient("bottom").ticks(5)
        .tickPadding(10)
        .innerTickSize(-height)
        .outerTickSize(0)
        .tickFormat((d, i) => `Week ${i + 1}`)

  xAxisTop = d3.svg.axis().scale(x)
      .orient("top")
      .ticks(5)
      .innerTickSize(0)
      .outerTickSize(0)
      .tickFormat('');


  yAxis = d3.svg.axis().scale(y)
      .orient("left").ticks(5);


  const [data, secondData] = multidata;

  // Former update starts here

  data.forEach(normalize);
  secondData.forEach(normalize);


  // Scale the range of the data
  x.domain(d3.extent(data, function(d) { return d.date; }));
  y.domain([0, d3.max(data, function(d) { return d.close; })]);

  // Add the X Axis and gridlines

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll('text')
        .style('text-anchor', 'end')
        .style('fill', 'gray');

  svg.append("g")
      .attr("class", "x axis top")
      .call(xAxisTop);


  // Add the first line
  svg.append("path")
      .attr("class", "first-line")
      .attr("d", firstLine(data))
      .attr('stroke', orangeLine);

  // Add the second line

  svg.append("path")
      .attr("class", "second-line")
      .attr("d", secondLine(secondData))
      .attr('stroke', greenLine);


  update(multidata, width, height);

  // setInterval(() => {
  //   const newData = multidata.map(data => data.map(item => Object.assign({}, item, { close: item.close*(Math.random()*3) })))
  //   update(newData, width, height);

  // }, 200);
}

function update(multidata, width, height) {
  const [data, secondData] = multidata;

  data.forEach(normalize);
  secondData.forEach(normalize);

  // Scale the range of the data
  x.domain(d3.extent(data, function(d) { return d.date; }));
  y.domain([0, d3.max(data, function(d) { return d.close; })]);

  const transition = svg.transition()

  transition.select('.first-line')
    .duration(150)
    .attr('d', firstLine(data));
  transition.select('.second-line')
    .duration(150)
    .attr('d', secondLine(secondData));

  transition.select('.x.axis')
    .duration(150)
    .call(xAxis);
  transition.select('.x.axis.top')
    .duration(150)
    .call(xAxisTop);


}

function resize(data, width, height) {
  d3.select(mount).selectAll("*").remove();
  init(data, width, height);
}

export default { init, update: throttle(update, 400), resize: throttle(resize, 400) }


