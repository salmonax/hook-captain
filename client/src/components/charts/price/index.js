import createG2 from 'g2-react';
import { Stat } from 'g2';
import React, { Component } from 'react';
// import data from '../../../fixtures/g2-pie-example.json';
import { observer, inject } from 'mobx-react';
import { toJS } from 'mobx';

import graph from './price';

class D3Wrapper extends Component {
  constructor(props) {
    super(props);
    this.id = "price"
  }

  componentDidMount() {
    const { data, width, height } = this.props;
    console.log('ON MOUNT:', width, height);
    graph.init(data, width, height, this.id);
    // graph.update(data, width, height);
    graph.resize(data, width, height);
  }


  componentWillReceiveProps(nextProps) {
    // console.log(nextProps);
    const { data, width, height } = nextProps;
    if (this.props.data !== nextProps.data) {
      // graph.update(data, width, height);
    } 
    if (this.props.width !== nextProps.width || this.props.height !== nextProps.height) {
      graph.resize(data, width, height);
    }

  }

  render() {
    return (
      <div id={this.id}></div>
    );
  }

}

@inject('common') @observer
class CranesChart extends Component {
  state = {
    width: 0,
    height: 0,
  };

  changeHandler() {
    const { chart } = this.refs.myChart;
    chart.clear();
    chart.intervalStack().position(Stat.summary.proportion()).color('price');      // operation
    chart.render();
  }

  // Note: refactor all of this:
  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    console.log('ON UPDATEDIMENSIONS', width, height);
    this.setState({ width, height });
  }

  componentDidMount() {
    window.addEventListener('resize',this._updateDimensions);
    this._updateDimensions();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  }

  mapData(scenario) {
    const tally = {};
    if (!scenario) return [];
    scenario.forEach(crane => {
      const { id, color } = crane;
      if (!tally[id]) tally[id] = { count: 0, color }
      tally[id].count++;
    });
    return Object.keys(tally).map(type => Object.assign(tally[type], { type }) )
  }

  render() {
    const { height, width } = this.state;

    const { abs, sin } = Math;
    const store = this.props.common;

    const { scenarios, currentScenario } = store;
    const { scenario } = this.props;

    const cranes = scenarios[scenario];
    
    const chartData = this.mapData(currentScenario);

    return (<div ref={(ref) => this.container = ref ? ref.parentNode : null }>
      { height && width ?
          <D3Wrapper
            width={this.state.width}
            height={this.state.height}
            data={chartData}
        /> : null
      }
        {/*<button onClick={this.changeHandler.bind(this)}>change</button>*/}
      </div>
    );
  }
}

export default CranesChart;


