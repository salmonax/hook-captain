import createG2 from 'g2-react';
import { Stat } from 'g2';
import React, { Component } from 'react';
import data from 'fixtures/g2-line-example.json';
import { observer, inject } from 'mobx-react';

// Note: not sure this pattern is really necessary, but leaving it for now
class HigherChart extends Component {

  constructor(props) {
    super(props);
    this.Chart = createG2(chart => {
      this.chart = chart;
      chart.line().position('time*price').color('name').shape(props.shape).size(2);
      chart.render();
    });
  }

  componentWillReceiveProps(nextProps) {
    // if (nextProps.data[5] === this.props.data[5]) {

    // }
    if (nextProps.shape !== this.props.shape) {
      this.chart.clear();
      this.chart.line().position('time*price').color('name').shape(nextProps.shape).size(2);
      this.chart.render();
    }
  }

  render() {
    return (<this.Chart {...this.props} />);
  }
}

@inject('common') @observer
class LineChart extends Component {
  state = {
    shape: 'spline',
    data: data.slice(0, data.length / 2 - 1),
    width: 0,
    height: 0,
    plotCfg: {
      margin: [10, 100, 50, 60],
    },
  }
  changeHandler = () => {
    this.setState({
      shape: 'line',
    });
  }

  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    this.setState({ width, height });
  }

  componentDidMount() {
    window.addEventListener('resize',this._updateDimensions);
    this._updateDimensions();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  }

  render() {
    const { height, width } = this.state;

    const { abs, sin } = Math;
    const store = this.props.common;
    const data = this.state.data.map((datum,i) => {
      return Object.assign({}, datum, { price: datum.price*abs(sin(store.random*.01+i)) });
    });

    return (<div ref={(ref) => this.container = ref ? ref.parentNode : null }>
      { height && width ?
          <HigherChart
            shape={this.state.shape}
            data={data}
            width={this.state.width}
            height={this.state.height}
            plotCfg={this.state.plotCfg}
        /> : null
      }
      {/*<button onClick={this.changeHandler}>Change shape</button>*/}
    </div>);
  }
}

export default LineChart;