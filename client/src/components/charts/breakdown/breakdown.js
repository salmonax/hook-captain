var dummyData = [
  {
    "date": "1-May-12",
    "close": 58.13
  },
  {
    "date": "30-Apr-12",
    "close": 53.98
  },
  {
    "date": "27-Apr-12",
    "close": 67
  },
  {
    "date": "26-Apr-12",
    "close": 89.7
  },
  {
    "date": "25-Apr-12",
    "close": 99
  },
  {
    "date": "24-Apr-12",
    "close": 130.28
  },
  {
    "date": "23-Apr-12",
    "close": 166.7
  },
  {
    "date": "20-Apr-12",
    "close": 234.98
  },
  {
    "date": "19-Apr-12",
    "close": 345.44
  },
  {
    "date": "18-Apr-12",
    "close": 443.34
  },
  {
    "date": "17-Apr-12",
    "close": 543.7
  },
  {
    "date": "16-Apr-12",
    "close": 580.13
  },
  {
    "date": "13-Apr-12",
    "close": 605.23
  },
  {
    "date": "12-Apr-12",
    "close": 622.77
  },
  {
    "date": "11-Apr-12",
    "close": 626.2
  },
  {
    "date": "10-Apr-12",
    "close": 628.44
  },
  {
    "date": "9-Apr-12",
    "close": 636.23
  },
  {
    "date": "5-Apr-12",
    "close": 633.68
  },
  {
    "date": "4-Apr-12",
    "close": 624.31
  },
  {
    "date": "3-Apr-12",
    "close": 629.32
  },
  {
    "date": "2-Apr-12",
    "close": 618.63
  },
  {
    "date": "30-Mar-12",
    "close": 599.55
  },
  {
    "date": "29-Mar-12",
    "close": 609.86
  },
  {
    "date": "28-Mar-12",
    "close": 617.62
  },
  {
    "date": "27-Mar-12",
    "close": 614.48
  },
  {
    "date": "26-Mar-12",
    "close": 606.98
  }
];

const greenLine = '#41c274';
const orangeLine = '#ffac2f';
const redLine = "#f00";
const gridLines = '#d8d8d8';

const aColor = "#ff9900";
const bColor = "#ff0099";
const cColor = "#0000ff";

const normalize = (d) => {
  if (typeof d.date == 'object') return;
  d.date = parseDate(d.date);
  d.close = +d.close;
};

const invertData = (data) => {
    const reversed = data.slice().reverse();
    return data.map((item, i) => Object.assign({}, item, { close: reversed[i].close/1.5 }));
};

dummyData = invertData(dummyData.map((item, i) => Object.assign({}, item, { close: Math.random()*50*i/1.5 })));

var reversedData = invertData(dummyData.map((item, i) => Object.assign({}, item, { close: Math.random()*50*i/1.5 })));

var whateverData = invertData(dummyData.map((item, i) => Object.assign({}, item, { close: Math.random()*50*i/2 })));


var margin = {top: 30, right: 50, bottom: 30, left: 60};

var x, y, xAxis, yAxis, xAxisTop, svg, id;
var firstLine, secondLine, thirdLine;
var parseDate = d3.time.format("%d-%b-%y").parse;
var mount;

function init(multidata, width, height, _mount) {
  width = width - margin.left - margin.right;
  height = height - margin.top - margin.bottom;

  // width = width/10;
  // height = height/10;

  multidata = [dummyData, reversedData, whateverData]; // superfluous, but just to signpost where it goes

  if (_mount) mount = '#' + _mount;

  // Adds the svg canvas
  svg = d3.select(mount)
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");

  // Set the ranges
  x = d3.time.scale().range([0, width]);
  y = d3.scale.linear().range([height, 0]);

  // Define the axes
  xAxis = d3.svg.axis().scale(x)
      .orient("bottom").ticks(5)
        .tickPadding(10)
        .innerTickSize(-height)
        .outerTickSize(0)
        .tickFormat((d, i) => `Week ${i + 1}`)

  xAxisTop = d3.svg.axis().scale(x)
      .orient("top")
      .ticks(5)
      .tickSize(0, 0)
      .tickFormat('');


  yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      .innerTickSize(-width)
      .outerTickSize(1)
      .tickPadding(10)
      .tickFormat(d => d);

  // NOTE: this was completely slapped in

  // Define the line
  firstLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.close); })
      .interpolate('basis');

  // Define the line
  secondLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.close); })
      .interpolate('basis');

  thirdLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.close); })
      .interpolate('basis');


  const [data, secondData, thirdData] = multidata;

  [data, secondData, thirdData]
    .forEach(dataset => dataset.forEach(normalize))

  // This is to round data to the nearest week; extract to a function
  var weekDomain = [];
  var minDate = Math.min.apply(null, data.map(datum => datum.date))
  var maxDays = 0;
  var maxDate = 0;
  const DAY = 86400000;
  data.forEach(({date}) => {
    const days = (date.getTime()-minDate)/DAY;
    if (!(days%7) && days > maxDays) {
      maxDays = days;
      maxDate = date.getTime();
    }
  });

  // Scale the range of the data
  // Note: this domain is adjusted to round to the nearest week
  x.domain([new Date(minDate), new Date(maxDate-DAY)]);
  y.domain([0, d3.max(data, function(d) { return d.close; })]);

  // Add the X Axis and gridlines

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll('text')
        .style('text-anchor', 'end')
        .style('fill', 'gray')
        // .attr('transform', 'translate(-'+ x(3.5*DAY+minDate) +')') // Really clever but don't need it
  
  svg.append("g")
      .attr("class", "x axis top")
      .call(xAxisTop);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .selectAll('text')
        .style('fill', 'gray');


  // Add the first line
  svg.append("path")
      .attr("class", "first-line")
      .attr("d", firstLine(data))
      .attr('stroke', aColor);

  // Add the second line

  svg.append("path")
      .attr("class", "second-line")
      .attr("d", secondLine(secondData))
      .attr('stroke', bColor);

  svg.append("path")
      .attr("class", "third-line")
      .attr("d", thirdLine(thirdData))
      .attr('stroke', cColor);

  // Add quick kludge to cover out-of-domain data
  svg.append("rect")
      .attr("fill", "white")
      .attr("x", width+1)
      .attr("y", -margin.top)
      .attr("height", height+margin.top+margin.bottom)
      .attr("width", margin.right);


  // update(multidata, width, height);
  // setInterval(() => {
  //   const newData = multidata.map(data => data.map(item => Object.assign({}, item, { close: item.close*(Math.random()*3) })))
  //   update(newData, width, height);

  // }, 200);
}


// Get the data
function update(multidata, width, height) {
    // Parse the date / time
    const [data, secondData, thirdData] = multidata;
    [data, secondData, thirdData]
      .forEach(dataset => dataset.forEach(normalize))

    // This is to round data to the nearest week; extract to a function
    // Note: it's assuming the domain is the same for all lines
    var weekDomain = [];
    var minDate = Math.min.apply(null, data.map(datum => datum.date))
    var maxDays = 0;
    var maxDate = 0;
    const DAY = 86400000;
    data.forEach(({date}) => {
      const days = (date.getTime()-minDate)/DAY;
      if (!(days%7) && days > maxDays) {
        maxDays = days;
        maxDate = date.getTime();
      }
    });

    // Scale the range of the data
    // Note: this domain is adjusted to round to the nearest week
    x.domain([new Date(minDate), new Date(maxDate-DAY)]);
    y.domain([0, d3.max(data, function(d) { return d.close; })]);

    const transition = svg.transition()

    transition.select('.first-line')
      .duration(150)
      .attr('d', firstLine(data));
    transition.select('.second-line')
      .duration(150)
      .attr('d', secondLine(secondData));
    transition.select('.third-line')
      .duration(150)
      .attr('d', thirdLine(thirdData));

    transition.select('.x.axis')
      .duration(150)
      .call(xAxis);
    transition.select('.x.axis.top')
      .duration(150)
      .call(xAxisTop);

    transition.select('.y.axis')
      .duration(150)
      .call(yAxis);


}

function resize(data, width, height) {
  d3.select(mount).selectAll("*").remove();
  init(data, width, height);
  // update(data, width, height);
}

export default { init, update, resize }

