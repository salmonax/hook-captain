import d3 from 'd3';
import throttle from 'lodash.throttle';

var dummyData = [
  {
    "date": "1-May-12",
    "close": 58.13
  },
  {
    "date": "30-Apr-12",
    "close": 53.98
  },
  {
    "date": "27-Apr-12",
    "close": 67
  },
  {
    "date": "26-Apr-12",
    "close": 89.7
  },
  {
    "date": "25-Apr-12",
    "close": 99
  },
  {
    "date": "24-Apr-12",
    "close": 130.28
  },
  {
    "date": "23-Apr-12",
    "close": 166.7
  },
  {
    "date": "20-Apr-12",
    "close": 234.98
  },
  {
    "date": "19-Apr-12",
    "close": 345.44
  },
  {
    "date": "18-Apr-12",
    "close": 443.34
  },
  {
    "date": "17-Apr-12",
    "close": 543.7
  },
  {
    "date": "16-Apr-12",
    "close": 280.13
  },
  {
    "date": "13-Apr-12",
    "close": 605.23
  },
  {
    "date": "12-Apr-12",
    "close": 622.77
  },
  {
    "date": "11-Apr-12",
    "close": 626.2
  },
  {
    "date": "10-Apr-12",
    "close": 628.44
  },
  {
    "date": "9-Apr-12",
    "close": 636.23
  },
  {
    "date": "5-Apr-12",
    "close": 633.68
  },
  {
    "date": "4-Apr-12",
    "close": 624.31
  },
  {
    "date": "3-Apr-12",
    "close": 629.32
  },
  {
    "date": "2-Apr-12",
    "close": 618.63
  },
  {
    "date": "30-Mar-12",
    "close": 599.55
  },
  {
    "date": "29-Mar-12",
    "close": 609.86
  },
  {
    "date": "28-Mar-12",
    "close": 617.62
  },
  {
    "date": "27-Mar-12",
    "close": 614.48
  },
  {
    "date": "26-Mar-12",
    "close": 606.98
  }
];

const lightArea = '#ffeed5';
const darkArea = '#ffe0b3';
const line = '#ffac2f';

const invertData = (data) => {
    const reversed = data.slice().reverse();
    return data.map((item, i) => Object.assign({}, item, { close: reversed[i].close/1.5 }));
};


var reversedData = invertData(dummyData);

var margin = {top: 30, right: 0, bottom: 30, left: 0};

var x, y, xAxis, yAxis, xAxisTop, svg, id;

var mount;

function init(multidata, width, height, _mount) {
  width = width - margin.left - margin.right,
  height = height - margin.top - margin.bottom,

  multidata = [dummyData, reversedData];

  if (_mount) mount = '#' + _mount;

  svg = d3.select(mount)
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");
  update(multidata, width, height);
}

function update(multidata, width, height) {

  // Parse the date / time
  var parseDate = d3.time.format("%d-%b-%y").parse;

  // Not on top of flow enough atm to piece through this
  // So just pulling it in from up top
  const [data, secondData] = [dummyData, reversedData]

  // Set the ranges
  x = d3.time.scale().range([0, width]);
  y = d3.scale.linear().range([height, 0]);

  // Define the axes
  var xAxis = d3.svg.axis().scale(x)
      .orient("bottom").ticks(5)
      .innerTickSize(-height)
      .tickPadding(10)
      .tickFormat((d, i) => `Week ${i + 1}`)

  // Define the line
  var firstLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.close); })
      .interpolate('basis');

  // Define the area
  var firstArea = d3.svg.area()
  .x(function(d) { return x(d.date); })
  .y0(height)
  .y1(function(d) { return y(d.close); })
  .interpolate('basis');

  // Define the line
  var secondLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.close); })
      .interpolate('basis');

  // Define the area
  var secondArea = d3.svg.area()
  .x(function(d) { return x(d.date); })
  .y0(height)
  .y1(function(d) { return y(d.close); })
  .interpolate('basis');

  const normalize = (d) => {
    if (typeof d.date === 'object') return;
    d.date = parseDate(d.date);
    d.close = +d.close;
  }

  console.log(typeof data, typeof secondData);
  data.forEach(normalize);
  secondData.forEach(normalize);


  // Scale the range of the data
  x.domain(d3.extent(data, function(d) { return d.date; }));
  y.domain([0, d3.max(data, function(d) { return d.close; })]);

  // Add the first line
  svg.append("path")
      .attr("class", "line")
      .attr("d", firstLine(data))
      .attr('stroke', line);

  // Add the first area

  svg.append("path")
     .data([data])
     .attr("class", "area")
     .attr("d", firstArea)
     .style('fill', darkArea);

  // Add the second line

  svg.append("path")
      .attr("class", "line")
      .attr("d", secondLine(secondData))
      .attr('stroke', line);

  // Add the second area

  svg.append("path")
     .data([secondData])
     .attr("class", "area")
     .attr("d", secondArea)
     .style('fill', lightArea);

  // Add the X Axis

  // select all lines, for interpolated gridlines
  const paths = svg.selectAll("path.line")[0];

  console.log('----------------------------', paths);

  // This keeps a potentially gigantic array around, but works for now
  let yMagic = [];
  paths.forEach(path => {
    const totalLength = path.getTotalLength();
    for (var i = 0; i < totalLength; i += 1) {
      const pos = path.getPointAtLength(i);
      const roundedX = Math.round(pos.x);
      if (!yMagic[roundedX] || (yMagic[roundedX] && yMagic[roundedX] > pos.y)) {
        yMagic[roundedX] = pos.y;
      }            
    }
  });

  // This is the time-inefficient alternative to get yMagic
  function getY(x, path, error){
    const totalLength = path.getTotalLength();
    // console.log('??',Math.round(x));
    x = Math.round(x);
    for (var i = 0; i < totalLength; i += 1) {
      const point = path.getPointAtLength(i);
      if (Math.round(x) === Math.round(point.x)) {
        return point.y;
      }
    }
    return -1;

  }



  // console.log('HERE',svg.selectAll("path.line"));
  const axisSelection = svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);


  axisSelection.selectAll('text')
      .style('text-anchor', 'end')
      .style('fill', 'gray')
  axisSelection.selectAll('line')
      .attr('y2', function(d) {
        const y = yMagic[Math.round(x(d))];
        return -height+y;
  });

  // Add the Y Axis
  // svg.append("g")
  //     .attr("class", "y axis")
  //     .call(yAxis);
}


function resize(data, width, height) {
  d3.select(mount).selectAll("*").remove();
  init(data, width, height);
}

export default { init, update: throttle(update, 400), resize: throttle(resize, 400) }