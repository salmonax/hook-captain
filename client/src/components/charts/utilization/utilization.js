import d3 from 'd3';
import throttle from 'lodash.throttle';

var dummyCranes = [
  {
    count: 1,
    "activity": 0.5,
    "color": "#ff9900",
    "type": "MB001"
  },
  {
    count: 2,
    "activity": 0.2,
    "color": "#ff0099",
    "type": "TC001"
  },
  {
    count: 3,
    "activity": 0.8,
    "color": "#0000ff",
    "type": "TC003"
  },
  {
    count: 4,
    "activity": 0.06,
    "color": "#4bc",
    "type": "MB002"
  },
];

// Take out, put into common helpers
const colorMap = (data) => {
  const output = {};
  data.forEach(item => {
    output[item.type] = item.color;
  })
  return output;
}




var margin = {top: 20, right: 50, bottom: 30, left: 50};

const gridLines = '#d8d8d8';


var x, y, xAxis, yAxis, svg;

function init(data, width, height) {
  try {
  console.log('init', data)
  width = Math.max(0, width - margin.left - margin.right);
  height = Math.max(0, height - margin.top - margin.bottom);
  // data = dummyCranes;
  // width = 50;
  // height = 50;
  svg = d3.select("#utilization-chart")
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");


  x = d3.scale.ordinal()
      .rangeRoundBands([0, width], .7, .35);

  y = d3.scale.linear()
      .range([height, 0]);

  xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .outerTickSize(-height)
      .innerTickSize(0)
      .tickPadding(10);

  yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      .innerTickSize(-width)
      .outerTickSize(1)
      .tickPadding(10)
      .tickFormat(d => {
        return d > 0 ? (d*100).toFixed(1)+'%' : ''
      });

  x.domain(data.map(function(d) { return d.count; }));
  y.domain([0, d3.max(data, d => d.activity)*1.1]);

  const colors = colorMap(data);
  const color = key => colors[key];

  // Should only do this when the domain changes
  // And it should be common to all components

  // svg.selectAll('.x.axis').data(data).enter()
  //     .append("g")
  //     .attr("class", "x axis")
  //     .attr("transform", "translate(0," + height + ")")
  //     .call(xAxis)

  // Seems the axis shouldn't have any data bound to it
  svg.append('g')
    .attr('class', 'x axis')
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

  svg.selectAll(".bar").data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr('fill', d => d.color)
      .attr('rx', 4)
      .attr("x", function(d) { return x(d.count); })
      .attr("y", function(d) { return y(d.activity); })
      .attr("height", function(d) { return Math.max(0, height - y(d.activity)); })
      .attr("width", x.rangeBand());

  svg.selectAll(".bar-title").data(data)
    .enter().append("rect")
      .attr("class", "bar-title")
      .attr('fill', d => d.color )
      .attr("x", function(d) { return x(d.count); })
      .attr("y", function(d) { return y(d.activity*.5); })
      .attr("height", function(d) { return Math.max(0, height - y(d.activity*.5)); })
      .attr("width", x.rangeBand());


  svg.selectAll('text')
    .style('fill', 'gray');

  let counter = 0;
  // setInterval(() => {
  //   let newData = data.map(item => Object.assign({}, item, { activity: Math.random() }))
  //   for (let i = 0; i < counter; i++) {
  //     newData.pop();
  //   }
  //   // newData = newData.slice(0, counter+1);
  //   counter++;
  //   if (counter === data.length) counter = 0;
  //   // if (!newData.length) newData = data;
  //   // console.log(JSON.stringify(newData, null, 2));
  //   update(newData, width, height);

  // },500);
  } catch(err) { console.log(err) }
}

function update(data, width, height) {
  // data = dummyCranes;
  try {
  // console.log('update', data);
  width = Math.max(0, width - margin.left - margin.right);
  height = Math.max(0, height - margin.top - margin.bottom);

  x.domain(data.map(function(d) { return d.count; }));
  y.domain([0, d3.max(data, d => d.activity)*1.1]);

  // update each bar
  // remember to handle enters and exits here; check cranes chart for reference
  // update the rangebands as well apparently

  const bar = svg.selectAll('.bar').data(data, d => d.count);
  const roundTop = svg.selectAll('.bar-title').data(data, d => d.count);

  // bar.exit().remove();
  // roundTop.exit().remove();
  bar.exit().transition().duration(150)
    .attr('y', d => height)
    .attr('height', d => 0)
    // .style('opacity', 0)
    .remove();
  roundTop.exit().transition().duration(150)
    .attr('y', d => height)
    .attr('height', d => 0)
    // .style('opacity', 0)
    .remove();

  bar.enter().append("rect")
      // .style('opacity', 0) // do a scale-up instead
      .attr("class", "bar")
      .attr('fill', d => d.color)
      .attr('rx', 4)
      .attr("x", function(d) { return x(d.count); })
      .attr("y", function(d) { return height; })
      .attr("height", function(d) { return 0; })
      .attr("width", x.rangeBand());
  
  roundTop.enter().append("rect")
      // .style('opacity', 0)
      .attr("class", "bar-title")
      .attr('fill', d => d.color )
      .attr("x", function(d) { return x(d.count); })
      .attr("y", function(d) { return height; })
      .attr("height", function(d) { return 0; })
      .attr("width", x.rangeBand())
      
  bar.transition().duration(150)
      // .style('opacity', 1)
      .attr("class", "bar")
      .attr('fill', d => d.color)
      .attr('rx', 4)
      .attr("x", function(d) { return x(d.count); })
      .attr("y", function(d) { return y(d.activity); })
      .attr("height", function(d) { return Math.max(0, height - y(d.activity)); })
      .attr("width", x.rangeBand());

  roundTop.transition().duration(150)
      // .style('opacity', 1)
      .attr("class", "bar-title")
      .attr('fill', d => d.color )
      .attr("x", function(d) { return x(d.count); })
      .attr("y", function(d) { return y(d.activity*.5); })
      .attr("height", function(d) { return Math.max(0, height - y(d.activity*.5)); })
      .attr("width", x.rangeBand());


  svg.transition().select('.x.axis')
    .duration(150)
    .call(xAxis);

  svg.transition().select('.y.axis')
    .duration(150)
    .call(yAxis);
  
  // update the x axis
  } catch(err) { console.log(err) }
}

function type(d) {
  d.activity = +d.activity; // coerce to number
  return d;
}

function resize(data, width, height) {
  d3.select("#utilization-chart").selectAll("*").remove();
  init(data, width, height);
  update(data, width, height);
}


export default { init, update: throttle(update, 400), resize: throttle(resize, 400) }