const WEEK = 60*60*24*7*1000;
// let datedWeather = weatherData.map((d, i) => Object.assign(d, { date: new Date(i*WEEK), probOfImpact: Math.random()*.27}));
// datedWeather = datedWeather; // one month in the past, four months ahead

function preparedData(data) {
  const dates = Array(data.length).fill().map((d, i) => new Date(i*WEEK));
  const now = new Date();
  let currentWeek = new Date(0);
  currentWeek.setDate(now.getDate());
  currentWeek.setMonth(now.getMonth());
  currentWeek.setYear(dates[3].getYear());
  currentWeek.setHours(0,0,0,0);
  // currentWeek = new Date(1970, 0, 4); // for testing purposes

  const nextWeekIndex = dates.findIndex(date => currentWeek < date.getTime());
  // Shift the weather over
  let shiftedWeather;
  if (nextWeekIndex >= 3) {
    shiftedWeather = data.slice(nextWeekIndex-3).concat(data.slice(0, nextWeekIndex-3));
  } else {
    shiftedWeather = data.slice(data.length-nextWeekIndex).concat(data.slice(0, data.length-nextWeekIndex))
    console.log(shiftedWeather.length, data.length)
  }

  // shift currentWeek date to equivalent point at beginning
  const deltaFromNextWeek = dates[nextWeekIndex].getTime()-currentWeek.getTime();
  currentWeek = new Date(dates[3].getTime()-deltaFromNextWeek);
  const datedWeather = data.slice(0,20).map((d, i) => Object.assign(d, { date: dates[i], probOfImpact: d.probOfImpact !== undefined ? d.probOfImpact : 0 }));
  return { datedWeather, currentWeek }
}

const greenLine = '#41c274';
const orangeLine = '#ffac2f';
const redLine = "#f00";
const gridLines = '#d8d8d8';

// var parseDate = d3.time.format("%d-%b-%y").parse;
// const normalize = (d) => {
//   if (typeof d.date == 'object') return;
//   d.date = parseDate(d.date);
//   d.avgWindSp = +d.avgWindSp;
// };

const invertData = (data) => {
    const reversed = data.slice().reverse();
    return data.map((item, i) => Object.assign({}, item, { close: reversed[i].avgWindSp/1.5 }));
};

var margin = {top: 20, right: 50, bottom: 30, left: 50};
var x, y, cranesY, xAxis, yAxis, xAxisTop, craneAxis, svg, id;
var medianLine, lowerLine, upperLine, upperBandwidth, lowerBandwidth;
var currentDateLine;

var mount;



function init(data, width, height, _mount) {
  width = Math.max(0, width - margin.left - margin.right);
  height = Math.max(0, height - margin.top - margin.bottom);

  const { datedWeather, currentWeek } = preparedData(data);
  const rawData = data;
  data = datedWeather;

  // data.forEach(normalize);
  // This is to round data to the nearest week; extract to a function
  var weekDomain = [];
  var minDate = Math.min.apply(null, data.map(datum => datum.date))
  var maxDays = 0;
  var maxDate = 0;
  const DAY = 86400000;
  data.forEach(({date}) => {
    const days = (date.getTime()-minDate)/DAY;
    if (!(days%7) && days > maxDays) {
      maxDays = days;
      maxDate = date.getTime();
    }
  });
  // Set the ranges
  x = d3.time.scale().range([0, width]);
  y = d3.scale.linear().range([height, 0]);
  cranesY = d3.scale.linear().range([height, 0]);
  // Scale the range of the data
  // Note: this domain is adjusted to round to the nearest week
  x.domain([new Date(minDate), new Date(maxDate)]);
  y.domain([0, d3.max(data, function(d) { return d.bandwidth[1]*1.1; })]);
  // console.log('!!!', d3.max(data, d=> d.probOfImpact ))
  cranesY.domain([0, d3.max(data, d => d.probOfImpact)*2]);


  if (_mount) mount = '#' + _mount;
  // Adds the svg canvas
  svg = d3.select(mount)
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");



  // Define the axes
  xAxis = d3.svg.axis().scale(x)
      .orient("bottom").ticks(12)
      .tickPadding(10)
      .innerTickSize(-height)
      .outerTickSize(0)
      .tickFormat((d, i) => i-2)

  xAxisTop = d3.svg.axis().scale(x)
      .orient("top")
      .ticks(5)
      .tickSize(0, 0)
      .tickFormat('');


  yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      // .innerTickSize(-width)
      // .outerTickSize(1)
      .tickSize(0)
      .tickPadding(6)
      .tickFormat(d => d + ' m/s');

  craneAxis = d3.svg.axis()
      .scale(cranesY)
      .orient("right")
      .ticks(5)
      // .innerTickSize(-width)
      // .outerTickSize(1)
      .tickSize(0)
      .tickPadding(10)
      .tickFormat(d => d + '%');


  // Define the line
  medianLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.avgWindSp); })
      .interpolate('basis');

  // Define the line
  lowerLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.bandwidth[0]); })
      .interpolate('basis');

  upperLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.bandwidth[1]); })
      .interpolate('basis');

  upperBandwidth = d3.svg.area()
    .interpolate('basis')
    .x(function (d) { return x(d.date); })
    .y0(function (d) { return y(d.bandwidth[1]); })
    .y1(function (d) { return y(d.avgWindSp); });

  medianLine = d3.svg.line()
    .interpolate('basis')
    .x(function (d) { return x(d.date); })
    .y(function (d) { return y(d.avgWindSp); });

  lowerBandwidth = d3.svg.area()
    .interpolate('basis')
    .x(function (d) { return x(d.date); })
    .y0(function (d) { return y(d.avgWindSp); })
    .y1(function (d) { return y(d.bandwidth[0]); });


  // Add the X Axis and gridlines

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll('text')
        .style('text-anchor', 'center')
        .style('fill', 'gray')
        // .attr("transform", "translate(-"+x(WEEK/2)+",0)")
        // .attr('transform', 'translate(-'+ x(3.5*DAY+minDate) +')') // Really clever but don't need it


  svg.selectAll('.x.axis .tick line')
    // gulp, not sure about this magic number
    .attr('transform', 'translate('+x(WEEK/2+DAY/8)+',0)');
  
  svg.append("g")
    .attr("class", "x axis top")
    .call(xAxisTop);

  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .selectAll('text')
    .style('fill', 'gray');

  svg.append("path")
      .attr("class", "upper-bandwidth")
      .attr("d", upperBandwidth(data))
      .style('fill', '#0b5')
      .style('opacity', 0.5)
      .style('stroke-width', 1);

  svg.append("path")
      .attr("class", "lower-bandwidth")
      .attr("d", lowerBandwidth(data))
      .style('fill', '#0b5')
      .style('opacity', 0.5)
      .style('stroke-width', 1);

  // Add the first line
  svg.append("path")
      .attr("class", "median-line")
      .attr("d", medianLine(data))
      .attr('stroke', greenLine)
      .style('stroke-width', 2)

  // Add the second line

  svg.append("path")
      .attr("class", "lower-line")
      .attr("d", lowerLine(data))
      .attr('stroke', greenLine)
      .style('stroke-width', 1);

  svg.append("path")
      .attr("class", "upper-line")
      .attr("d", upperLine(data))
      .attr('stroke', greenLine)
      .style('stroke-width', 1);


  svg.selectAll(".bar").data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr('fill', d => '#2c78ba')
      .attr('rx', 3)
      .attr("x", function(d) { return x(d.date); })
      .attr("y", function(d) { return cranesY(d.probOfImpact); })
      .attr("height", function(d) { return Math.max(0, height - cranesY(d.probOfImpact)); })
      .attr("width", x(WEEK/2))
      .attr("transform", "translate("+x(WEEK/4)+",0)")

  svg.selectAll(".bar-title").data(data)
    .enter().append("rect")
      .attr("class", "bar-title")
      .attr('fill', d => '#2c78ba' )
      .attr("x", function(d) { return x(d.date); })
      .attr("y", function(d) { return cranesY(d.probOfImpact*.5); })
      .attr("height", function(d) { return Math.max(0, height - cranesY(d.probOfImpact*.5)); })
      .attr("width", x(WEEK/2))
      .attr("transform", "translate("+x(WEEK/4)+",0)")


  // Add quick kludge to cover out-of-domain data
  svg.append("rect")
      .attr("fill", "white")
      .attr("x", width+1)
      .attr("y", -margin.top)
      .attr("height", height+margin.top+margin.bottom)
      .attr("width", margin.right);

  // Draw crane axis on top
  svg.append('g')
    .attr('class', 'crane axis')
    .call(craneAxis)
    .attr('transform', 'translate('+width+',0)')
    .selectAll('text')
    .style('fill', 'gray');

  svg.append("line")
    .attr("class", "date-line")
    .attr("x1", x(currentWeek))
    .attr("x2", x(currentWeek))
    .attr("y1", 0)
    .attr("y2", height)
    .attr("stroke", "red");



  // setInterval(() => {
  //     const newData = rawData.map(item => { 
  //       const rando = item.avgWindSp*(Math.random()+1);
  //       return Object.assign({}, item, { 
  //         probOfImpact: Math.random()*0.27,
  //         avgWindSp: rando,
  //         bandwidth: [rando*0.8,rando*1.2]
  //       });
  //     });

  // //   //   const newData = multidata.map(data => data.map(item => Object.assign({}, item, { close: item.avgWindSp*(Math.random()*3) })))
  //   update(newData, width, height);

  // }, 200);
  // update(rawData, width, height);
}


// Get the data
function update(data, width, height) {
    // Parse the date / time
    // const [data, secondData, thirdData] = multidata;
    // [data, secondData, thirdData]
    //   .forEach(dataset => dataset.forEach(normalize))
    width = Math.max(0, width - margin.left - margin.right);
    height = Math.max(0, height - margin.top - margin.bottom);

    const { datedWeather, currentWeek } = preparedData(data);
    const rawData = data;
    data = datedWeather;

    // This is to round data to the nearest week; extract to a function
    // Note: it's assuming the domain is the same for all lines
    var weekDomain = [];
    var minDate = Math.min.apply(null, data.map(datum => datum.date))
    var maxDays = 0;
    var maxDate = 0;
    const DAY = 86400000;
    data.forEach(({date}) => {
      const days = (date.getTime()-minDate)/DAY;
      if (!(days%7) && days > maxDays) {
        maxDays = days;
        maxDate = date.getTime();
      }
    });

    // Scale the range of the data
    // Note: this domain is adjusted to round to the nearest week
    x.domain([new Date(minDate), new Date(maxDate)]);
    y.domain([0, d3.max(data, function(d) { return d.bandwidth[1]*1.1; })]);
    console.log('!!!', d3.max(data, d=> d.probOfImpact ))
    cranesY.domain([0, d3.max(data, d => d.probOfImpact)*2]);

    const transition = svg.transition()

    transition.select(".upper-bandwidth")
        .duration(150)
        .attr("d", upperBandwidth(data))
        .style('fill', '#0b5')
        .style('opacity', 0.5)
        .style('stroke-width', 1)

    transition.select(".lower-bandwidth")
        .duration(150)
        .attr("d", lowerBandwidth(data))
        .style('fill', '#0b5')
        .style('opacity', 0.5)
        .style('stroke-width', 1)

    transition.select('.median-line')
      .duration(150)
      .attr('d', medianLine(data));
    transition.select('.lower-line')
      .duration(150)
      .attr('d', lowerLine(data));
    transition.select('.upper-line')
      .duration(150)
      .attr('d', upperLine(data));

    transition.select('.x.axis')
      .duration(150)
      .call(xAxis)
      .selectAll('text')
        .style('text-anchor', 'center')
        .style('fill', 'gray');

    transition.select('.crane.axis')
      .duration(150)
      .call(craneAxis)
      .attr('transform', 'translate('+width+',0)')
        .selectAll('text')
        .style('fill', 'gray');






    transition.select('.x.axis.top')
      .duration(150)
      .call(xAxisTop);

    transition.select('.y.axis')
      .duration(150)
      .call(yAxis);

    const bar = svg.selectAll('.bar').data(data, d => d.date);
    const roundTop = svg.selectAll('.bar-title').data(data, d => d.date);

    bar.exit().transition().duration(150)
      .attr('y', d => height)
      .attr('height', d => 0)
      // .style('opacity', 0)
      .remove();
    roundTop.exit().transition().duration(150)
      .attr('y', d => height)
      .attr('height', d => 0)
      // .style('opacity', 0)
      .remove();

    bar.enter().append("rect")
        // .style('opacity', 0) // do a scale-up instead
        .attr("class", "bar")
        // .attr('fill', d => d.color)
        .attr('rx', 4)
        .attr("x", function(d) { return x(d.date); })
        .attr("y", function(d) { return height; })
        .attr("height", function(d) { return 0; })
        .attr("width", x(WEEK/2));
    
    roundTop.enter().append("rect")
        // .style('opacity', 0)
        .attr("class", "bar-title")
        .attr('fill', d => d.color )
        .attr("x", function(d) { return x(d.date); })
        .attr("y", function(d) { return height; })
        .attr("height", function(d) { return 0; })
        .attr("width", x(WEEK/2))
        


    bar.transition().duration(150)
        // .style('opacity', 1)
        .attr("class", "bar")
        // .attr('fill', d => d.color)
        .attr('rx', 4)
        .attr("x", function(d) { return x(d.date); })
        .attr("y", function(d) { return cranesY(d.probOfImpact); })
        .attr("height", function(d) { return Math.max(0, height - cranesY(d.probOfImpact)); })
        .attr("width", x(WEEK/2));

    roundTop.transition().duration(150)
        // .style('opacity', 1)
        .attr("class", "bar-title")
        // .attr('fill', d => d.color )
        .attr("x", function(d) { return x(d.date); })
        .attr("y", function(d) { return cranesY(d.probOfImpact*.5); })
        .attr("height", function(d) { return Math.max(0,height - cranesY(d.probOfImpact*.5)); })
        .attr("width", x(WEEK/2));



}

function resize(data, width, height) {
  d3.select(mount).selectAll("*").remove();
  init(data, width, height);
  // update(data, width, height);
}

export default { init, update, resize }

