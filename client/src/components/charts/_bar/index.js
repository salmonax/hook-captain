import React from 'react';
import './index.scss';

export default function _Bar(props) {
  const { fill, rear } = props;
  const setColor = (color) => color ? { backgroundColor: color } : null;
  const fillWidth = {
    width: `${props.value}%`,
    transition: 'width 150ms', 
  };
  return (
    <div>
      <div className='simple-bar-rear' style={setColor(rear)}>
        <div className='simple-bar-fill' style={Object.assign(fillWidth, setColor(fill))} />
      </div>
    </div>
  );
}