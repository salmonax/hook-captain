import createG2 from 'g2-react';
import { Stat } from 'g2';
import React, { Component } from 'react';
// import data from '../../../fixtures/g2-pie-example.json';
import { observer, inject } from 'mobx-react';
import { toJS } from 'mobx';


const data = [
  { type:'Type A', count: 2},
  { type:'Type B', count: 1},
  { type:'Type C', count: 3},
];
      // var Stat = G2.Stat;


const dummyData = [
  {
    "count": 2,
    "color": "#ff9900",
    "type": "MB001"
  },
  {
    "count": 1,
    "color": "#ff0099",
    "type": "TC001"
  },
  {
    "count": 3,
    "color": "#0099ff",
    "type": "TC003"
  }
];

class HigherChart extends Component {
  constructor(props) {
    super(props);
    this.Chart = createG2(chart => {
      this.chart = chart;
      chart.source(dummyData);
      // 以 year 为维度划分分面

      chart.legend({
        position: 'bottom'
      });
      chart.coord('theta', {
        radius: 1,
        inner: 0.9
      });
      chart.tooltip({
        title: null
      });
      chart.intervalStack()
        .position(Stat.summary.percent('count'))
        .color('type')
        .style({
          lineWidth: 1
        });
      chart.render();
    });
  }

  componentWillReceiveProps(nextProps) {
    this.chart.clear();
    const colors = nextProps.data.map(crane => crane.color);
    this.chart.intervalStack().position(Stat.summary.percent('count'))
      .color('type', colors);
  }

  render() {
    return (<this.Chart {...this.props} />);
  }
}

@inject('common') @observer
class PieChart extends Component {
  state = {
    data: data.slice(0, data.length / 2 - 1),
    width: 500,
    height: 250,
    plotCfg: {
      margin: [10, 10, 50, 10],
    },
  };

  changeHandler() {
    const { chart } = this.refs.myChart;
    chart.clear();
    chart.intervalStack().position(Stat.summary.proportion()).color('price');      // operation
    chart.render();
  }

  // Note: refactor all of this:
  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    this.setState({ width, height });
  }

  componentDidMount() {
    window.addEventListener('resize',this._updateDimensions);
    this._updateDimensions();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  }

  mapData(scenario) {
    const tally = {};
    if (!scenario) return [];
    scenario.forEach(crane => {
      const { id, color } = crane;
      if (!tally[id]) tally[id] = { count: 0, color }
      tally[id].count++;
    });
    return Object.keys(tally).map(type => Object.assign(tally[type], { type }) )
  }

  render() {
    const { height, width } = this.state;

    const { abs, sin } = Math;
    const store = this.props.common;

    const { scenarios, currentScenario } = store;
    const { scenario } = this.props;

    const cranes = scenarios[scenario];
    
    const chartData = this.mapData(currentScenario);
    // console.log('??', JSON.stringify(chartData, null, 2));


    return (<div ref={(ref) => this.container = ref ? ref.parentNode : null }>
      { height && width ?
          <HigherChart
            shape={this.state.shape}
            width={this.state.width}
            height={this.state.height}
            plotCfg={this.state.plotCfg}
            data={chartData}
        /> : null
      }
        {/*<button onClick={this.changeHandler.bind(this)}>change</button>*/}
      </div>
    );
  }
}

export default PieChart;


