import createG2 from 'g2-react';
import { Stat } from 'g2';
import React, { Component } from 'react';
// import data from '../../../fixtures/g2-pie-example.json';
import { observer, inject } from 'mobx-react';
import { toJS } from 'mobx';

import graph from './stacked';


// import algoOutput from 'fixtures/algo.json';

const LABELS = {
  'Scenario A': 'Scenario A',
  'Scenario B': 'Scenario B',
  'Scenario C': 'Scenario C',
};

const aColor = "#ff9900";
const bColor = "#ff0099";
const cColor = "#0000ff";

// const COLORS = {
//   'Scenario A': aColor,
//   'Scenario B': bColor,
//   'Scenario C': cColor,
// }
const COLORS = {
  Value: '#19C388',
  Speed: '#497DF9',
  Agility: '#E82089',
};

// const dummyScenarios = {
//   'Scenario A': algoOutput,
//   'Scenario B': algoOutput,
//   'Scenario C': algoOutput,
// };

const SCENARIOS = ['Scenario A', 'Scenario B', 'Scenario C'];

const formatterMap = {
  Value: d => '£ '+(d/1000000).toFixed() + 'M',
  Speed: d => d + ' days',
  Agility: d => (d*100).toFixed() + '%', 
};


class D3Wrapper extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { data, width, height } = this.props;
    console.log('ON MOUNT:', width, height);
    graph.init(data, width, height, formatterMap[this.props.indicator]);
    // graph.update(data, width, height);
    graph.resize(data, width, height, formatterMap[this.props.indicator]);
  }


  componentWillReceiveProps(nextProps) {
    const { data, width, height } = nextProps;
    if (this.props.data !== nextProps.data) {
      // console.log('new props!', nextProps, JSON.stringify(nextProps.data));

      graph.update(data, width, height, formatterMap[nextProps.indicator]);
    } 
    if (this.props.width !== nextProps.width || this.props.height !== nextProps.height) {
      graph.resize(data, width, height, formatterMap[nextProps.indicator]);
    }

  }

  render() {
    return (
      <div id="stacked-chart"></div>
    );
  }

}

@inject('common') @observer
class CranesChart extends Component {
  state = {
    width: 0,
    height: 0,
  };

  changeHandler() {
    const { chart } = this.refs.myChart;
    chart.clear();
    chart.intervalStack().position(Stat.summary.proportion()).color('price');      // operation
    chart.render();
  }

  // Note: refactor all of this:
  _updateDimensions = () => {
    const computed = getComputedStyle(this.container);
    const { offsetWidth, offsetHeight } = this.container;
    const { paddingLeft, paddingRight, paddingBottom, paddingTop } = computed;
    const width = offsetWidth - parseFloat(paddingRight) - parseFloat(paddingLeft);
    const height = offsetHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
    console.log('ON UPDATEDIMENSIONS', width, height);
    this.setState({ width, height });
  }

  componentDidMount() {
    window.addEventListener('resize',this._updateDimensions);
    this._updateDimensions();
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  }


  extractBreakdowns(scenarioMap, key) {
    const { indicator } = this.props;
    const output = [];
    const isCost = /Cost/.test(key);
    const pushEmpty = scenario => {
      const layers = {
          Installation: 0,
          Operation: 0,
          Delay: 0,
      };
      if (isCost) Object.assign(layers, {
        Maintenance: 0,
      });
      output.push({
        name: scenario,
        color: COLORS[indicator],
        layers,
      });
    };
    SCENARIOS.forEach(scenario => {
      if (!scenarioMap[scenario] || !Object.keys(scenarioMap[scenario]).length) {
        pushEmpty(scenario);
        return;
      };
      const scenarioMetrics = scenarioMap[scenario].get ? 
        scenarioMap[scenario].get('scenarioMetrics') : 
        scenarioMap[scenario]['scenarioMetrics'];

      if (!scenarioMetrics[key]) {
        pushEmpty(scenario);
        return;
      }

      if (isCost) {
        // Extract instCost, opCost, delayCost, maintCost
        const { instCost, opCost, delayCost, maintCost } = scenarioMetrics[key];
        output.push({
          name: scenario,
          color: COLORS[indicator],
          layers: {
            Installation: instCost,
            Operation: opCost, 
            Delay: Math.abs(delayCost),
            Maintenance: maintCost,
          },

        });

      } else {
        // Extract Zones: A, B, C
        const { A, B, C } = scenarioMetrics[key];
        output.push({
          name: scenario,
          color: COLORS[indicator],
          layers: {
            'Installation': Math.abs(A),
            'Operation': Math.abs(B),
            'Delay': Math.abs(C),
          },
        });
      }
    });
    return output;
  }

  render() {
    const { height, width } = this.state;

    const { abs, sin } = Math;
    const common = this.props.common;

    const { scenarios, currentScenario } = common;
    const { scenario } = this.props;

    const cranes = scenarios[scenario];
    
    // const chartData = this.mapData(currentScenario);

    const chartData = this.extractBreakdowns(common.algoMapByScenario, this.props.chartKey);
    // const chartData = this.extractBreakdowns(dummyScenarios, this.props.chartKey);

    // console.warn('here is: ', JSON.stringify(chartData, null, 2));

    const starterData = [{ layers: {
      Installation: 0,
      Operation: 0,
      Delay: 0,
    } }];

    return (<div ref={(ref) => this.container = ref ? ref.parentNode : null }>
      { height && width ?
          <D3Wrapper
            indicator={this.props.indicator}
            width={this.state.width}
            height={this.state.height}
            data={chartData.length ? chartData : starterData}
        /> : null
      }
        {/*<button onClick={this.changeHandler.bind(this)}>change</button>*/}
      </div>
    );
  }
}

export default CranesChart;


