import d3 from 'd3';
import throttle from 'lodash.throttle';

var dummyCranes = [
  {
    name: 'Scenario A',
    layers: {
      installation: 0.05,
      operation: 0.1,
      relay: 0.1,
      mileage: 0.1,
    },
    "color": "#ff9900",
    "type": "MB001"
  },
  {
    name: 'Scenario B',
    layers: {
      installation: 0.3,
      operation: 0.1,
      relay: 0.3,
      mileage: 0.1,
    },
    "color": "#ff0099",
    "type": "TC001"
  },
  {
    name: 'Scenario C',
    layers: {
      installation: 0.2,
      operation: 0.2,
      relay: 0.3,
      mileage: 0,
    },
    "color": "#0000ff",
    "type": "TC003"
  }
];

// Take out
const colorMap = (data) => {
  const output = {};
  data.forEach(item => {
    output[item.type] = item.color;
  })
  return output;
}


// var margin = {top: 30, right: 50, bottom: 30, left: 60};
// Note: kludging this for the component; they're not actually accurate
var margin = {top: 12, right: 50, bottom: 50, left: 60};

const gridLines = '#d8d8d8';

var x, y, xAxis, yAxis, svg, xAxisTop;

var innerPadding = 0.5;
var outerPadding = innerPadding/2;

// const layers = `installation operation relay mileage`.split(' ');
const rounding = 7; // should be propertional to graph size

let interval = null

const getMax = (data) => Math.max.apply(
  null, 
  data.map(scenario => 
    Object.keys(data[0].layers).reduce((total, layer) => total += scenario.layers[layer],0)
  )
);

function init(data, width, height, formatter) {
  // const layers = data.data;

  width = width - margin.left - margin.right;
  height = height - margin.top - margin.bottom;


  // data = dummyCranes;

  const layers = Object.keys(data[0].layers);

  svg = d3.select("#stacked-chart")
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");


  x = d3.scale.ordinal()
      .rangeRoundBands([0, width], innerPadding, outerPadding);

  y = d3.scale.linear()
      .range([height, 0]);

  xAxisTop = d3.svg.axis().scale(x)
      .orient("top")
      .ticks(5)
      .tickSize(0, 0)
      .tickFormat('');
  svg.append("g")
    .attr("class", "x axis top")
    .call(xAxisTop);
    // .attr("transform", "translate(0," + 10 + ")");


  xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .outerTickSize(1)
      .innerTickSize(-height)
      .tickPadding(10);

  yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      .innerTickSize(-width)
      .outerTickSize(1)
      .tickPadding(10)
      .tickFormat(formatter);

  x.domain(data.map(function(d) { return d.name; }));
  y.domain([0, Math.max(1,getMax(data)*1.1)]);


  const colors = colorMap(data);
  const color = key => colors[key];

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
        .selectAll('line')
          .attr('transform', 'translate(' + (x.rangeBand()/2+(x.rangeBand()*innerPadding)-0.5) + ',-1)'); 

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);


  let stackOffset = Array(layers.length).fill(0);

  // NOTE: check if values are happening here on resize
  // Also, this enter pattern appears to be spurious
  
  // const zeroTops = layers.slice().reverse().findIndex(n => n);
  const getTopLayerIndex = (d) => {
    // return layers.length-1;
    const values = layers.map(key => d.layers[key]).reverse();
    return Math.min(layers.length-1-values.findIndex(n => n), layers.length-1);
  }
  const getTopLayer = (d) => layers[getTopLayerIndex(d)]
  const isTop = (d, layer) => getTopLayer(d) === layer;
  const mult = (d, layer) => isTop(d, layer) ? 0.5 : 1;


  layers.forEach((layer, i) => {
    svg.selectAll(`.bar-${layer}`).data(data)
        .enter().append('rect')
        .attr("class", `bar-${layer}`)
        .attr('fill', d => d3.hcl(d.color).brighter(i/2))
        .attr("x", function(d) { return x(d.name); })
        .attr("y", function(d, i) { return y(d.layers[layer]*mult(d, layer) + stackOffset[i]); })
        .attr("height", function(d) { return Math.max(0, height - y(d.layers[layer]*mult(d, layer))); })
        .attr("width", x.rangeBand())
        .each((d, i) => { if (!isTop(d, layer)) stackOffset[i] += d.layers[layer] });
  });

  svg.selectAll(`.bar-topper`).data(data)
    .enter().append("rect")
      .attr('class', `bar-topper`)
      .attr('fill', d => d3.hcl(d.color).brighter(getTopLayerIndex(d)/2))
      .attr('rx', rounding)
      .attr("x", function(d) { return x(d.name); })
      .attr("y", function(d, i) { return y(d.layers[getTopLayer(d)] + stackOffset[i]); })
      .attr("height", function(d) { return Math.max(0, height - y(d.layers[getTopLayer(d)])); })
      .attr("width", x.rangeBand())
      .each((d, i) => stackOffset[i] += d.layers[getTopLayer(d)]);


  svg.selectAll('text')
    .style('fill', 'gray');

  // Interestingly, this is the first time I'm canceling the interval here
  // And this *may* be the only chart that can't handle resizes correctly.
  // if (interval) clearInterval(interval);
  // interval = setInterval(() => {
  //   console.log('interval', interval);
  //   let zeroHerder = Math.random()/3;
  //   if (zeroHerder < 0.3) zeroHerder = 0;
  //   console.log(zeroHerder);
  //   const newRandomStuff = { 
  //     operation: Math.random()/10, 
  //     relay: Math.random()/10,
  //     installation: Math.random()/2,
  //     mileage: zeroHerder,
  //   };
  //   // delete newRandomStuff['mileage']; // doesn't work

  //   const newData = data.map(item => Object.assign({}, item, { layers: newRandomStuff }))

  //   // console.log(newData)
  //   // console.log(JSON.stringify(newData, null, 2));
  //   update(newData, width, height, true);

  // },500);


}

function update(data, width, height, formatter) {
  // if (!debug) {
      width = width - margin.left - margin.right;
      height = height - margin.top - margin.bottom;
  // }
  window.formatter = formatter;
  window.data = data;
  // x = d3.scale.ordinal()
      // .rangeRoundBands([0, width], innerPadding, outerPadding);

  // y = d3.scale.linear()
      // .range([height, 0]);

  x.domain(data.map(function(d) { return d.name; }));
  y.domain([0, Math.max(1,getMax(data)*1.1)]);

  // console.warn('Y DOMAIN: ', Math.max(1,getMax(data)*1.1));


  const layers = Object.keys(data[0].layers);

  let stackOffset = Array(layers.length).fill(0);

  // NOTE: check if values are happening here on resize
  // Also, this enter pattern appears to be spurious

  // const zeroTops = layers.slice().reverse().findIndex(n => n);

  yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      .innerTickSize(-width)
      .outerTickSize(1)
      .tickPadding(10)
      .tickFormat(formatter);


  const getTopLayerIndex = (d) => {
    // return layers.length-1;
    const values = layers.map(key => d.layers[key]).reverse();
    return Math.min(layers.length-1-values.findIndex(n => n), layers.length-1);
  }
  const getTopLayer = (d) => layers[getTopLayerIndex(d)]
  const isTop = (d, layer) => getTopLayer(d) === layer;
  const mult = (d, layer) => isTop(d, layer) ? 0.5 : 1;

  // last minute kludge to erase maintenance item:
  if (!layers.includes('Maintenance')) {
    svg.selectAll(`.bar-Maintenance`).remove();
  }

  layers.forEach((layer, i) => {
    const barLayer = svg.selectAll(`.bar-${layer}`).data(data);
    barLayer.enter().append('rect')
        .attr("class", `bar-${layer}`)
        .attr('fill', d => d3.hcl(d.color).brighter(i/2))
        .attr("x", function(d) { return x(d.name); })
        .attr("y", function(d, i) { return y(d.layers[layer]*mult(d, layer) + stackOffset[i]); })
        .attr("height", function(d) { return Math.max(0, height - y(d.layers[layer]*mult(d, layer))); })
        .attr("width", x.rangeBand())
        .each((d, i) => { if (!isTop(d, layer)) stackOffset[i] += d.layers[layer] });

    barLayer.transition().duration(150)
        .attr("class", `bar-${layer}`)
        .attr('fill', d => d3.hcl(d.color).brighter(i/2))
        .attr("x", function(d) { return x(d.name); })
        .attr("y", function(d, i) { return y(d.layers[layer]*mult(d, layer) + stackOffset[i]); })
        .attr("height", function(d) { return Math.max(0, height - y(d.layers[layer]*mult(d, layer))); })
        .attr("width", x.rangeBand())
        .each((d, i) => { if (!isTop(d, layer)) stackOffset[i] += d.layers[layer] });
  });

  const barTopper = svg.selectAll(`.bar-topper`).data(data);
  barTopper.transition().duration(150)
    // .enter().append("rect")
      .attr('class', `bar-topper`)
      .attr('fill', d => d3.hcl(d.color).brighter(getTopLayerIndex(d)/2))
      .attr('rx', rounding)
      .attr("x", function(d) { return x(d.name); })
      .attr("y", function(d, i) { 
        return y(d.layers[getTopLayer(d)] + stackOffset[i]); })
      .attr("height", function(d) {
        return Math.max(0, height - y(d.layers[getTopLayer(d)])); })
      .attr("width", x.rangeBand())
      .each((d, i) => stackOffset[i] += d.layers[getTopLayer(d)]);

  svg.selectAll('.y.axis').transition().duration(150).call(yAxis);
  // console.warn('weqweweqew', yAxis);

  svg.selectAll('.x.axis.top').transition().duration(150).call(xAxisTop);

}

function type(d) {
  d.utilization = +d.utilization; // coerce to number
  return d;
}

function resize(data, width, height, formatter) {
  d3.select("#stacked-chart").selectAll("*").remove();
  init(data, width, height, formatter);
  // update(data, width, height);
}

export default { init, update, resize: throttle(resize, 400) }
