const Path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const cssnano = require('cssnano');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = (options) => {
  const webpackConfig = {
    devtool: options.devtool,
    entry: [
      `webpack-dev-server/client?http://localhost:${+ options.port}`,
      Path.join(__dirname, '../src/index'),
    ],
    output: {
      path: Path.join(__dirname, '../dist'),
      filename: `scripts/${options.jsFileName}`,
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: [
        'client/src/components',
        'client/src',
        'client/static',
        'node_modules',
      ],
    },
    module: {
      rules: [{
        test: /.jsx?$/,
        include: [Path.join(__dirname, '../src')],
        loader: 'babel-loader',
        query: {
          plugins: [
            'transform-decorators-legacy',
            'transform-class-properties',
            'transform-function-bind',
            'transform-object-rest-spread',
            ['import', [{ libraryName: 'flux-ui' }]],
          ],
        },
      }, {
        test: /\.woff2$/,
        loader: 'url-loader?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]',
      }, {
        test: /\.(png)$/,
        loader: 'url-loader?name=images/[name].[ext]&limit=8192',
      }, {
        test:  /\.(ttf)$/,
        loader: 'file-loader?name=assets/[name].[ext]'
      }],
    },
    plugins: [
      new Webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(options.isProduction ? 'production' : 'development'),
        },
      }),
      new HtmlWebpackPlugin({
        template: Path.join(__dirname, '../static/index.html'),
      }),
      // new BundleAnalyzerPlugin(),
      new Webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    ],
  };

  if (options.isProduction) {
    webpackConfig.entry = [Path.join(__dirname, '../src/index')];
    webpackConfig.plugins.push(
      new CopyWebpackPlugin([{ 
        from: Path.join(__dirname, '../static' ),
        to: Path.join(__dirname, '../dist/assets')
      }]),
      // new Webpack.optimize.OccurenceOrderPlugin(),
      new Webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false,
        },
      }),
      new CompressionPlugin({
        asset: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0.8,
      }),
      new Webpack.LoaderOptionsPlugin({
        minimize: true,
      }),
      new ExtractTextPlugin({ filename: 'styles.css', disable: false, allChunks: true }),
      new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.optimize\.css$/g,
        cssProcessor: cssnano,
        cssProcessorOptions: { discardComments: { removeAll: true } },
        canPrint: true,
      })
    );

    webpackConfig.module.rules.push({
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: ['css-loader', 'sass-loader'],
      }),
    });

    webpackConfig.module.rules.push({
      test: /\.css$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader', use: 'css-loader',
      }),
    });
  } else {
    webpackConfig.module.rules.push({
      test: /\.scss$/,
      loaders: ['style-loader', 'css-loader', 'sass-loader'],
    });

    webpackConfig.module.rules.push({
      test: /\.css$/,
      loaders: ['style-loader', 'css-loader'],
    });

    webpackConfig.devServer = {
      contentBase: Path.join(__dirname, '../'),
      port: options.port,
      inline: true,
      historyApiFallback: true,
      disableHostCheck: true,
      proxy: {
        '/ws': { target: 'ws://localhost:3001', ws: true },
        '/api': { target: 'http://localhost:3001', secure: false },
        '/login': { target: 'http://localhost:3001', secure: false },
        '/logout': { target: 'http://localhost:3001', secure: false },
        '/assets': { target: 'http://localhost:3001', secure: false },
      },
    };
  }

  return webpackConfig;
};
