const modelGenerator = require('./model/index.js');
const componentGenerator = require('./component/index.js');
const routeGenerator = require('./route/index.js');
const storeGenerator = require('./store/index.js');

module.exports = (plop) => {
  plop.setGenerator('model', modelGenerator);
  plop.setGenerator('component', componentGenerator);
  plop.setGenerator('route', routeGenerator);
  plop.setGenerator('store', storeGenerator);
};
