const fs = require('fs');

function modelExists(comp) {
  try {
    fs.accessSync(`client/src/stores/${comp}.js`, fs.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}

module.exports = {
  description: 'Add a store',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    default: 'user',
    validate: (value) => {
      if ((/.+/).test(value)) {
        return modelExists(value) ? 'A store with this name already exists' : true;
      }
      return 'The name is required';
    },
  }],
  actions: (data) => {
    const actions = [{
      type: 'add',
      path: '../../client/src/stores/{{camelCase name}}.js',
      templateFile: './store/store.js.hbs',
      abortOnFail: true,
    }];
    return actions;
  },
};
