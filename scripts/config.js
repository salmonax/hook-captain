require('shelljs/global');
let fs = require('fs');
let p = require('../package.json');

let dir = pwd().split('/');
let dashed = dir[dir.length-1];
let title = dashed.split('-').map((a) => a.charAt(0).toUpperCase() + a.slice(1)).join(' ');

exec('rm ./scripts/config.js')
exec('find . -path ./node_modules -prune -o -name "*.*" -type f -print', (code, stdout, stderr) => {
  let files = stdout.trim().split("\n");
  files.map((file) => {
    sed('-i', /flux-kit/, dashed, file);
    sed('-i', /Flux Kit/, title, file);
  })
  exec('rm -rf ./.git')
  exec('git init')
  exec('git add .')
  exec('git commit -m "Initial commit"')
})

delete p.scripts.config;
fs.writeFileSync('package.json', JSON.stringify(p, null, 2))

exec('yarn')
