(function(mount = 'risk', height = 250, width = 380) {
var weatherData = [
  {
    "avgWindSp": 4.032,
    "bandwidth": [
      2.89,
      5.174
    ]
  },
  {
    "avgWindSp": 4.052,
    "bandwidth": [
      2.903,
      5.2
    ]
  },
  {
    "avgWindSp": 2.849,
    "bandwidth": [
      1.86,
      3.838
    ]
  },
  {
    "avgWindSp": 3.701,
    "bandwidth": [
      2.441,
      4.96
    ]
  },
  {
    "avgWindSp": 4.671,
    "bandwidth": [
      3.245,
      6.096
    ]
  },
  {
    "avgWindSp": 4.461,
    "bandwidth": [
      3.073,
      5.849
    ]
  },
  {
    "avgWindSp": 3.874,
    "bandwidth": [
      2.489,
      5.259
    ]
  },
  {
    "avgWindSp": 4.344,
    "bandwidth": [
      3.045,
      5.644
    ]
  },
  {
    "avgWindSp": 4.385,
    "bandwidth": [
      3.291,
      5.48
    ]
  },
  {
    "avgWindSp": 3.25,
    "bandwidth": [
      2.357,
      4.143
    ]
  },
  {
    "avgWindSp": 4.315,
    "bandwidth": [
      3.178,
      5.451
    ]
  },
  {
    "avgWindSp": 4.898,
    "bandwidth": [
      3.592,
      6.204
    ]
  },
  {
    "avgWindSp": 3.819,
    "bandwidth": [
      2.812,
      4.825
    ]
  },
  {
    "avgWindSp": 3.68,
    "bandwidth": [
      2.707,
      4.653
    ]
  },
  {
    "avgWindSp": 3.875,
    "bandwidth": [
      2.979,
      4.772
    ]
  },
  {
    "avgWindSp": 3.956,
    "bandwidth": [
      2.98,
      4.932
    ]
  },
  {
    "avgWindSp": 3.695,
    "bandwidth": [
      2.797,
      4.593
    ]
  },
  {
    "avgWindSp": 3.883,
    "bandwidth": [
      2.949,
      4.817
    ]
  },
  {
    "avgWindSp": 3.953,
    "bandwidth": [
      2.83,
      5.076
    ]
  },
  {
    "avgWindSp": 3.347,
    "bandwidth": [
      2.37,
      4.324
    ]
  },
  {
    "avgWindSp": 3.504,
    "bandwidth": [
      2.684,
      4.323
    ]
  },
  {
    "avgWindSp": 3.361,
    "bandwidth": [
      2.549,
      4.173
    ]
  },
  {
    "avgWindSp": 3.4,
    "bandwidth": [
      2.044,
      4.755
    ]
  },
  {
    "avgWindSp": 3.038,
    "bandwidth": [
      1.946,
      4.131
    ]
  },
  {
    "avgWindSp": 3.188,
    "bandwidth": [
      2.142,
      4.235
    ]
  },
  {
    "avgWindSp": 3.369,
    "bandwidth": [
      2.461,
      4.278
    ]
  },
  {
    "avgWindSp": 3.795,
    "bandwidth": [
      2.951,
      4.638
    ]
  },
  {
    "avgWindSp": 3.452,
    "bandwidth": [
      2.611,
      4.293
    ]
  },
  {
    "avgWindSp": 3.33,
    "bandwidth": [
      2.352,
      4.309
    ]
  },
  {
    "avgWindSp": 3.308,
    "bandwidth": [
      2.518,
      4.099
    ]
  },
  {
    "avgWindSp": 3.495,
    "bandwidth": [
      2.504,
      4.486
    ]
  },
  {
    "avgWindSp": 3.084,
    "bandwidth": [
      2.107,
      4.062
    ]
  },
  {
    "avgWindSp": 3.514,
    "bandwidth": [
      2.396,
      4.632
    ]
  },
  {
    "avgWindSp": 3.24,
    "bandwidth": [
      2.38,
      4.101
    ]
  },
  {
    "avgWindSp": 3.668,
    "bandwidth": [
      2.741,
      4.594
    ]
  },
  {
    "avgWindSp": 2.618,
    "bandwidth": [
      1.911,
      3.325
    ]
  },
  {
    "avgWindSp": 3.103,
    "bandwidth": [
      2.229,
      3.976
    ]
  },
  {
    "avgWindSp": 2.765,
    "bandwidth": [
      1.917,
      3.613
    ]
  },
  {
    "avgWindSp": 2.834,
    "bandwidth": [
      1.942,
      3.726
    ]
  },
  {
    "avgWindSp": 2.837,
    "bandwidth": [
      2.007,
      3.668
    ]
  },
  {
    "avgWindSp": 3.445,
    "bandwidth": [
      2.518,
      4.372
    ]
  },
  {
    "avgWindSp": 3.416,
    "bandwidth": [
      2.656,
      4.176
    ]
  },
  {
    "avgWindSp": 3.402,
    "bandwidth": [
      2.346,
      4.457
    ]
  },
  {
    "avgWindSp": 3.205,
    "bandwidth": [
      2.379,
      4.03
    ]
  },
  {
    "avgWindSp": 3.336,
    "bandwidth": [
      2.415,
      4.258
    ]
  },
  {
    "avgWindSp": 3.784,
    "bandwidth": [
      2.625,
      4.943
    ]
  },
  {
    "avgWindSp": 3.682,
    "bandwidth": [
      2.516,
      4.849
    ]
  },
  {
    "avgWindSp": 3.02,
    "bandwidth": [
      1.954,
      4.085
    ]
  },
  {
    "avgWindSp": 3.983,
    "bandwidth": [
      2.867,
      5.1
    ]
  },
  {
    "avgWindSp": 3.705,
    "bandwidth": [
      2.521,
      4.889
    ]
  },
  {
    "avgWindSp": 4.575,
    "bandwidth": [
      3.45,
      5.7
    ]
  },
  {
    "avgWindSp": 4.478,
    "bandwidth": [
      3.153,
      5.803
    ]
  },
  {
    "avgWindSp": 5.103,
    "bandwidth": [
      4.177,
      6.028
    ]
  }
];



const WEEK = 60*60*24*7*1000;
let datedWeather = weatherData.map((d, i) => Object.assign(d, { date: new Date(i*WEEK), numOfCrane: Math.random()*.27}));
datedWeather = datedWeather; // one month in the past, four months ahead

const now = new Date();
let currentWeek = new Date(0);
currentWeek.setDate(now.getDate());
currentWeek.setMonth(now.getMonth());
currentWeek.setYear(currentWeek.getYear()+1);
currentWeek.setHours(0,0,0,0);
// currentWeek = new Date(1970, 0, 4); // for testing purposes

const nextWeekIndex = datedWeather.findIndex(item => currentWeek < item.date.getTime());
console.log(nextWeekIndex);
// Shift the weather over
let shiftedWeather;
if (nextWeekIndex >= 3) {
  shiftedWeather = datedWeather.slice(nextWeekIndex-3).concat(datedWeather.slice(0, nextWeekIndex-3));
} else {
  shiftedWeather = datedWeather.slice(datedWeather.length-nextWeekIndex).concat(datedWeather.slice(0, datedWeather.length-nextWeekIndex))
  console.log(shiftedWeather.length, datedWeather.length)
}

// shift currentWeek date to equivalent point at beginning
const deltaFromNextWeek = datedWeather[nextWeekIndex].date.getTime()-currentWeek.getTime();
currentWeek = new Date(datedWeather[3].date.getTime()-deltaFromNextWeek);


datedWeather = datedWeather.map((item, i) => Object.assign({}, item, { avgWindSp: shiftedWeather[i].avgWindSp, bandwidth: shiftedWeather[i].bandwidth }))
datedWeather = datedWeather.slice(0,20);




const greenLine = '#41c274';
const orangeLine = '#ffac2f';
const redLine = "#f00";
const gridLines = '#d8d8d8';

const normalize = (d) => {
  if (typeof d.date == 'object') return;
  d.date = parseDate(d.date);
  d.avgWindSp = +d.avgWindSp;
};

const invertData = (data) => {
    const reversed = data.slice().reverse();
    return data.map((item, i) => Object.assign({}, item, { close: reversed[i].avgWindSp/1.5 }));
};


const dummyData = datedWeather;
// console.log(JSON.stringify(dummyData));

// dummyData = invertData(dummyData.map((item, i) => Object.assign({}, item, { close: Math.random()*50*i/1.5 })));

// var reversedData = invertData(dummyData.map((item, i) => Object.assign({}, item, { close: Math.random()*50*i/1.5 })));

// var whateverData = invertData(dummyData.map((item, i) => Object.assign({}, item, { close: Math.random()*50*i/2 })));

var reversedData = dummyData;
var whateverData = dummyData;


var margin = {top: 30, right: 50, bottom: 30, left: 60};

var x, y, cranesY, xAxis, yAxis, xAxisTop, craneAxis, svg, id;
var medianLine, lowerLine, upperLine, upperBandwidth, lowerBandwidth;
var currentDateLine;
var parseDate = d3.time.format("%d-%b-%y").parse;
var mount;

function init(multidata, width, height, _mount) {
  width = width - margin.left - margin.right;
  height = height - margin.top - margin.bottom;

  // width = width/10;
  // height = height/10;

  multidata = [dummyData, reversedData, whateverData]; // superfluous, but just to signpost where it goes

  if (_mount) mount = '#' + _mount;

  // Adds the svg canvas
  svg = d3.select(mount)
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");

  // Set the ranges
  x = d3.time.scale().range([0, width]);
  y = d3.scale.linear().range([height, 0]);
  cranesY = d3.scale.linear().range([height, 0]);

  // Define the axes
  xAxis = d3.svg.axis().scale(x)
      .orient("bottom").ticks(12)
      .tickPadding(10)
      .innerTickSize(-height)
      .outerTickSize(0)
      .tickFormat((d, i) => i-2)

  xAxisTop = d3.svg.axis().scale(x)
      .orient("top")
      .ticks(5)
      .tickSize(0, 0)
      .tickFormat('');


  yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      // .innerTickSize(-width)
      // .outerTickSize(1)
      .tickSize(0)
      .tickPadding(6)
      .tickFormat(d => d + ' m/s');

  craneAxis = d3.svg.axis()
      .scale(cranesY)
      .orient("right")
      .ticks(5)
      // .innerTickSize(-width)
      // .outerTickSize(1)
      .tickSize(0)
      .tickPadding(10)
      .tickFormat(d => d*100 + '%');


  // NOTE: this was completely slapped in

  // Define the line
  medianLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.avgWindSp); })
      .interpolate('basis');

  // Define the line
  lowerLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.bandwidth[0]); })
      .interpolate('basis');

  upperLine = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.bandwidth[1]); })
      .interpolate('basis');

  upperBandwidth = d3.svg.area()
    .interpolate('basis')
    .x(function (d) { return x(d.date); })
    .y0(function (d) { return y(d.bandwidth[1]); })
    .y1(function (d) { return y(d.avgWindSp); });

  medianLine = d3.svg.line()
    .interpolate('basis')
    .x(function (d) { return x(d.date); })
    .y(function (d) { return y(d.avgWindSp); });

  lowerBandwidth = d3.svg.area()
    .interpolate('basis')
    .x(function (d) { return x(d.date); })
    .y0(function (d) { return y(d.avgWindSp); })
    .y1(function (d) { return y(d.bandwidth[0]); });




  const [data, secondData, thirdData] = multidata;

  [data, secondData, thirdData]
    .forEach(dataset => dataset.forEach(normalize))

  // This is to round data to the nearest week; extract to a function
  var weekDomain = [];
  var minDate = Math.min.apply(null, data.map(datum => datum.date))
  var maxDays = 0;
  var maxDate = 0;
  const DAY = 86400000;
  data.forEach(({date}) => {
    const days = (date.getTime()-minDate)/DAY;
    if (!(days%7) && days > maxDays) {
      maxDays = days;
      maxDate = date.getTime();
    }
  });

  // Scale the range of the data
  // Note: this domain is adjusted to round to the nearest week
  x.domain([new Date(minDate), new Date(maxDate)]);
  y.domain([0, d3.max(data, function(d) { return d.bandwidth[1]*1.1; })]);
  cranesY.domain([0, 1]);

  // Add the X Axis and gridlines

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll('text')
        .style('text-anchor', 'center')
        .style('fill', 'gray')
        // .attr("transform", "translate(-"+x(WEEK/2)+",0)")
        // .attr('transform', 'translate(-'+ x(3.5*DAY+minDate) +')') // Really clever but don't need it


  svg.selectAll('.x.axis .tick line')
    // gulp, not sure about this magic number
    .attr('transform', 'translate('+x(WEEK/2+DAY/8)+',0)');
  
  svg.append("g")
    .attr("class", "x axis top")
    .call(xAxisTop);

  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .selectAll('text')
    .style('fill', 'gray');

  svg.append("path")
      .attr("class", "upper-bandwidth")
      .attr("d", upperBandwidth(data))
      .style('fill', '#0b5')
      .style('opacity', 0.5)
      .style('stroke-width', 1);

  svg.append("path")
      .attr("class", "lower-bandwidth")
      .attr("d", lowerBandwidth(data))
      .style('fill', '#0b5')
      .style('opacity', 0.5)
      .style('stroke-width', 1);

  // Add the first line
  svg.append("path")
      .attr("class", "median-line")
      .attr("d", medianLine(data))
      .attr('stroke', greenLine)
      .style('stroke-width', 2)

  // Add the second line

  svg.append("path")
      .attr("class", "lower-line")
      .attr("d", lowerLine(data))
      .attr('stroke', greenLine)
      .style('stroke-width', 1);

  svg.append("path")
      .attr("class", "upper-line")
      .attr("d", upperLine(data))
      .attr('stroke', greenLine)
      .style('stroke-width', 1);


  svg.selectAll(".bar").data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr('fill', d => '#fa0')
      .attr('rx', 3)
      .attr("x", function(d) { return x(d.date); })
      .attr("y", function(d) { return cranesY(d.numOfCrane); })
      .attr("height", function(d) { return height - cranesY(d.numOfCrane); })
      .attr("width", x(WEEK/2))
      .attr("transform", "translate("+x(WEEK/4)+",0)")

  svg.selectAll(".bar-title").data(data)
    .enter().append("rect")
      .attr("class", "bar-title")
      .attr('fill', d => '#fa0' )
      .attr("x", function(d) { return x(d.date); })
      .attr("y", function(d) { return cranesY(d.numOfCrane*.5); })
      .attr("height", function(d) { return height - cranesY(d.numOfCrane*.5); })
      .attr("width", x(WEEK/2))
      .attr("transform", "translate("+x(WEEK/4)+",0)")


  // Add quick kludge to cover out-of-domain data
  svg.append("rect")
      .attr("fill", "white")
      .attr("x", width+1)
      .attr("y", -margin.top)
      .attr("height", height+margin.top+margin.bottom)
      .attr("width", margin.right);

  // Draw crane axis on top
  svg.append('g')
    .attr('class', 'crane axis')
    .call(craneAxis)
    .attr('transform', 'translate('+width+',0)')
    .selectAll('text')
    .style('fill', 'gray');

  svg.append("line")
    .attr("class", "date-line")
    .attr("x1", x(currentWeek))
    .attr("x2", x(currentWeek))
    .attr("y1", 0)
    .attr("y2", height)
    .attr("stroke", "red");



  // setInterval(() => {
  //     const newData = datedWeather.map(item => { 
  //       const rando = item.avgWindSp*(Math.random()+1);
  //       return Object.assign({}, item, { 
  //         numOfCrane: Math.round(Math.random()*4),
  //         avgWindSp: rando,
  //         bandwidth: [rando*0.8,rando*1.2]
  //       });
  //     });

  // //   //   const newData = multidata.map(data => data.map(item => Object.assign({}, item, { close: item.avgWindSp*(Math.random()*3) })))
  //   update(newData, width, height);

  // }, 200);
}


// Get the data
function update(data, width, height) {
    // Parse the date / time
    // const [data, secondData, thirdData] = multidata;
    // [data, secondData, thirdData]
    //   .forEach(dataset => dataset.forEach(normalize))

    // This is to round data to the nearest week; extract to a function
    // Note: it's assuming the domain is the same for all lines
    var weekDomain = [];
    var minDate = Math.min.apply(null, data.map(datum => datum.date))
    var maxDays = 0;
    var maxDate = 0;
    const DAY = 86400000;
    data.forEach(({date}) => {
      const days = (date.getTime()-minDate)/DAY;
      if (!(days%7) && days > maxDays) {
        maxDays = days;
        maxDate = date.getTime();
      }
    });

    // Scale the range of the data
    // Note: this domain is adjusted to round to the nearest week
    x.domain([new Date(minDate), new Date(maxDate)]);
    y.domain([0, d3.max(data, function(d) { return d.bandwidth[1]; })]);
    cranesY.domain([0, d3.max(data, function(d) { return d.bandwidth[1]; })]);

    const transition = svg.transition()

    transition.select(".upper-bandwidth")
        .duration(150)
        .attr("d", upperBandwidth(data))
        .style('fill', '#0b5')
        .style('opacity', 0.5)
        .style('stroke-width', 1)

    transition.select(".lower-bandwidth")
        .duration(150)
        .attr("d", lowerBandwidth(data))
        .style('fill', '#0b5')
        .style('opacity', 0.5)
        .style('stroke-width', 1)

    transition.select('.median-line')
      .duration(150)
      .attr('d', medianLine(data));
    transition.select('.lower-line')
      .duration(150)
      .attr('d', lowerLine(data));
    transition.select('.upper-line')
      .duration(150)
      .attr('d', upperLine(data));

    transition.select('.x.axis')
      .duration(150)
      .call(xAxis)
      .selectAll('text')
        .style('text-anchor', 'center')
        .style('fill', 'gray')

    transition.select('.x.axis.top')
      .duration(150)
      .call(xAxisTop);

    transition.select('.y.axis')
      .duration(150)
      .call(yAxis);

    const bar = svg.selectAll('.bar').data(data, d => d.date);
    const roundTop = svg.selectAll('.bar-title').data(data, d => d.date);

    bar.exit().transition().duration(150)
      .attr('y', d => height)
      .attr('height', d => 0)
      // .style('opacity', 0)
      .remove();
    roundTop.exit().transition().duration(150)
      .attr('y', d => height)
      .attr('height', d => 0)
      // .style('opacity', 0)
      .remove();

    bar.enter().append("rect")
        // .style('opacity', 0) // do a scale-up instead
        .attr("class", "bar")
        // .attr('fill', d => d.color)
        .attr('rx', 4)
        .attr("x", function(d) { return x(d.date); })
        .attr("y", function(d) { return height; })
        .attr("height", function(d) { return 0; })
        .attr("width", x(WEEK/2));
    
    roundTop.enter().append("rect")
        // .style('opacity', 0)
        .attr("class", "bar-title")
        .attr('fill', d => d.color )
        .attr("x", function(d) { return x(d.date); })
        .attr("y", function(d) { return height; })
        .attr("height", function(d) { return 0; })
        .attr("width", x(WEEK/2))
        


    bar.transition().duration(150)
        // .style('opacity', 1)
        .attr("class", "bar")
        // .attr('fill', d => d.color)
        .attr('rx', 4)
        .attr("x", function(d) { return x(d.date); })
        .attr("y", function(d) { return y(d.numOfCrane); })
        .attr("height", function(d) { return height - y(d.numOfCrane); })
        .attr("width", x(WEEK/2));

    roundTop.transition().duration(150)
        // .style('opacity', 1)
        .attr("class", "bar-title")
        // .attr('fill', d => d.color )
        .attr("x", function(d) { return x(d.date); })
        .attr("y", function(d) { return y(d.numOfCrane*.5); })
        .attr("height", function(d) { return height - y(d.numOfCrane*.5); })
        .attr("width", x(WEEK/2));



}

function resize(data, width, height) {
  d3.select(mount).selectAll("*").remove();
  init(data, width, height);
  // update(data, width, height);
}

init(dummyData, width, height, mount);

})();