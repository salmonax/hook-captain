(function() {
  var dummyCranes = [
    {
      name: 'Scenario A',
      halo: .8,
      height: 0.6,
      color: "#ff9900",
    },
    {
      name: 'Scenario B',
      halo: 0.6,
      height: 0.2,
      color: "#ff0099",
    },
    {
      name: 'Scenario C',
      halo: 0.3,
      height: 0.4,
      color: "#0000ff",
    }
  ];

  // Take out
  const colorMap = (data) => {
    const output = {};
    data.forEach(item => {
      output[item.type] = item.color;
    })
    return output;
  }




  var margin = {top: 30, right: 50, bottom: 30, left: 50};
  var width = 380 - margin.left - margin.right,
      height = 250 - margin.top - margin.bottom,
      radius = Math.min(width, height) / 2;
  var svg;

  const gridLines = '#d8d8d8';

  // Adds the svg canvas
  var svg = d3.select("#bubble")
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");

  // Do other stuff

  var innerPadding = 0.5;
  var outerPadding = 0.35;
  var x = d3.scale.ordinal()
      .rangeRoundBands([0, width]);

  window.x = x

  var y = d3.scale.linear()
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .outerTickSize(1)
      .innerTickSize(-height)
      .tickPadding(10);

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      .innerTickSize(-width)
      .outerTickSize(1)
      .tickPadding(10)
      .tickFormat(d => {
        return d > 0 ? (d*100).toFixed()+'%' : ''
      });


  function update(data) {
    x.domain(data.map(function(d) { return d.name; }));
    y.domain([0, 1]);

    var r1 = d3.scale.linear()
            .range([0, x.rangeBand()/2 ]); // radius for the handle

    var handlePercent = 0.2;
    var r2 = d3.scale.linear()
             .range([r1(handlePercent), x.rangeBand()/2]); // radius for quantities

    

    const colors = colorMap(data);
    const color = key => colors[key];

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
          .selectAll('line')
            .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)'); 

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    const enterSelection = svg.selectAll(".circle").data(data).enter();

    enterSelection.append("circle")
          .attr("r", d=> r2(d.halo))
          .attr("class", "bar")
          .attr('fill', d => d.color)
          .attr("cx", function(d) { return x(d.name); })
          .attr("cy", function(d) { return y(d.height); })
          .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)')
          .attr("opacity", 0.4)
    enterSelection.append("circle")
          .attr("r", r1(handlePercent))
          .attr("class", "bar")
          .attr('fill', d => '#fff')
          .attr("cx", function(d) { return x(d.name); })
          .attr("cy", function(d) { return y(d.height); })
          .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)');
    enterSelection.append("circle")
          .attr("r", r1(handlePercent*0.75))
          .attr("class", "bar")
          .attr('fill', d => d.color)
          .attr("cx", function(d) { return x(d.name); })
          .attr("cy", function(d) { return y(d.height); })
          .attr('transform', 'translate(' + x.rangeBand()/2 + ',0)');





    svg.selectAll('text')
      .style('fill', 'gray');
  }

  function type(d) {
    d.utilization = +d.utilization; // coerce to number
    return d;
  }
  update(dummyCranes);
})();