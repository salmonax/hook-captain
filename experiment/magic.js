const fs = require('fs');
const pug = require('pug');

const sass = require('jstransformer-sass');

const fn = process.argv[2];
const pugFile = `${fn}.pug`;
const htmlFile = `${fn}.html`;


console.log('Now watching '+pugFile);
fs.watch(pugFile, (eventType, filename) => {
  console.log('Change detected; rebuilding');
  try {
    const htmlOut = pug.renderFile(pugFile);
    fs.writeFileSync(htmlFile,htmlOut) 
  } catch(err) {
    console.log('There was an error; watching for file change');
  }
});
