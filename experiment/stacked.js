(function() {
  /**
    Notes:
      1. Grid offset calcs are hard coded, so padding can't be changed
      2. Right gridline corners are hackily nudged into position

  **/

  var dummyCranes = [
    {
      name: 'Scenario A',
      "utilization": 0.5,
      installation: 0.05,
      operation: 0.1,
      relay: 0.1,
      mileage: 0.1,
      "color": "#ff9900",
      "type": "MB001"
    },
    {
      name: 'Scenario B',
      "utilization": 0.2,
      installation: 0.3,
      operation: 0.1,
      relay: 0.3,
      mileage: 0.1,
      "color": "#ff0099",
      "type": "TC001"
    },
    {
      name: 'Scenario C',
      installation: 0.2,
      operation: 0.2,
      relay: 0.3,
      mileage: 0.1,
      "color": "#0000ff",
      "type": "TC003"
    }
  ];

  // Take out
  const colorMap = (data) => {
    const output = {};
    data.forEach(item => {
      output[item.type] = item.color;
    })
    return output;
  }




  var margin = {top: 30, right: 50, bottom: 30, left: 50};
  var width = 380 - margin.left - margin.right,
      height = 250 - margin.top - margin.bottom,
      radius = Math.min(width, height) / 2;
  var svg;

  const gridLines = '#d8d8d8';

  // Adds the svg canvas
  var svg = d3.select("#stacked")
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");

  // Do other stuff

  var innerPadding = 0.5;
  var outerPadding = innerPadding/2;
  var x = d3.scale.ordinal()
      .rangeRoundBands([0, width], innerPadding, outerPadding);

  window.x = x

  var y = d3.scale.linear()
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .outerTickSize(1)
      .innerTickSize(-height)
      .tickPadding(10);

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      .innerTickSize(-width)
      .outerTickSize(1)
      .tickPadding(10)
      .tickFormat(d => {
        return d > 0 ? (d*100).toFixed()+'%' : ''
      });


  function update(data) {
    x.domain(data.map(function(d) { return d.name; }));
    y.domain([0, 1]);


    const colors = colorMap(data);
    const color = key => colors[key];

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
          .selectAll('line')
            .attr('transform', 'translate(' + (x.rangeBand()/2+(x.rangeBand()*innerPadding)-0.5) + ',-1)'); 

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);


    const layers = `installation operation relay mileage`.split(' ');
    let stackOffset = Array(layers.length).fill(0);
    layers.forEach((layer, i) => {
      const isLast = i === layers.length-1;
      const mult = isLast ? .5 : 1;
      svg.selectAll(`bar-${layer}`).data(data)
        .enter().append("rect")
          .attr("class", "bar")
          .attr('fill', d => d3.hcl(d.color).brighter(i/2))
          .attr("x", function(d) { return x(d.name); })
          .attr("y", function(d, i) { return y(d[layer]*mult + stackOffset[i]); })
          .attr("height", function(d) { return height - y(d[layer]*mult); })
          .attr("width", x.rangeBand())
          .each((d, i) => { if (!isLast) stackOffset[i] += d[layer] });
      if (isLast) {
        svg.selectAll(`bar-${layer}-topper`).data(data)
          .enter().append("rect")
            .attr('fill', d => d3.hcl(d.color).brighter(i/2))
            .attr('rx', 4)
            .attr("x", function(d) { return x(d.name); })
            .attr("y", function(d, i) { return y(d[layer] + stackOffset[i]); })
            .attr("height", function(d) { return height - y(d[layer]); })
            .attr("width", x.rangeBand())
            .each((d, i) => stackOffset[i] += d[layer]);
      }

    });


    svg.selectAll('text')
      .style('fill', 'gray');
  }

  function type(d) {
    d.utilization = +d.utilization; // coerce to number
    return d;
  }
  update(dummyCranes);
})();