(function() {
  var dummyCranes = [
    {
      ordinal: 1,
      "utilization": 0.5,
      "color": "#ff9900",
      "type": "MB001"
    },
    {
      ordinal: 2,
      "utilization": 0.2,
      "color": "#ff0099",
      "type": "TC001"
    },
    {
      ordinal: 3,
      "utilization": 0.8,
      "color": "#0000ff",
      "type": "TC003"
    },
    {
      ordinal: 4,
      "utilization": 0.06,
      "color": "#4bc",
      "type": "MB002"
    },
  ];

  // Take out
  const colorMap = (data) => {
    const output = {};
    data.forEach(item => {
      output[item.type] = item.color;
    })
    return output;
  }




  var margin = {top: 30, right: 50, bottom: 30, left: 50};
  var width = 380 - margin.left - margin.right,
      height = 250 - margin.top - margin.bottom,
      radius = Math.min(width, height) / 2;
  var svg;

  const gridLines = '#d8d8d8';

  // Adds the svg canvas
  var svg = d3.select("#util")
      .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
      .append("g")
          .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");

  // Do other stuff

  var x = d3.scale.ordinal()
      .rangeRoundBands([0, width], .7, .35);

  var y = d3.scale.linear()
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .outerTickSize(-height)
      .innerTickSize(0)
      .tickPadding(10);

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(5)
      .innerTickSize(-width)
      .outerTickSize(1)
      .tickPadding(10)
      .tickFormat(d => {
        return d > 0 ? (d*100).toFixed()+'%' : ''
      });


  function update(data) {
    x.domain(data.map(function(d) { return d.ordinal; }));
    y.domain([0, 1]);


    const colors = colorMap(data);
    const color = key => colors[key];

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);


    svg.selectAll(".bar")
        .data(data)
      .enter().append("rect")
        .attr("class", "bar")
        .attr('fill', d => d.color)
        .attr('rx', 4)
        .attr("x", function(d) { return x(d.ordinal); })
        .attr("y", function(d) { return y(d.utilization); })
        .attr("height", function(d) { return height - y(d.utilization); })
        .attr("width", x.rangeBand());

    svg.selectAll(".bar-title")
        .data(data)
      .enter().append("rect")
        .attr("class", "bar")
        .attr('fill', d => d.color )
        .attr("x", function(d) { return x(d.ordinal); })
        .attr("y", function(d) { return y(d.utilization*.5); })
        .attr("height", function(d) { return height - y(d.utilization*.5); })
        .attr("width", x.rangeBand());


    svg.selectAll('text')
      .style('fill', 'gray');
  }

  function type(d) {
    d.utilization = +d.utilization; // coerce to number
    return d;
  }
  update(dummyCranes);
})();