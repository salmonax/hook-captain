(function() {
    var dummyData = [
      {
        "date": "1-May-12",
        "close": 58.13
      },
      {
        "date": "30-Apr-12",
        "close": 53.98
      },
      {
        "date": "27-Apr-12",
        "close": 67
      },
      {
        "date": "26-Apr-12",
        "close": 89.7
      },
      {
        "date": "25-Apr-12",
        "close": 99
      },
      {
        "date": "24-Apr-12",
        "close": 130.28
      },
      {
        "date": "23-Apr-12",
        "close": 166.7
      },
      {
        "date": "20-Apr-12",
        "close": 234.98
      },
      {
        "date": "19-Apr-12",
        "close": 345.44
      },
      {
        "date": "18-Apr-12",
        "close": 443.34
      },
      {
        "date": "17-Apr-12",
        "close": 543.7
      },
      {
        "date": "16-Apr-12",
        "close": 580.13
      },
      {
        "date": "13-Apr-12",
        "close": 605.23
      },
      {
        "date": "12-Apr-12",
        "close": 622.77
      },
      {
        "date": "11-Apr-12",
        "close": 626.2
      },
      {
        "date": "10-Apr-12",
        "close": 628.44
      },
      {
        "date": "9-Apr-12",
        "close": 636.23
      },
      {
        "date": "5-Apr-12",
        "close": 633.68
      },
      {
        "date": "4-Apr-12",
        "close": 624.31
      },
      {
        "date": "3-Apr-12",
        "close": 629.32
      },
      {
        "date": "2-Apr-12",
        "close": 618.63
      },
      {
        "date": "30-Mar-12",
        "close": 599.55
      },
      {
        "date": "29-Mar-12",
        "close": 609.86
      },
      {
        "date": "28-Mar-12",
        "close": 617.62
      },
      {
        "date": "27-Mar-12",
        "close": 614.48
      },
      {
        "date": "26-Mar-12",
        "close": 606.98
      }
    ];

    const greenLine = '#41c274';
    const orangeLine = '#ffac2f';
    const gridLines = '#d8d8d8';

    const invertData = (data) => {
        const reversed = data.slice().reverse();
        return data.map((item, i) => Object.assign({}, item, { close: reversed[i].close/1.5 }));
    };


    var reversedData = invertData(dummyData);

    var margin = {top: 30, right: 0, bottom: 30, left: 0};

    var width = 380 - margin.left - margin.right,
        height = 250 - margin.top - margin.bottom,
        radius = Math.min(width, height) / 2;
    var svg;

    // Parse the date / time
    var parseDate = d3.time.format("%d-%b-%y").parse;

    // Set the ranges
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);

    // Define the axes
    var xAxis = d3.svg.axis().scale(x)
        .orient("bottom").ticks(5)
          .tickPadding(10)
          .innerTickSize(-height)
          .tickFormat((d, i) => `Week ${i + 1}`)

    var xAxisTop = d3.svg.axis().scale(x)
        .orient("top")
        .ticks(5)
        .innerTickSize(0)
        .tickFormat('');


    var yAxis = d3.svg.axis().scale(y)
        .orient("left").ticks(5);


        
    // Adds the svg canvas
    var svg = d3.select("#price")
        .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
        .append("g")
            .attr("transform", 
                  "translate(" + margin.left + "," + margin.top + ")");

    // Define the line
    var firstLine = d3.svg.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.close); })
        .interpolate('basis');

    // Define the line
    var secondLine = d3.svg.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.close); })
        .interpolate('basis');


    // Get the data
    function update(data, secondData) {
        data.forEach(function(d) {
            d.date = parseDate(d.date);
            d.close = +d.close;
        });

        secondData.forEach(function(d) {
            d.date = parseDate(d.date);
            d.close = +d.close;
        });


        // Scale the range of the data
        x.domain(d3.extent(data, function(d) { return d.date; }));
        y.domain([0, d3.max(data, function(d) { return d.close; })]);



        // Add the X Axis and gridlines

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .selectAll('text')
              .style('text-anchor', 'end')
              .style('fill', 'gray');
        
        svg.append("g")
            .attr("class", "x axis")
            .call(xAxisTop);


        // Add the first line
        svg.append("path")
            .attr("class", "line")
            .attr("d", firstLine(data))
            .attr('stroke', orangeLine);

        // Add the second line

        svg.append("path")
            .attr("class", "line")
            .attr("d", secondLine(secondData))
            .attr('stroke', greenLine);

        // Add the Y Axis
        // svg.append("g")
        //     .attr("class", "y axis")
        //     .call(yAxis);


    }

    update(dummyData, reversedData);

})();