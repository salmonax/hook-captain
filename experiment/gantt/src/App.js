import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom';


class TimeBar extends Component {
  state = {
    left: 0,
    right: 0,
  }

  componentDidMount() {
    // Prevent sticky mouse on window leave
    window.addEventListener('mouseup', this.onMouseUp, true);
    // Allow mouse movement to persist when outside of element
    window.addEventListener('mousemove', this.onMouseMove, true);
    // Sorry, React

  }

  componentWillUnmount() {
    window.removeEventListener('mouseup', this.onMouseUp, true);
    window.addEventListener('mousemove', this.onMouseMove, true);
  }

  moveBar = (e) => {
    // if (!e.clientX) return;
    const offset = e.clientX - this._startX;
    const { left, right } = this._startRects;
    const rightMost = right;
    const leftMost = left;
    let newLeft = this._startLeft + offset;
    let newRight = this._startRight - offset;
    if (newRight < 0) {
      newLeft = newRight + newLeft;
      newRight = 0;
    }
    if (newLeft < 0) {
      newRight = newRight + newLeft;
      newLeft = 0;
    }
    this.setState({
      left: newLeft,
      right: newRight,
    });
  }

  resizeLeft = (e) => {
    const offset = e.clientX - this._startX;
    const newPosition = this._startLeft + offset;
    this.setState({
      left: Math.min(Math.max(newPosition, 0), this._startRects.width-this.state.right-5)
    });
  }

  resizeRight = (e) => {
    const offset = e.clientX - this._startX;
    const newPosition = this._startRight - offset;
    this.setState({
      right: Math.min(Math.max(newPosition, 0), this._startRects.width-this.state.left-5)
    });
  }

  _initClick = (e) => {
    this._startRects = this.container.getClientRects()[0];
    const { left, width } = this._startRects;
    this._startOffsetLeft = e.clientX - left;
    this._startOffsetRight = width - this._startOffsetLeft;
    this._startX = e.clientX;
    this._startLeft = this.state.left;
    this._startRight = this.state.right;
  }

  outerResize = (e) => {
    if (this._startOffsetLeft < this._startLeft) {
      this.resizeLeft(e);
    } else if (this._startOffsetRight < this._startRight) {
      this.resizeRight(e);
    }
  }

  onMouseDown = (e) => {
    e.preventDefault();
    this._initClick(e);
    this._action = {
      'inner-bar-left-handle': 'resizeLeft',
      'inner-bar-right-handle': 'resizeRight',
      'inner-bar': 'moveBar',
      'outer-bar-container': 'outerResize',
    }[e.target.className];
  }

  onMouseUp = (e) => {
    e.preventDefault();
    this._action = null
  }

  onMouseMove = (e) => {
    e.preventDefault();
    const action = this[this._action];
    if (action) action(e);
  }

  render() {
    const { left, right } = this.state;
    return (
      <div className="outer-bar-container" 
        onMouseDown={this.onMouseDown}
        onMouseMove={this.onMouseMove}
        ref={ref => this.container = ref}
      >
        <div className="outer-bar"/>
        <div className="inner-bar" style={{ left, right }}>
          <div className="inner-bar-left-handle"/>
          <div className="inner-bar-right-handle"/>
        </div>
      </div>
    );
  }

}





class App extends Component {
  render() {
    return (
      <div className="App" style={{
        margin: 20,
        width: 500,
        height: 400,
        backgroundColor: 'white',
        boxShadow: '2px 2px 7px #aae',
        position: 'relative'

      }}>
        <div style={{width: '100%'}}>
          <TimeBar/>
          <TimeBar/>
          <TimeBar/>
          <TimeBar/>
        </div>
      </div>
    );
  }
}

export default App;
