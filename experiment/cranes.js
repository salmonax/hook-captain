(function() {
  const dummyData = [
    {
      "count": 2,
      "color": "#ff9900",
      "type": "MB001"
    },
    {
      "count": 1,
      "color": "#ff0099",
      "type": "TC001"
    },
    {
      "count": 3,
      "color": "#0000ff",
      "type": "TC003"
    },
  ];

  const dummyData2 = [
    {
      "count": 2,
      "color": "#ff9900",
      "type": "MB001"
    },
    {
      "count": 1,
      "color": "#ff0099",
      "type": "TC001"
    },
    {
      "count": 3,
      "color": "#00ff00",
      "type": "TC005"
    },
    {
      "count": 3,
      "color": "#0000ff",
      "type": "TC003"
    },
  ];

  const dummyData3 = [
    {
      "count": 5,
      "color": "#ff9900",
      "type": "CB001"
    },
    {
      "count": 1,
      "color": "#ff0099",
      "type": "TC001"
    },
    {
      "count": 3,
      "color": "#00ff00",
      "type": "TC005"
    },
    {
      "count": 3,
      "color": "#0000ff",
      "type": "TC003"
    },
  ];




  var width = 380,
      height = 250,
      radius = Math.min(width, height) / 2;
  var svg;

  // var color = d3.scale.category10(), svg;

  const key = d => d.data.type;

  const colorMap = (data) => {
    const output = {};
    data.forEach(item => {
      output[item.type] = item.color;
    })
    return output;
  }

  var pie = d3.layout.pie()
    .value(d => d.count)
      .sort(null);

  var arc = d3.svg.arc()
    .innerRadius(radius - 10)
    .outerRadius(radius - 20);

  function arcTween(d) {
    var i = d3.interpolate(this._current, d);
    this._current = i(0);
    return function(t) { return arc(i(t)); };
  }


  svg = d3.select("#cranes").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


  function update(data) {
    var lastData = svg.selectAll(".outer-path").data();
    var currentData = pie(data);

    const colors = colorMap(data);
    const color = (key) => colors[key];

    console.log(lastData, currentData);

    var path = svg.selectAll(".outer-path").data(currentData, key);
            
    path.exit()
      .datum(function (d, i) {
        return findNeighborArc(i, currentData, currentData, key) || d;
      })
      .transition()
        .duration(500)
        .attrTween("d", arcTween)
        .remove();

    path.enter().append("path")
        .attr("class", "outer-path")
        .attr("d", arc)
        .each(function(d, i) { 
          this._current = findNeighborArc(i, lastData, currentData, key) || d;
        })
        .attr("fill", d => { 
          // console.log(d.data.type); 
          console.log(color(d.data.type)); 
          return color(d.data.type) 
        });

    path.transition()
          .duration(500)
          .attrTween("d", arcTween);
  }  

  update(dummyData);

  function reDrawChart() {
    update(dummyData2);
  }

  setTimeout(reDrawChart, 500);


  function findNeighborArc(i, data0, data1, key) {
    var d;
    return (d = findPreceding(i, data0, data1, key)) ? {startAngle: d.endAngle, endAngle: d.endAngle}
        : (d = findFollowing(i, data0, data1, key)) ? {startAngle: d.startAngle, endAngle: d.startAngle}
        : null;
  }

  // Find the element in data0 that joins the highest preceding element in data1.
  function findPreceding(i, data0, data1, key) {
    var m = data0.length;
    while (--i >= 0) {
      var k = key(data1[i]);
      for (var j = 0; j < m; ++j) {
        if (key(data0[j]) === k) return data0[j];
      }
    }
  }

  // Find the element in data0 that joins the lowest following element in data1.
  function findFollowing(i, data0, data1, key) {
    var n = data1.length, m = data0.length;
    while (++i < n) {
      var k = key(data1[i]);
      for (var j = 0; j < m; ++j) {
        if (key(data0[j]) === k) return data0[j];
      }
    }
  }


  var inner = d3.svg.arc()
    .outerRadius(radius - 20)
    .innerRadius(radius - 40);



  var g2 = svg.selectAll(".inner")
    .data(pie([{count: 1}]))
    .enter().append("g")
    .style("fill", "#eee")
    .attr("class", "inner");
  g2.append("path")
    .attr("d", inner)
    .style("fill", "#eee")

})();