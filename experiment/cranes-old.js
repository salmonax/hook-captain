const dummyData = [
  {
    "count": 2,
    "color": "#ff9900",
    "type": "MB001"
  },
  {
    "count": 1,
    "color": "#ff0099",
    "type": "TC001"
  },
  {
    "count": 3,
    "color": "#0099ff",
    "type": "TC003"
  }
];

var width = 380,
    height = 250,
    radius = Math.min(width, height) / 2;


const colorRange = dummyData.map(({color}) => color);

var color = d3.scale.ordinal()
    .range(colorRange);


var outer = d3.svg.arc()
    .outerRadius(radius - 10)
    .innerRadius(radius - 30);

var inner = d3.svg.arc()
	.outerRadius(radius - 30)
	.innerRadius(radius - 50);

var pie = d3.layout.pie()
    .sort(null)
    .value(d => d.count);

var svg = d3.select("#cranes").append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

var path = svg.selectAll('path').data(pie(dummyData));

path.enter().append('path')
	.attr('fill', d => color(d.data.type))
	.attr('d', outer)
	.each(d => this._current = d)

path.transition()
	.attrTween('d', arcTween);

path.exit().remove();


function arcTween(a) {
  var i = d3.interpolate(this._current, a);
  this._current = i(0);
  return function(t) {
    return arc(i(t));
  };
}



var g2 = svg.selectAll(".inner")
	.data(pie([{count: 1}]))
	.enter().append("g")
	.attr("class", "inner");
g2.append("path")
	.attr("d", inner)
	.style("fill", "#eee");

